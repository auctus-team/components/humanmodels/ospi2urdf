%% Calculate the RMS of streamed EMG %%

clear;
load 0323-yang-jump1-2-13s.mat; % EMG file %

EMG_4 = ans(:,4); % which channel to calculate

figure(1);
plot(EMG_4);

RMS = rms(EMG_4,30,10,0);


figure(2);
plot(RMS); % RMS sampled at 10hz %
