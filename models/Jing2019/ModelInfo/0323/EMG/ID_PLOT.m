ID = readtable('ID.txt');
subplot(4,2,1)
plot(ID.hip_flexion_r_moment)
title('Right hip flexion moment');
subplot(4,2,2)
plot(ID.hip_flexion_r_moment)
title('Left hip flexion moment');
subplot(4,2,3)
plot(ID.knee_angle_r_moment)
title('Right knee flexion moment');
axis([0 120 -750 750]);
subplot(4,2,4)
plot(ID.knee_angle_l_moment)
title('Left knee flexion moment');
axis([0 120 -750 750]);
subplot(4,2,5)
plot(ID.ankle_angle_r_moment)
title('Right ankle moment');
subplot(4,2,6)
plot(ID.ankle_angle_l_moment)
title('Left ankle moment');
subplot(4,2,7)
plot(ID.elv_angle_moment)
title('Right shoulder swing moment');
axis([0 120 -2500 2500]);
subplot(4,2,8)
plot(ID.elv_angle_l_moment)
title('Left shoulder swing moment');
axis([0 120 -2500 2500]);
