%% Calculate the RMS of streamed EMG %%

clear;
load 0323-yang-jump1-2-13s.mat; % EMG file %

EMG_4 = ans(400:2600,4); % which channel to calculate

RMS = rms(EMG_4,30,10,0);
RMS2 = rms(EMG_4,20,5,0);

subplot(3,1,1);
plot(EMG_4);

subplot(3,1,2);
plot(RMS); % RMS sampled at 10hz %

subplot(3,1,3);
plot(RMS2); 

xlswrite('RMS_Jump1',RMS)