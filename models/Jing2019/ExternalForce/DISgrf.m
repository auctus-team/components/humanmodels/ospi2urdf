%%% using the foot tracking position to distinguish the distribution of grf%%%
load position.txt
t=0:0.01666667:2;
T=1:120;
Fr=position(:,1);
Fl=position(:,2);
grf=position(:,3);

figure(7)
subplot(2,1,1)
plot(t,Fr,t,Fl);
subplot(2,1,2)
plot(t,grf);

figure(8)
subplot(2,1,1)
plot(T,Fr,'r',T,Fl,'k');
subplot(2,1,2)
plot(T,grf);