#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Fri Jan 22 10:49:31 2021

@author: auctus
"""

import sys
sys.path.append('../../ospi')

# inspired from https://gepettoweb.laas.fr/doc/stack-of-tasks/pinocchio/master/doxygen-html/md_doc_b-examples_i-inverse-kinematics.html

import subprocess
import time
#from ospi 
#import viewer_utils as vw
import wrapper as wr
#import motion_parser as mtp

import pinocchio as se3
from pinocchio.robot_wrapper import RobotWrapper

from numpy.linalg import norm, solve
import numpy as np
import algebra as alg
import random

import os

import xml.etree.ElementTree as xml

import pyopsim
import comparison_urdf_os as cuo






def create_noise_positions(urdf_model,urdf_data, OS_model,nbe_pos = 10,noise = 1e-3):
    upperlims = []
    lowerlims = []
    L_req_pos = []
    L_true_pos = []
    
    for name in (OS_model.D_joints_range):
        lim = OS_model.D_joints_range[name]
        upperlims.append(lim[1])
        lowerlims.append(lim[0])
    
    LQtst = [upperlims,lowerlims]
    
    for k in range(nbe_pos):
        Qtry = []
        for i in range(len(upperlims)):
            val = random.random()*( ( upperlims[i]-lowerlims[i] ) ) + lowerlims[i]
            Qtry.append(val)
        LQtst.append(Qtry)        

    for i in range(len(LQtst)):
        
        q_urdf = se3.neutral(urdf_model)
        Lname = []
        
        osim_joints_processed = list(OS_model.D_joints_value.keys())
        for joint,name in zip(urdf_model.joints,urdf_model.names):
            if name in osim_joints_processed:
                nq = joint.nq
                idx_q = joint.idx_q
                q_osim_joint = OS_model.D_joints_value[name]
                q_urdf[idx_q:idx_q+nq] = q_osim_joint
                osim_joints_processed.remove(name)
                Lname.append(name)

        se3.forwardKinematics(urdf_model,urdf_data,q_urdf)

        select_pos = {}    
        true_pos = {}
        for joint_name in Lname:
                        
            urdf_id = urdf_model.getJointId(joint_name)
            jp= urdf_data.oMi[urdf_id]
    
            true_pos[joint_name] = [jp.rotation,jp.translation]
    
            r,p,y = alg.euler_from_rot_mat(jp.rotation)
            
            r2 = r+ noise*(2*np.random.random()-1)
            p2 = p+ noise*(2*np.random.random()-1)            
            y2 = y+ noise*(2*np.random.random()-1)            
            
            R = alg.euler_matrix(r2,p2,y2)[:3,:3]

            T = jp.translation+ noise*(2*np.random.random((1,3))-1)
            T = T.reshape(3,1)
            
            select_pos[joint_name] = [R,T]
            
        L_req_pos.append(select_pos)
        L_true_pos.append(true_pos)
    
    return(L_req_pos,L_true_pos)

def tst_random_IK(urdf_model,urdf_data, OS_model, ntry = 10):
       
    q_urdf = se3.neutral(urdf_model)
    
    L_req_pos,L_true_pos = create_noise_positions(urdf_model, urdf_data, OS_model,ntry,1e-2)
    
    max_q = -1
    max_err = -1
    max_err_pond = -1
    
    for i in range(len(L_req_pos)):
        
        Lpond = []
        Lcons = []
    
 
        Lcons_true = []
        
        current_req_pos = L_req_pos[i]
        true_req_pos = L_true_pos[i]
        Lname = list(current_req_pos.keys())
       
        for joint_name in Lname:
            R,T = current_req_pos[joint_name]
            
            # if joint_name != "hand_finger_flex_r":
            #     n_nope = 1

            #     Lpond += [n_nope for k in range(6)]
            # else:
            #     Lpond += [1e-8,1e-8,1e-8,1e-8,1e-8,1e-8]
            Lpond +=[1 for k in range(6)]
            oMdes = se3.SE3(R,T)   
            Lcons.append(oMdes) 
            
            R,T = true_req_pos[joint_name]
            oMdes = se3.SE3(R,T)   
            Lcons_true.append(oMdes) 
            
  
        
        vbe = False
        q_pond,urdf_data_pond,err_pond = cuo.moveListJoint_pond(Lname, Lcons, Lpond, urdf_model, urdf_data, verbose = vbe)
        q,urdf_data,err = cuo.moveListJoint_pond(Lname, Lcons_true, [1 for k in range(len(Lpond)) ], urdf_model, urdf_data, verbose = vbe)
    
        if max_err < np.linalg.norm(err) :
            max_err = np.linalg.norm(err)
        
        if max_err_pond < np.linalg.norm(err_pond) :
            max_err_pond = np.linalg.norm(err_pond)
        
        dec_q = np.max(np.abs(q-q_pond))
        if max_q < dec_q:
            max_q = dec_q
    
    print("err pond : ",max_err_pond)
    print("err : ", max_err)
    print("dec q : ", max_q)
    
    # print("q : ", q)
    #print("q_pond : ", q_pond)
    
if __name__ == "__main__":
    
    # # ---- arm26.osim
    
    # ik_path = '/home/auctus/Documents/OpenSim/4.2-2020-11-24-d9c8bb2c/Models/Arm26/OutputReference/InverseKinematics'

    # osim_filename = '/home/auctus/Documents/OpenSim/4.2-2020-11-24-d9c8bb2c/Models/Arm26/OutputReference/arm26.osim'

    # setup_ik_file = 'Setup_IK_arm26_elbow_flex.xml'

    # trc_path = '/home/auctus/Documents/OpenSim/4.2-2020-11-24-d9c8bb2c/Models/Arm26/OutputReference/InverseKinematics/arm26_elbow_flex.trc'

    # geometry_path = "/home/auctus/Documents/OpenSim/4.2-2020-11-24-d9c8bb2c/Models/SimulationDataAndSetupFiles-4.0/Geometry"

    # urdf_filename = "/home/auctus/catkin_ws/src/human_control/description/arm26/arm26_aCol.urdf.xacro"
    
     ## Complete path to the .osim file (path + .osim file)
    osim_filename = '/home/auctus/ospi2urdf/models/whole_body/wholebody.osim'
    
    ## Path to the folder containing Inverse Kinematics configuration file.
    ik_path = '/home/auctus/opensim_python/OpenSim/XML'
    
    ## Name of the Inverse Kinematics configuration file.
    setup_ik_file = "Walk_Mkrs_IK.xml"
    
    ## Path to a trc file associated with the .osim file.
    trc_path= '/home/auctus/opensim_python/OpenSim/TRC/Walk_Mkrs.trc'
    
    ## Path to the .vtp files associated with the .osim model.
    geometry_path = "/home/auctus/Documents/OpenSim/4.2-2020-11-24-d9c8bb2c/Models/SimulationDataAndSetupFiles-4.0/Geometry"
        
    ## Complete path to the .urdf file (path + .urdf file)
    urdf_filename="/home/auctus/catkin_ws/src/human_control/description/wholebody/wholebody_aCol.urdf.xacro"

    ## Constructor of the OsimModel class
    OS_model = pyopsim.OsimModel(osim_filename,geometry_path,ik_path,setup_ik_file,trc_path)
    
    urdf_model    = se3.buildModelFromUrdf(urdf_filename)
    
    urdf_data = urdf_model.createData()
    
    tst_random_IK(urdf_model,urdf_data, OS_model)
        

    
        # trviz = list(jp.translation)
        
        # quat_rviz = [-0.706,0.04,-0.001,0.707]
        
        # XYZQuat = tuple(trviz+quat_rviz)
    
        # MXYZQuat = se3.XYZQUATToSE3(XYZQuat)
        
        # oMdes2 = se3.SE3( MXYZQuat )
        
        # qt = [quat_rviz[3],quat_rviz[0],quat_rviz[1],quat_rviz[2]]
    
        # oMdes = se3.SE3( alg.quaternion_matrix(qt)[:3,:3] , np.array(trviz))    
    