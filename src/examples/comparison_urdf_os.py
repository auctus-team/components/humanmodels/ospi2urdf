import sys
sys.path.append('../../ospi')

# inspired from https://gepettoweb.laas.fr/doc/stack-of-tasks/pinocchio/master/doxygen-html/md_doc_b-examples_i-inverse-kinematics.html

import subprocess
import time
#from ospi 
#import viewer_utils as vw
import wrapper as wr
#import motion_parser as mtp

import pinocchio as se3
from pinocchio.robot_wrapper import RobotWrapper

from numpy.linalg import norm, solve
import numpy as np
import algebra as alg
import random

import os

import xml.etree.ElementTree as xml

import pyosim



def getDoc(obj):
    """!
    
    Get and print all the documentation of a Pinocchio object.
    
    @param obj : A pinocchio object.

    @return None

    """
    print("---- Documentation ----")
    for name, function in obj.__class__.__dict__.items():
        print(' **** %s: %s' % (name, function.__doc__))  

def moveListJoint_pond(Lname,Lcons,Lpond,urdf_model,urdf_data,verbose=False):
    """!

    Given a list of joints name Lname, a list of constraints to be reached 
    Lcons, a Pinocchio model urdf_model and its associated data object urdf_data, execute
    an Inverse Kinematic operation in order to determine the configuration q to 
    reach the constraints, if these constraints can be achieved. 
    A message is displayed if those constraints are achieved or not. 
    Inspired from https://gepettoweb.laas.fr/doc/stack-of-tasks/pinocchio/master/doxygen-html/md_doc_b-examples_i-inverse-kinematics.html .
    
    @param Lname : List.
    
        List of the name of the joints on which the Inverse Kinematics operation is to be applied.
    
    @param Lcons : List of list.
    
        List of the constraint to be reached by the selected joints.
        One element is organized as follows : 
        [ Rotation matrix, Position matrix ]
        Example : 
        [  se3.utils.rotate('x',3.0)  , np.array([0.08, -0.07, 0.86]) ]
    
    @param urdf_model : Pinocchio model object.
        Pinocchio object containing all the physical description of the loaded URDF model, 
        including kinematic and inertial parameters defining its structure.
        Please check https://gepettoweb.laas.fr/doc/stack-of-tasks/pinocchio/master/doxygen-html/md_doc_a-features_intro.html 
        for more informations.
        
    @param urdf_data : Pinocchio data object.
         Pinocchio object containing all the values of the loaded URDF model which
         are the result of a computation. (joint configuration, velocity, ...).

    @param verbose : Boolean.
        Activate the print functions of the algorithm. Useful for debugging.
        The default is False.

    @return q : numpy.array
        Vector Q from the Inverse Kinematics operation.
    
    @return urdf_data : Pinocchio data object.
         The pinocchio data object updated so the joints have the values set in 
         the Q vector.
    
    @return err : numpy.array (6 x number_of_joints, )
        Final error vector between current and desired joints values.

    """
    L_joint = []
    L_oMdes = []
    
     
    for k in range(len(Lname)):
        JOINT_ID = urdf_model.getJointId(Lname[k])
        try:
            oMdes = se3.SE3( Lcons[k][0] , Lcons[k][1])
        except TypeError:
            oMdes = Lcons[k]
        L_joint.append(JOINT_ID)
        L_oMdes.append(oMdes) 

    q      = se3.neutral(urdf_model)

    # minimum difference between current and desired joints values to stop the
    # Inverse Kinematics operation.
    eps    = 1e-4
    # the maximum number of iterations of the Inverse Kinematics operation.
    IT_MAX = 200
    # time step
    DT     = 1e-1
    # regularization value
    damp   = 1e-6

    M_pond = damp*np.array(Lpond)
    
    i=0
    while True:
          nb_cons = len(L_joint)
          # forward kinematics to the urdf model
          se3.forwardKinematics(urdf_model,urdf_data,q)
          
          # check the difference between current and desired joints values.
          err = np.zeros((1,1))
          for k in range(len(L_oMdes)):
            dMi= L_oMdes[k].actInv(urdf_data.oMi[L_joint[k]] )
            if err.shape == (1,1):
                err = se3.log(dMi).vector
            else:
                err = np.hstack( ( err,  se3.log(dMi).vector ) )
         
          if norm(err) < eps:
              success = True
              break
          if i >= IT_MAX:
              success = False
              break
    
          # get the Jacobian of each of the joints concerned by the Inverse Kinematics
          # operation.
          J = np.zeros((1,1))
          for k in range(len(L_joint)):
              Jk = se3.computeJointJacobian(urdf_model,urdf_data,q,L_joint[k])
              if J.shape == (1,1):
                  J = Jk
              else:
                  J = np.vstack( (J, Jk) )
         
          # get the velocity vector.
          
          # test de plutôt mettre M_pond sur err
          v = - J.T.dot( solve(J.dot(J.T) + np.multiply( np.eye(6*nb_cons), M_pond ), err)           )
          
          # get the vector Q according to the velocity vector.
          q = se3.integrate(urdf_model,q,v*DT)
              
          if not i % 10 and verbose:
              print('%d: error = %s' % (i, err.T))
          i += 1
     
    if success :
          print("Convergence achieved!")
    else:
          print("\nWarning: the iterative algorithm has not reached convergence to the desired precision")
          print(err.T)
    if verbose:
        print('\nresult: %s' % q.flatten().tolist())
        print('\nfinal error: %s' % err.T)
    
        printConfig(urdf_model,urdf_data)
        
    return(q,urdf_data,err)    


def moveListJoint(Lname,Lcons,urdf_model,urdf_data,verbose=False):
    """!

    Given a list of joints name Lname, a list of constraints to be reached 
    Lcons, a Pinocchio model urdf_model and its associated data object urdf_data, execute
    an Inverse Kinematic operation in order to determine the configuration q to 
    reach the constraints, if these constraints can be achieved. 
    A message is displayed if those constraints are achieved or not. 
    Inspired from https://gepettoweb.laas.fr/doc/stack-of-tasks/pinocchio/master/doxygen-html/md_doc_b-examples_i-inverse-kinematics.html .
    
    @param Lname : List.
    
        List of the name of the joints on which the Inverse Kinematics operation is to be applied.
    
    @param Lcons : List of list.
    
        List of the constraint to be reached by the selected joints (expressed in ground referential).
        One element is organized as follows : 
        [ Rotation matrix, Position matrix ]
        Example : 
        [  se3.utils.rotate('x',3.0)  , np.array([0.08, -0.07, 0.86]) ]
    
    @param urdf_model : Pinocchio model object.
        Pinocchio object containing all the physical description of the loaded URDF model, 
        including kinematic and inertial parameters defining its structure.
        Please check https://gepettoweb.laas.fr/doc/stack-of-tasks/pinocchio/master/doxygen-html/md_doc_a-features_intro.html 
        for more informations.
        
    @param urdf_data : Pinocchio data object.
         Pinocchio object containing all the values of the loaded URDF model which
         are the result of a computation. (joint configuration, velocity, ...).

    @param verbose : Boolean.
        Activate the print functions of the algorithm. Useful for debugging.
        The default is False.

    @return q : numpy.array
        Vector Q from the Inverse Kinematics operation.
    
    @return urdf_data : Pinocchio data object.
         The pinocchio data object updated so the joints have the values set in 
         the Q vector.
    
    @return err : numpy.array (6 x number_of_joints, )
        Final error vector between current and desired joints values.

    """
    L_joint = []
    L_oMdes = []
    
     
    for k in range(len(Lname)):
        JOINT_ID = urdf_model.getJointId(Lname[k])
        try:
            oMdes = se3.SE3( Lcons[k][0] , Lcons[k][1])
        except TypeError:
            oMdes = Lcons[k]
        L_joint.append(JOINT_ID)
        L_oMdes.append(oMdes) 

    q      = se3.neutral(urdf_model)

    # minimum difference between current and desired joints values to stop the
    # Inverse Kinematics operation.
    eps    = 1e-4
    # the maximum number of iterations of the Inverse Kinematics operation.
    IT_MAX = 200
    # time step
    DT     = 1e-1
    # regularization value
    damp   = 1e-6
    #print(L_oMdes[0].toActionMatrix() )
    # print( L_oMdes[0].actInv(urdf_data.oMi[L_joint[0]] ) )

    # M1 = L_oMdes[0]
    # M2 = urdf_data.oMi[L_joint[0]]

    # R3 = np.dot(M1.rotation.T,M2.rotation)

    # print("R3 : ", R3)

    i=0
    while True:
          nb_cons = len(L_joint)
          # forward kinematics to the urdf model
          se3.forwardKinematics(urdf_model,urdf_data,q)
          
          # check the difference between current and desired joints values.
          err = np.zeros((1,1))
          for k in range(len(L_oMdes)):
            dMi= L_oMdes[k].actInv(urdf_data.oMi[L_joint[k]] )
            #print(dMi)
            if err.shape == (1,1):
                err = se3.log(dMi).vector
            else:
                err = np.hstack( ( err,  se3.log(dMi).vector ) )
         
          if norm(err) < eps:
              success = True
              break
          if i >= IT_MAX:
              success = False
              break
    
          # get the Jacobian of each of the joints concerned by the Inverse Kinematics
          # operation.
          J = np.zeros((1,1))
          for k in range(len(L_joint)):
              Jk = se3.computeJointJacobian(urdf_model,urdf_data,q,L_joint[k])
              if J.shape == (1,1):
                  J = Jk
              else:
                  J = np.vstack( (J, Jk) )
         
          # get the velocity vector.
          v = - J.T.dot( solve(J.dot(J.T) + damp * np.eye(6*nb_cons), err)           )
          
          # get the vector Q according to the velocity vector.
          q = se3.integrate(urdf_model,q,v*DT)
              
          if not i % 10 and verbose:
              print('%d: error = %s' % (i, err.T))
          i += 1
     
    if success :
          print("Convergence achieved!")
    else:
          print("\nWarning: the iterative algorithm has not reached convergence to the desired precision")
      
    if verbose:
        print('\nresult: %s' % q.flatten().tolist())
        print('\nfinal error: %s' % err.T)
    
        printConfig(urdf_model,urdf_data)
        
    return(q,urdf_data,err)

def os2pi_Dpos(D_osim,Meth = 1):
    """!
    
    Take position dictionnaries expressed in osim referential, and express them into 
    .urdf referential.

    @param D_osim : Dictionnary of List.
    
        Dictionary containing, for each name, the rotation and position
        matrix expressed in the global referential of the .osim file.
        
        The dictionary is organized as follows :
            
        D_osim[name] = [ Position matrix, Rotation matrix ]
        
    @param Meth : Int, optionnal.
       Method chosen to express the rotation matrix from the .osim referential 
       to the .urdf referential.
    
    @return D_urdf : Dictionnary of List.
    
        Dictionary containing, for each name, the rotation and position
        matrix expressed in the global referential of the .urdf file.
        
        The format of this dictionary is the same as the D_urdf dictionary.
    
    """
    osMpi = np.dot(se3.utils.rotate('z', np.pi/2) , se3.utils.rotate('x', np.pi/2) )

    D_urdf = {}
    
    for name in D_osim:
        p_urdf = np.dot(osMpi, D_osim[name][0] )
            
        if Meth == 1:
            # rotation matrix
            R_osim = D_osim[name][1]
            R_urdf = np.dot(osMpi,  np.dot(R_osim, osMpi.T) )

        elif Meth == 2:
            # hand transform
            R_osim = D_osim[name][1]
            ro,po,yo = alg.euler_from_rot_mat(D_osim[name][1])
            R_urdf = np.eye(3,3)
            
            R_urdf[0,0] = R_osim[2,2]
            R_urdf[0,1] = R_osim[2,0]
            R_urdf[0,2] = R_osim[2,1]
                
            R_urdf[1,0] = R_osim[0,2]
            R_urdf[1,1] = R_osim[0,0]
            R_urdf[1,2] = R_osim[0,1]
                
            R_urdf[2,0] = R_osim[1,2]
            R_urdf[2,1] = R_osim[1,0]
            R_urdf[2,2] = R_osim[1,1]       

        D_urdf[name] = [p_urdf,R_urdf]
        
    return(D_urdf)

def findGroundJointId(urdf_model):
    """!
    
    Short method to get the ground joint index of a Pinocchio model based on a
    .osim model.
    The ground joint is the joint between the origin of the global referential and
    the root of the Pinocchio model.

    @param urdf_model : Pinocchio model object.
        Pinocchio object containing all the physical description of the loaded URDF model, 
        including kinematic and inertial parameters defining its structure.
        Please check https://gepettoweb.laas.fr/doc/stack-of-tasks/pinocchio/master/doxygen-html/md_doc_a-features_intro.html 
        for more informations.
        
    @return ground_joint_id : Int. 
        The index of the Ground joint in the Pinocchio model.

    """
    
    frame_ground = urdf_model.frames[ urdf_model.getFrameId("ground") ]
    ground_joint_id = frame_ground.parent+1

    return(ground_joint_id)
    

def staticDifference(urdf_model,urdf_data,OS_model, lim_err = 1e-6,verbose = True):
    """!
    
    Checks that the placement of joints (w.r.t each of them and then w.r.t world)
    of the .urdf model is consistent with that of the .osim model from which 
    it is derived.
    
    To do so, get the position and the orientation of each of the joints of the
    .osim model, express those informations into the .urdf referential, and then
    compare those informations to the concerned joints of the .urdf model.

    First, this method checks the placement of the joints expressed into the 
    the referential of their parent joint, then checks the placement of the joints
    expressed in the global referential for a neutral configuration of the Q 
    vector (= all elements of the Q vector set to 0).

    @param urdf_model : Pinocchio model object.
        Pinocchio object containing all the physical description of the loaded URDF model, 
        including kinematic and inertial parameters defining its structure.
        Please check https://gepettoweb.laas.fr/doc/stack-of-tasks/pinocchio/master/doxygen-html/md_doc_a-features_intro.html 
        for more informations.
        
    @param urdf_data : Pinocchio data object.
         Pinocchio object containing all the values of the loaded URDF model which
         are the result of a computation. (joint configuration, velocity, ...).

    @param OS_model : pyopsim OsimModel class.
        pyopsim object containing all the necessary informations about the .osim
        model.
        
    @param lim_err : float, optional.
        Maximal authorized error between the position and orientation of a .osim joint 
        and a .urdf joint. Otherwise, returns an error.
        The default is 1e-6.
        
    @param verbose : Boolean.
        Activate the print functions of the algorithm. Useful for debugging.
        The default is False.

    @return max_err : float.
        The maximal error encountered during the verification process.
    
    @return D_parts_urdf : Dictionary of List.
        Dictionary containing, for each joint name of the .urdf model, its joint
        parent index, the joint object associated with this name, and the placement
        of the joint in the referential of its parent joint. 
        An element of this dictionary is expressed as follows : 
            
            D_parts_urdf[joint name] = [ index of the parent joint, 
                                        Pinocchio joint object associated with the joint name,
                                        placement of the joint in the referential of its parent joint ]
            
    """

    D_parts_urdf = {}

    max_err = 0.0
    
    # create D_parts_urdf
    for name, parent, jp, joint in zip(urdf_model.names,urdf_model.parents,urdf_model.jointPlacements,urdf_model.joints):
        if name !="universe":
           D_parts_urdf[name] = [parent,joint,jp]
        

    # express the .osim model joint placement informations into the .urdf referential.
    D_jp_osim = os2pi_Dpos(OS_model.D_jp)
    first_case = False
    # compare the placement of each of the coordinates of the .osim model
    # to the placement of the joints into the .urdf model.    
    for part_name in D_parts_urdf:
        # handle the issue where the .osim joint associated to the .osim coordinate
        # object has more than one DOF.
        # in that case, as multiple .urdf joints are needed to represent this
        # type of .osim joint, a correction to part_name is added to be
        # sure that the first .urdf joint (linked to the good parent joint) is taken.
        try:
            # get the .osim joint name linked to the coordinate object.
            osim_joint_name = OS_model.D_coord_joints[part_name]
            # get the name of the first coordinate of the .osim joint (which is
            # the name of the first .urdf joint).
            part_name_cons = OS_model.D_joints_coord[osim_joint_name][0]

        except KeyError:
            # special case
            print("part_name : ", part_name)
            osim_joint_name = part_name
            part_name_cons = part_name
            
        # get the homogeneous matrix of the .urdf joint.
        jp = D_parts_urdf[part_name_cons][-1]
        
        # deal with bug about fixed joints not recognized by Pinocchio.
        # this bug is expressed by the fact the fixed joint is not expressed
        # into the chain, but its position is still saved : like if the joint
        # was erased, but its parameters are still saved.
        
        # solution to correct it : fuse the position of the fixed joint into Opensim
        # with the position of the next joint.
        
        osim_trans = D_jp_osim[osim_joint_name][0]
        osim_rot =  D_jp_osim[osim_joint_name][1]

        # prendre celui avant celui traite (id - 1)?
        # meilleur solution : trouver comment exprimer type du joint.
        # solution pr ça : comparer si ground_joint dans nom joints de urdf
        # (nom joint ground urdf = nom joint ground osim), si pas le cas, veut dire
        # qu'il y a pbme. 
        
        if first_case ==False and OS_model.D_gd_prop["joint_type"] == "WeldJoint":
            L_jp_os = list(D_jp_osim.keys())

            osim_trans_ori = D_jp_osim[L_jp_os[0]][0]
            osim_rot_ori =  D_jp_osim[L_jp_os[0]][1]

            osim_rot_bef = osim_rot
            osim_trans_bef = osim_trans
            
            osim_trans = np.dot(osim_rot_bef,osim_trans_ori) + osim_trans_bef
            osim_rot = np.dot(osim_rot_bef,osim_rot_ori)
            

        M_urdf = np.eye(4,4)
        M_urdf[:3,3] = np.array(jp.translation)
        M_urdf[:3,:3] = jp.rotation  
        
        # get the homogeneous matrix of the .osim joint.
        M_osim = np.eye(4,4)
        M_osim[:3,3] = osim_trans.flatten()
        M_osim[:3,:3] = osim_rot       

        # compare those homogeneous matrix.
        err = np.linalg.norm(M_osim-M_urdf)
        
        first_case = True
        
        if verbose or err > lim_err:
            print("Name : %s"%part_name)
            print("Difference OSIM / URDF : %f"%err)       
        
        if err > lim_err:
            print("Warning : err != 0")
            print("URDF : ")
            print("Name : %s"%part_name_cons)    
            print(jp.translation)
            print(jp.rotation)           
            print(M_urdf)
            print("OSIM : ")
            print("Name : %s"%osim_joint_name)  
            print(osim_trans)
            print(osim_rot)        
            print(M_osim)   
            exit(1)
        if err > max_err:
            max_err = err

    # checks the placement of the joints expressed in the global referential.
    
    # set the .urdf model to a neutral configuration.
    q_urdf = se3.neutral(urdf_model)
    se3.forwardKinematics(urdf_model,urdf_data,q_urdf)
    
    # set the .osim model to a neutral configuration.
    q_osim = [0. for k in range(len(OS_model.D_joints_value))]
    OS_model.setQ(q_osim)
    
    # express the placement of the joints of the .osim model into
    # the .urdf referential.
    D_joints_pos_osim = os2pi_Dpos(OS_model.D_joints_pos)
    if verbose:
        print("*** Absolute placement ***")
    # compare the position and orientation of each of the joints of the .osim model
    # to the position and orientation of the joints into the .urdf model.
    
    root_index = findGroundJointId(urdf_model)
    gd_joint = urdf_model.joints[root_index]
    gd_nq_urdf = gd_joint.nq
    gd_name_osim = OS_model.D_gd_prop["joint_name"]
    gd_nq_osim = len(OS_model.D_joints_coord[ gd_name_osim ])

    
    for part_name in D_parts_urdf:
        # handle the issue where the .osim joint associated to the .osim coordinate
        # object has more than one DOF.
        # in that case, as multiple .urdf joints are needed to represent this
        # type of .osim joint, a correction to part_name is added to be
        # sure that the last .urdf joint (which is going to combine all of the
        # previous transformations of the .urdf joints) is taken.
        try:
            osim_joint_name = OS_model.D_coord_joints[part_name]
            if osim_joint_name == OS_model.D_gd_prop["joint_name"]:
                if gd_nq_urdf ==1 and gd_nq_osim == 6:
                    part_name_cons = OS_model.D_joints_coord[osim_joint_name][2]
                else: 
                    part_name_cons = OS_model.D_joints_coord[osim_joint_name][-1]
            else:
                part_name_cons = OS_model.D_joints_coord[osim_joint_name][-1]
        except KeyError:
            # special case.
            osim_joint_name = part_name
            part_name_cons = part_name                         
        
        # get the homogeneous matrix of the .urdf joint.        
        urdf_id = urdf_model.getJointId(part_name_cons)
        jp= urdf_data.oMi[urdf_id]
        M_urdf = np.eye(4,4)
        M_urdf[:3,3] = np.array(jp.translation)
        M_urdf[:3,:3] = jp.rotation
        
        # get the homogeneous matrix of the .osim joint.                
        M_osim = np.eye(4,4)
        osim_trans = D_joints_pos_osim[osim_joint_name][0]
        osim_rot =  D_joints_pos_osim[osim_joint_name][1]        
        M_osim[:3,3] = osim_trans.flatten()
        M_osim[:3,:3] = osim_rot

        # compare those homogeneous matrix.
        err = np.linalg.norm(M_osim-M_urdf)
        
        if verbose or err > lim_err:
            print("Name : %s"%part_name)
            print("Difference OSIM / URDF : %f"%err)       
        
        if err > lim_err:
            print("Warning : err != 0")
            print("URDF : ")
            print("Name : %s"%part_name_cons)    
            print(jp.translation)
            print(jp.rotation)           
            print(M_urdf)
            print("OSIM : ")
            print("Name : %s"%osim_joint_name)  
            print(osim_trans)
            print(osim_rot)        
            print(M_osim)           
        
        
        if err > max_err:
            max_err = err    

    return(max_err,D_parts_urdf)

def printQ(urdf_model,q):
    """!
    
    Displays the value of each of the articulations of the .urdf model.

    @param urdf_model : Pinocchio model object.
        Pinocchio object containing all the physical description of the loaded URDF model, 
        including kinematic and inertial parameters defining its structure.
        Please check https://gepettoweb.laas.fr/doc/stack-of-tasks/pinocchio/master/doxygen-html/md_doc_a-features_intro.html 
        for more informations.
        
    @param q : numpy.array
        Vector Q associated with the .urdf model. 

    @return None.

    """
    print("----- Q from URDF -----")    
    for joint,name in zip(urdf_model.joints,urdf_model.names):
        nq = joint.nq
        idx_q = joint.idx_q
        q_joint = q[idx_q:idx_q+nq]
        print(" %s : "%name, q_joint)




def printConfig(urdf_model,urdf_data):
    """!
    
    Displays the position and orientation of each of the joints of the .urdf
    model, expressed into the global referential associated with the .urdf. 
    
    @param urdf_model : Pinocchio model object.
        Pinocchio object containing all the physical description of the loaded URDF model, 
        including kinematic and inertial parameters defining its structure.
        Please check https://gepettoweb.laas.fr/doc/stack-of-tasks/pinocchio/master/doxygen-html/md_doc_a-features_intro.html 
        for more informations.
        
    @param urdf_data : Pinocchio data object.
         Pinocchio object containing all the values of the loaded URDF model which
         are the result of a computation. (joint configuration, velocity, ...).

    @return None.

    """    
    print("----- Translation -----")    
    for name, oMi in zip(urdf_model.names, urdf_data.oMi):
        print(("{:<24} : {: .4f} {: .4f} {: .4f}"
              .format( name, *oMi.translation.T.flat )))
    print("----- Rotation -----")
    for name, oMi in zip(urdf_model.names, urdf_data.oMi):
        print(("{:<24} : {: .4f} {: .4f} {: .4f} "
            .format(name, *list(alg.euler_from_rot_mat(oMi.rotation)))) )

def getJointAxis(urdf_model):
    """!
    
    (Not used anymore).
    
    Extract from a .urdf model the axis of each of its joints.
    
    @param urdf_model : Pinocchio model object.
        Pinocchio object containing all the physical description of the loaded URDF model, 
        including kinematic and inertial parameters defining its structure.
        Please check https://gepettoweb.laas.fr/doc/stack-of-tasks/pinocchio/master/doxygen-html/md_doc_a-features_intro.html 
        for more informations.
        
    @return L_axis : List of List
        List containing the axis of each of the joints of the .urdf_model, 
        in the same order as the list names of the Pinocchio object urdf_model.
        Each element of the L_axis object is expressed as follows : 
            L_axis = [x-component of the axis vector, y-component of the axis vector, z-component of the axis vector ].

    """
    urdf_model.saveToXML('temp.xml',"XML")
    tree = xml.parse('temp.xml')
    root = tree.getroot()
    
    L_axis = []
    for joints in root.findall('./XML/joints'):
        
        item_list = joints.iter('item')
        for item in item_list:
            ID = item.find('i_id')
            if ID != None:
                #print(ID)
                AX = []
                axis_coor_list = item.findall('base_variant/value/axis/data/item')
                for coor in axis_coor_list:
                    AX.append(float(coor.text) )
                L_axis.append(AX)
    os.remove('temp.xml')

    return(L_axis)


def checkFreeFlyer(vec):
    """!
    
    (Not used anymore).
    
    Check if the values of a free flyer joint (6 DOF without any limits) are 
    correct or not.
    
    @param vec : numpy.array (7x1)
        Vector of the values of the free flyer joint, expressed as follows : 
        [ x , y , z , q_x , q_y , q_z , q_s ] (with q a quaternion).

    @return tst : Boolean.
        Indicates if the values are correct or not.

    """
    tst = True
    k = 0
    while k <3 and tst :
        if vec[k] == np.inf:
            tst = False
        k+=1
    k = 0
    while k <4 and tst:
        if vec[3+k] > 1.0:
            tst = False
        k+=1
    
    return(tst)


def testDynamicModel(OS_model, urdf_model, urdf_data,D_parts_urdf,ntry = 5,lim_err = 1e-6,verbose=False):
    """!
    
    Carries out a series of tests to check that the .urdf model conforms to the .osim model. 
    To do this, first test the positions of the joints at their extreme values, 
    then check for a series of ntry of random configurations the positions of 
    the joints of both models. 
    Stop the verification if the maximum error of a configuration is superior to
    the value lim_err. In this case, displays the number of the test that failed,
    the configuration vector Q and the value of the error. 

    @param OS_model : pyopsim OsimModel class.
        pyopsim object containing all the necessary informations about the .osim
        model.
    
    @param urdf_model : Pinocchio model object.
        Pinocchio object containing all the physical description of the loaded URDF model, 
        including kinematic and inertial parameters defining its structure.
        Please check https://gepettoweb.laas.fr/doc/stack-of-tasks/pinocchio/master/doxygen-html/md_doc_a-features_intro.html 
        for more informations.
        
    @param urdf_data : Pinocchio data object.
         Pinocchio object containing all the values of the loaded URDF model which
         are the result of a computation. (joint configuration, velocity, ...).

    @param D_parts_urdf : Dictionary of List.
        Dictionary containing, for each joint name of the .urdf model, its joint
        parent index, the joint object associated with this name, and the placement
        of the joint in the referential of its parent joint. 
        An element of this dictionary is expressed as follows : 
            
            D_parts_urdf[joint name] = [ index of the parent joint, 
                                        Pinocchio joint object associated with the joint name,
                                        placement of the joint in the referential of its parent joint ]
            
    @param ntry : Int, optional.
        Number of random tests that should be executed. The default is 5.
        
    @param lim_err : float, optional
        Maximal authorized error between the position and orientation of a .osim joint 
        and a .urdf joint for a configuration Q. Otherwise, @return None. an error. 
        The default is 1e-6.
        
    @param verbose : Boolean.
        Activate the print functions of the algorithm. Useful for debugging.
        The default is False.

    @return max_err : float.
        The maximal error encountered during the verification process for all the
        tested configurations.

    """

    print("")
    print("---- Dynamic tests for %i tests ---- \n"%ntry)
    
    
    upperlims = []
    lowerlims = []
    
    for name in (OS_model.D_joints_range):
        lim = OS_model.D_joints_range[name]
        upperlims.append(lim[1])
        lowerlims.append(lim[0])
    
    # if False:
    #     try:
    #         L_ex = [11,12]
    #         n1 =0
    #         n2 = 20
    #         for k in range(n1,n2):
    #             if k not in L_ex:
    #                 upperlims[k] = 0.
    #                 lowerlims[k] = 0.
            
    #         # n = 20
     
    #     except ValueError:
    #         upperlims = urdf_model.upperPositionLimit[7:]
    #         lowerlims = urdf_model.lowerPositionLimit[7:]        
    # lowerlims = urdf_model.lowerPositionLimit[7:]
       
    # create the list containing all the configurations that should be tested.
    LQtst = [upperlims,lowerlims]

    # create random configurations for the joints of the model.
    for k in range(ntry):
        Qtry = []
        for i in range(len(upperlims)):
            val = random.random()*( ( upperlims[i]-lowerlims[i] ) ) + lowerlims[i]
            Qtry.append(val)
        LQtst.append(Qtry)

    limit_err = lim_err
    max_err = 0
    k = 0

    # test each one of the configurations into LQtst.
    while k < len(LQtst) and max_err <= limit_err:
        qosim = LQtst[k]
        OS_model.setQ(qosim)        
        max_err = QOsimToQURDF(OS_model,urdf_model,urdf_data,D_parts_urdf,lim_err,verbose)
        if max_err <= limit_err:
            k+=1
    
    if max_err > limit_err : 
        print("Error with Q as : ",LQtst[k])
        print("Value of k : ", k)
        print("Value of error : ", max_err)
    else:
        print("Success of test for %i tests. Max error :  "%(k),max_err)
    return(max_err,k)
    

def changeFloating(osim_rpy,L_os):
    """!

    Express the rotation matrix of a .osim free flyer joint into the .urdf
    global referential. 
    This transformation is slightly different from the classic ones, and depends 
    on the order of the rotation axes of the .osim file. This order is expressed 
    thanks to the list L_os. 

    Parameters
    ----------
    @param osim_rpy : List.
        Roll, pitch, yaw angles of the .osim free flyer joint, expressed into
        the .osim global referential.
        
    @param L_os : List
        Order of x-axis, y-axis and z-axis expressed into the .osim file.
        Example : L_os = [2,0,1] : the order is then [z-axis,x-axis,y-axis]
        which means that the angles are [yaw,roll,pitch].

    @return R_urdf : numpy.array (3x3)
        Rotation angles of the free flyer .osim joint expressed into the .urdf
        referential.

    """

    P = np.array([[0.,1.,0.],[1.,0.,0.],[0.,0.,1.]])
    
    R_osim_roll = alg.euler_matrix(osim_rpy[0],0,0)[:3,:3]
    R_urdf_pitch =  np.dot( np.dot(P, R_osim_roll), P.T)
    
    R_osim_pitch =alg.euler_matrix(0, osim_rpy[1], 0)[:3,:3]
    R_urdf_roll = np.dot( np.dot(P, R_osim_pitch), P.T)
    
    R_osim_yaw = alg.euler_matrix(0, 0, osim_rpy[2])[:3,:3]
    R_urdf_yaw = np.dot( np.dot(P, R_osim_yaw), P.T)
    
    L = [ R_urdf_pitch, R_urdf_roll, R_urdf_yaw ]
    
    A = L[L_os[0]]
    B = L[L_os[1]]
    C = L[L_os[2]]

    R_urdf = np.dot( np.dot(A,B), C)
    
    print("Other combinaisons : ")
    print(   np.dot( np.dot(A,C), B) )
    print(   np.dot( np.dot(B,A), C) )
    print(   np.dot( np.dot(B,C), A) )
    print(   np.dot( np.dot(C,A), B) )
    print(   np.dot( np.dot(C,B), A) )
        
    return(R_urdf)    

def QOsimToQURDF(OS_model, urdf_model, urdf_data,D_parts_urdf,lim_err = 1e-6,verbose = False):
    """!
    
    Method used to apply the configuration of a given OsimModel to the .urdf model
    from which it is derived. Then, this method apply this configuration to the .urdf
    model and checks that the placement of joints (expressed into the global referential 
    of the .urdf model) of the .urdf model is consistent with that of the .osim model.
    
    To do so, get the position and the orientation of each of the joints of the
    .osim model, express those informations into the .urdf referential, and then
    compare those informations to the concerned joints of the .urdf model.

    @param OS_model : pyopsim OsimModel class.
        pyopsim object containing all the necessary informations about the .osim
        model.
    
    @param urdf_model : Pinocchio model object.
        Pinocchio object containing all the physical description of the loaded URDF model, 
        including kinematic and inertial parameters defining its structure.
        Please check https://gepettoweb.laas.fr/doc/stack-of-tasks/pinocchio/master/doxygen-html/md_doc_a-features_intro.html 
        for more informations.
        
    @param urdf_data : Pinocchio data object.
         Pinocchio object containing all the values of the loaded URDF model which
         are the result of a computation. (joint configuration, velocity, ...).
         
    @param D_parts_urdf : Dictionary of List.
    
        Dictionary containing, for each joint name of the .urdf model, its joint
        parent index, the joint object associated with this name, and the placement
        of the joint in the referential of its parent joint. 
        An element of this dictionary is expressed as follows : 
            
            D_parts_urdf[joint name] = [ index of the parent joint, 
                                        Pinocchio joint object associated with the joint name,
                                        placement of the joint in the referential of its parent joint ]
            
        
    @param lim_err : float, optional.
        Maximal authorized error between the position and orientation of a .osim joint 
        and a .urdf joint. Otherwise, @return None. an error.
        The default is 1e-6.

    @param verbose : Boolean.
        Activate the print functions of the algorithm. Useful for debugging.
        The default is False.

    @return max_err : float.
        The maximal error encountered during the verification process.
        
    """
    
    max_err = 0.0
    osMpi = np.dot(se3.utils.rotate('z', np.pi/2) , se3.utils.rotate('x', np.pi/2) )
    root_index = findGroundJointId(urdf_model)

    # set q for the urdf model, from the .osim configuration 
    q_urdf = se3.neutral(urdf_model)

    osim_joints_processed = list(OS_model.D_joints_value.keys())
    for joint,name in zip(urdf_model.joints,urdf_model.names):
        if name in osim_joints_processed:
            nq = joint.nq
            idx_q = joint.idx_q
            q_osim_joint = OS_model.D_joints_value[name]
            q_urdf[idx_q:idx_q+nq] = q_osim_joint
            osim_joints_processed.remove(name)
    
    # if necessary, set the value of the free flyer ground joint ( ground --> root 
    # joint of urdf model )   
    # normally, only 6 joints left; otherwise, bug
    if OS_model.D_gd_prop["joint_type"] == "CustomJoint"  and len(osim_joints_processed) == 6:
            # the main difference here is that in Pinocchio, a free flyer joint is
            # expressed thanks to 7 parameters [x y z q_x q_y q_z q_s] (a position vector
            # and a quaternion), whereas the ground joint is expressed in OpenSim with
            # 6 parameters [x y z roll pitch yaw].
            # A special transformation is then necessary.
            
            # find root joint of model in urdf, and save its position in the
            # q vector of the .urdf model.
            gd_joint = urdf_model.joints[root_index]
            nq = gd_joint.nq
            idx_q = gd_joint.idx_q
            
            D_gd_prop_osim = OS_model.D_gd_prop["properties"]
            
            
            # report the values rpy and xyz of the .osim file. 
            osim_rpy = [0,0,0]
            osim_xyz = [0,0,0]
            
            Li = []
            
            # check all the last coordinates of the .osim
            # model (which should correspond to the DOF of the ground joint ).
            for k in range(len(osim_joints_processed)):
                
                name_joint = osim_joints_processed[k]
                
                props_joint = D_gd_prop_osim[name_joint]
                axe = props_joint[1]
                maxi = np.argmax(np.absolute(axe) )         
                # save the value of the coordinate according to the position
                # of the maximum value of its axis and of the type of transformation.
                if "rotation" in props_joint[0]:
                    axe = props_joint[1]
                    maxi = np.argmax(np.absolute(axe) )
                    if maxi == 0:
                        # axis (1,0,0)
                        osim_rpy[0] = -OS_model.D_joints_value[name_joint]    
                        Li.append(0)            
                        
                    elif maxi == 1:
                        # axis (0,1,0)
                        osim_rpy[2] = -OS_model.D_joints_value[name_joint]   
                        Li.append(2)
                        
                    elif maxi ==2:
                        # axis (0,0,1)
                        osim_rpy[1] = -OS_model.D_joints_value[name_joint]   
                        Li.append( 1 )
                
                elif "translation" in props_joint[0]:
                    osim_xyz[maxi] = OS_model.D_joints_value[name_joint]
                    
            # express those values into the .urdf referential and save them into
            # the q vector of the .urdf model.
            p_urdf = np.dot(osMpi, np.array(osim_xyz))
            q_urdf[idx_q:idx_q+3] = p_urdf
            
            R_urdf = changeFloating(osim_rpy,Li)
            
            M_urdf = np.eye(4,4)
            M_urdf[:3,:3] = R_urdf
            
            quat_urdf = alg.quaternion_from_matrix(M_urdf)
            
            x,y,z,s = quat_urdf[0], quat_urdf[1], quat_urdf[2], quat_urdf[3]
            
            quat_urdf = [y,z,s,x]
            q_urdf[idx_q+3:idx_q+7] = quat_urdf

    if verbose:
        # display the configuration vector of the two models.
        pyopsim.printJointsValues(OS_model.osimModel, OS_model.osimState)
        printQ(urdf_model,q_urdf)

    # apply the configuration to the .urdf model.
    se3.forwardKinematics(urdf_model,urdf_data,q_urdf)
        
    if verbose:
        # print the joint positions of the .urdf model and the .osim model.
        printConfig(urdf_model, urdf_data)
        pyopsim.printJointsPositions(OS_model.osimModel,OS_model.osimState)

    # compare the placement of each of the coordinates of the .osim model
    # to the placement of the joints into the .urdf model.    
    D_joints_pos_osim = os2pi_Dpos(OS_model.D_joints_pos)

    # if OS_model.D_gd_prop["joint_type"] == "CustomJoint"  and len(osim_joints_processed) < 6:
    #     # special case 6 DOF, need to correct translation vector for D_joints_pos_osim
    #     gd_j_name = OS_model.D_gd_prop["joint_name"]
    #     L = OS_model.D_joints_coord[gd_j_name] 
    #     OS_model.D_joints_coord[gd_j_name] = L[3:6] + L[0:3]
    #     print(OS_model.D_joints_coord[gd_j_name])
    #     #exit(1)

    gd_joint = urdf_model.joints[root_index]
    gd_nq_urdf = gd_joint.nq
    gd_name_osim = OS_model.D_gd_prop["joint_name"]
    gd_nq_osim = len(OS_model.D_joints_coord[ gd_name_osim ])

    for part_name in D_parts_urdf:
        # handle the issue where the .osim joint associated to the .osim coordinate
        # object has more than one DOF.
        # in that case, as multiple .urdf joints are needed to represent this
        # type of .osim joint, a correction to part_name is added to be
        # sure that the last .urdf joint (which is going to combine all of the
        # previous transformations of the .urdf joints) is taken.
        try:
            osim_joint_name = OS_model.D_coord_joints[part_name]
            if osim_joint_name == OS_model.D_gd_prop["joint_name"]:
                if gd_nq_urdf ==1 and gd_nq_osim == 6:
                    part_name_cons = OS_model.D_joints_coord[osim_joint_name][2]
                else: 
                    part_name_cons = OS_model.D_joints_coord[osim_joint_name][-1]
            else:
                part_name_cons = OS_model.D_joints_coord[osim_joint_name][-1]

        except KeyError:
            # special case.
            osim_joint_name = part_name
            part_name_cons = part_name  

        # get the homogeneous matrix of the .urdf joint.        
        urdf_id = urdf_model.getJointId(part_name_cons)
        jp= urdf_data.oMi[urdf_id]
        M_urdf = np.eye(4,4)
        M_urdf[:3,3] = np.array(jp.translation)
        M_urdf[:3,:3] = jp.rotation
        
        # get the homogeneous matrix of the .osim joint.                
        M_osim = np.eye(4,4)
        osim_trans = D_joints_pos_osim[osim_joint_name][0]
        osim_rot =  D_joints_pos_osim[osim_joint_name][1]        
        M_osim[:3,3] = osim_trans.flatten()
        M_osim[:3,:3] = osim_rot

        # compare those homogeneous matrix.
        err = np.linalg.norm(M_osim-M_urdf)
        
        if verbose:
            print("Name : %s"%part_name)
            print("Difference OSIM / URDF : %f"%err)       
        
        if err > lim_err:
            print("Warning : err != 0")
            print("URDF : ")
            print("Name : %s"%part_name_cons)    
            print(jp.translation)
            print(jp.rotation) 
            print(alg.euler_from_matrix(jp.rotation))
            print(alg.quaternion_from_matrix(jp.rotation))
            print(M_urdf)
            print("OSIM : ")
            print("Name : %s"%osim_joint_name)  
            print(osim_trans)
            print(osim_rot)        
            print(alg.euler_from_matrix(osim_rot))  
            print(alg.quaternion_from_matrix(osim_rot))
            print(M_osim)            
            pyopsim.printJointsDivisionsIntoOneDOF(OS_model.osimModel,OS_model.osimState)
            pyopsim.printJointsPositions(OS_model.osimModel, OS_model.osimState)
            pyopsim.printJointsValues(OS_model.osimModel, OS_model.osimState)
            exit(1)

        if err > max_err:
            max_err = err    

    if verbose:  
        print("Max err : ", max_err)
    return(max_err)


def IKOsimFKURDF(OS_model, urdf_model, urdf_data,D_parts_urdf,verbose = False):
    """!
    Do Inverse Kinematics on the .osim model thanks to the parameters saved into
    the OsimModel object, check if the .urdf model can reach the joint positions with 
    Forward Dynamics.
    
    @param OS_model : pyopsim OsimModel class.
        pyopsim object containing all the necessary informations about the .osim
        model.
    
    @param urdf_model : Pinocchio model object.
        Pinocchio object containing all the physical description of the loaded URDF model, 
        including kinematic and inertial parameters defining its structure.
        Please check https://gepettoweb.laas.fr/doc/stack-of-tasks/pinocchio/master/doxygen-html/md_doc_a-features_intro.html 
        for more informations.
        
    @param urdf_data : Pinocchio data object.
         Pinocchio object containing all the values of the loaded URDF model which
         are the result of a computation. (joint configuration, velocity, ...).
         
    @param D_parts_urdf : Dictionary of List.
    
        Dictionary containing, for each joint name of the .urdf model, its joint
        parent index, the joint object associated with this name, and the placement
        of the joint in the referential of its parent joint. 
        An element of this dictionary is expressed as follows : 
            
            D_parts_urdf[joint name] = [ index of the parent joint, 
                                        Pinocchio joint object associated with the joint name,
                                        placement of the joint in the referential of its parent joint ]

    @param verbose : Boolean.
        Activate the print functions of the algorithm. Useful for debugging.
        The default is False.

    @return max_err : float.
        The maximal error encountered during the verification process.
        
    """

    OS_model.runIK()
    
    err_max = QOsimToQURDF(OS_model, urdf_model, urdf_data,D_parts_urdf,verbose)
    if verbose:
        printConfig(urdf_model,urdf_data)
        pyopsim.printJointsPositions(OS_model.osimModel,OS_model.osimState)
    return(err_max)
    
if __name__ == "__main__":
    # # # ---- wholebody.osim
    
    # ## Complete path to the .osim file (path + .osim file)
    # osim_filename = '/home/auctus/ospi2urdf/models/whole_body/wholebody.osim'
    
    # ## Path to the folder containing Inverse Kinematics configuration file.
    # ik_path = '/home/auctus/opensim_python/OpenSim/XML'
    
    # ## Name of the Inverse Kinematics configuration file.
    # setup_ik_file = "Walk_Mkrs_IK.xml"
    
    # ## Path to a trc file associated with the .osim file.
    # trc_path= '/home/auctus/opensim_python/OpenSim/TRC/Walk_Mkrs.trc'
    
    # ## Path to the .vtp files associated with the .osim model.
    # geometry_path = "/home/auctus/Documents/OpenSim/4.2-2020-11-24-d9c8bb2c/Models/SimulationDataAndSetupFiles-4.0/Geometry"
        
    # ## Complete path to the .urdf file (path + .urdf file)
    # urdf_filename="/home/auctus/catkin_ws/src/human_control/description/wholebody/wholebody_aCol.urdf.xacro"

    # # # ---- upper_body.osim
    
    # ## Complete path to the .osim file (path + .osim file)
    # osim_filename = '/home/auctus/ospi2urdf/models/upper_body/upper_body.osim'
    
    # ## Path to the folder containing Inverse Kinematics configuration file.
    # ik_path = '/home/auctus/opensim_python/OpenSim/XML'
    
    # ## Name of the Inverse Kinematics configuration file.
    # setup_ik_file = "Walk_Mkrs_IK.xml"
    
    # ## Path to a trc file associated with the .osim file.
    # trc_path= '/home/auctus/opensim_python/OpenSim/TRC/Walk_Mkrs.trc'
    
    # ## Path to the .vtp files associated with the .osim model.
    # geometry_path = "/home/auctus/Documents/OpenSim/4.2-2020-11-24-d9c8bb2c/Models/SimulationDataAndSetupFiles-4.0/Geometry"
        
    # ## Complete path to the .urdf file (path + .urdf file)
    # urdf_filename="/home/auctus/catkin_ws/src/human_control/description/upper_body/upper_body_aCol.urdf.xacro"


    # # ---- two_arms_only.osim
    
    # ## Complete path to the .osim file (path + .osim file)
    # osim_filename = '/home/auctus/ospi2urdf/models/two_arms_only/two_arms_only.osim'
    
    # ## Path to the folder containing Inverse Kinematics configuration file.
    # ik_path = '/home/auctus/opensim_python/OpenSim/XML'
    
    # ## Name of the Inverse Kinematics configuration file.
    # setup_ik_file = "Walk_Mkrs_IK.xml"
    
    # ## Path to a trc file associated with the .osim file.
    # trc_path= '/home/auctus/opensim_python/OpenSim/TRC/Walk_Mkrs.trc'
    
    # ## Path to the .vtp files associated with the .osim model.
    # geometry_path = "/home/auctus/Documents/OpenSim/4.2-2020-11-24-d9c8bb2c/Models/SimulationDataAndSetupFiles-4.0/Geometry"
        
    # ## Complete path to the .urdf file (path + .urdf file)
    # urdf_filename="/home/auctus/catkin_ws/src/human_control/description/two_arms_only/two_arms_only_aCol.urdf.xacro"

    # ---- two_arms_only_marks.osim
    
    ## Complete path to the .osim file (path + .osim file)
    osim_filename = '/home/auctus/ospi2urdf/models/two_arms_only/two_arms_only_marks.osim'
    
    ## Path to the folder containing Inverse Kinematics configuration file.
    ik_path = '/home/auctus/opensim_python/OpenSim/XML'
    
    ## Name of the Inverse Kinematics configuration file.
    setup_ik_file = "Walk_Mkrs_IK.xml"
    
    ## Path to a trc file associated with the .osim file.
    trc_path= '/home/auctus/opensim_python/OpenSim/TRC/Walk_Mkrs.trc'
    
    ## Path to the .vtp files associated with the .osim model.
    geometry_path = "/home/auctus/Documents/OpenSim/4.2-2020-11-24-d9c8bb2c/Models/SimulationDataAndSetupFiles-4.0/Geometry"
        
    ## Complete path to the .urdf file (path + .urdf file)
    urdf_filename="/home/auctus/catkin_ws/src/human_control/description/two_arms_only_marks/two_arms_only_marks_aCol.urdf.xacro"

    # # ---- arm26.osim
    
    # ik_path = '/home/auctus/Documents/OpenSim/4.2-2020-11-24-d9c8bb2c/Models/Arm26/OutputReference/InverseKinematics'

    # osim_filename = '/home/auctus/Documents/OpenSim/4.2-2020-11-24-d9c8bb2c/Models/Arm26/OutputReference/arm26.osim'

    # setup_ik_file = 'Setup_IK_arm26_elbow_flex.xml'

    # trc_path = '/home/auctus/Documents/OpenSim/4.2-2020-11-24-d9c8bb2c/Models/Arm26/OutputReference/InverseKinematics/arm26_elbow_flex.trc'

    # geometry_path = "/home/auctus/Documents/OpenSim/4.2-2020-11-24-d9c8bb2c/Models/SimulationDataAndSetupFiles-4.0/Geometry"

    # urdf_filename = "/home/auctus/catkin_ws/src/human_control/description/arm26/arm26_aCol.urdf.xacro"

    # # ---- TwoArm_7ddOS4.osim
    
    # ik_path = '/home/auctus/Documents/OpenSim/4.2-2020-11-24-d9c8bb2c/Models/Arm26/OutputReference/InverseKinematics'
    
    # osim_filename = '/home/auctus/Documents/OpenSim/4.2-2020-11-24-d9c8bb2c/Models/Arm7/TwoArm_7ddOS4.osim'

    # setup_ik_file = 'Setup_IK_arm26_elbow_flex.xml'
    
    # trc_path= '/home/auctus/Documents/OpenSim/4.2-2020-11-24-d9c8bb2c/Models/Arm26/OutputReference/InverseKinematics/arm26_elbow_flex.trc'
    
    # geometry_path = "/home/auctus/Documents/OpenSim/4.2-2020-11-24-d9c8bb2c/Models/Arm7/Geometry"
    
    # urdf_filename = "/home/auctus/catkin_ws/src/human_control/description/TwoArm_7ddOS4/TwoArm_7ddOS4_aCol.urdf.xacro"
    
    
    # Load the urdf model
    
    ## Pinocchio object containing all the physical description of the loaded URDF model.
    urdf_model    = se3.buildModelFromUrdf(urdf_filename)
    ## Pinocchio object containing all the values of the loaded URDF model which are the result of a computation.
    urdf_data = urdf_model.createData()

    ## Constructor of the OsimModel class
    OS_model = pyopsim.OsimModel(osim_filename,geometry_path,ik_path,setup_ik_file,trc_path)

    ## Maximum error output of the staticDifference method.
    max_err = -1
    
    ## Maximum error output of the testDynamicModel method.
    max_err_dyn = -1

    ## Dictionary containing, for each joint name of the .urdf model, its joint
    ## parent index, the joint object associated with this name, and the placement
    ## of the joint in the referential of its parent joint. 
    D_parts_urdf = {}

    ## Number of random tests that should be executed.
    ntry = 1

    max_err,D_parts_urdf = staticDifference(urdf_model,urdf_data,OS_model)

    if max_err < 0.1:
        max_err_dyn,k = testDynamicModel(OS_model, urdf_model, urdf_data,D_parts_urdf, ntry)
        print("\n")
        print("---- Report ----")
        print("Name of the model : ", osim_filename.split("/")[-1])
        print("Maximal error from the static experiment : ", max_err)
        print("Maximal error from the dynamic experiment (number of tests : %d) : "%(k), max_err_dyn)    

    
    print(urdf_model.velocityLimit)
    
    getDoc(urdf_model.frames[0])
    
    id_f = urdf_model.getFrameId("radius_r", se3.BODY)
    
    frame_f = urdf_model.frames[id_f]
    

    
    print(frame_f)
    
    print( urdf_model.frames[ frame_f.previousFrame ].type == se3.JOINT )
    
    name = urdf_model.getJointId(urdf_model.frames[ frame_f.previousFrame ].name)
    
    q = se3.neutral(urdf_model)
    print(se3.computeJointJacobian(urdf_model,urdf_data,q,name))
    
    print(urdf_model.supports[name].tolist())
    
    print(frame_f.parent)
    getDoc(urdf_model.joints[0])
    print(urdf_model.names[0])
    
    getDoc(urdf_data.oMf[0])
    
    print(urdf_data.oMf[0])
    
    print(urdf_model.joints[3])
    
    

 #    B = np.array([[ 0.08681193,  0.99459768, -0.05691345, -0.18528826],
 # [-0.99416398,  0.09016353,  0.05923281,  0.00111865],
 # [ 0.06404433,  0.05143919,  0.99662046, -0.16422084],
 # [ 0.,          0.,          0.,          1.        ]])
    
 #    A = np.array([[ 0.99940777,  0.00153351,  0.03437679, -0.21470178],
 # [-0.00367351,  0.99805228,  0.06227476, -0.01477421],
 # [-0.03421434, -0.06236416,  0.99746684, -0.16076883],
 # [ 0.    ,      0.    ,      0.    ,      1.        ]])
    
 #    K = np.dot(B, np.linalg.inv(A))

    
    #getDoc(urdf_model.supports[name])
    
    #printConfig(urdf_model, urdf_data)
    
    #print( urdf_model.frames[ urdf_model.frames[2].parent ] )
    

    
