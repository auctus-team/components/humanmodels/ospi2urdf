#!/usr/bin/env python
# -*- coding: utf-8 -*-
"""
Created on Fri Jan 22 10:49:31 2021

@author: auctus
"""

import rospy
from sensor_msgs.msg import JointState
from std_msgs.msg import Header,Float64

class Q_publication():
    
    def __init__(self,urdf_path,freq):
        
        # urdf_model    = se3.buildModelFromUrdf(urdf_filename)
    
        # urdf_data = urdf_model.createData()
        self.rate = rospy.Rate(freq)
    
        self.L_joints = ["r_shoulder_elev","r_elbow_flex"]
        
        self.pub = rospy.Publisher("Q_pub", JointState)
        
        Q = [0.5,0.5]
        
        
        
        while not rospy.is_shutdown():
            self.publishQ(Q)
            self.rate.sleep()
            
        
    def publishQ(self,Q):
        
        Lpose = []
        
        for k in range(len(self.L_joints)):
            Lpose.append(Q[k])
        
        js = JointState()
        js.header = Header()
        js.header.stamp = rospy.Time.now()
        js.header.frame_id = "ground"
        js.name = self.L_joints
        js.position = Lpose
        js.velocity = []
        js.effort = []
        
        self.pub.publish(js)
        
        
    
    



if __name__ == "__main__":
    rospy.init_node("test_publi")
    
    # # ---- arm26.osim

    urdf_filename = "/home/auctus/catkin_ws/src/human_control/description/arm26/arm26_aCol.urdf.xacro"

    # # ---- wholebody.osim

    ## Complete path to the .urdf file (path + .urdf file)
    #urdf_filename="/home/auctus/catkin_ws/src/human_control/description/wholebody/wholebody_aCol.urdf.xacro"

    Q_publication(urdf_filename,1)