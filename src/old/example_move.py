import sys
sys.path.append('../../ospi')

# inspired from https://gepettoweb.laas.fr/doc/stack-of-tasks/pinocchio/master/doxygen-html/md_doc_b-examples_i-inverse-kinematics.html

import subprocess
import time
#from ospi 
#import viewer_utils as vw
import wrapper as wr
#import motion_parser as mtp

import pinocchio as se3
from numpy.linalg import norm, solve
import numpy as np
import algebra as alg

# The path to the model meshes
mesh_path='/home/auctus/ospi/models/whole_body/obj'
# The path to the model and the filename
filename='/home/auctus/ospi/models/whole_body/wholebody.osim'
# Create a wrapper specific to the whole-body model 
# The wrapper parse the OpenSim model and builds pinocchio model and data
wb_model = wr.Wrapper(filename, mesh_path, name='whole-body_model1')

# TODO : check autres modeles
print(wb_model.model)

model = wb_model.model

data     = wb_model.data

L_joint = []
L_oMdes = []

 
## -- fingers_r id = 19
JOINT_ID = 19
oMdes = se3.SE3( se3.utils.rotate('y',np.pi)  , np.array([0.0, 0.0, 0.0]))
L_joint.append(JOINT_ID)
L_oMdes.append(oMdes) 


# hip_r = 3
# JOINT_ID = 3
# oMdes = se3.SE3( se3.utils.rotate('x',3.0)  , np.array([0.08, -0.07, 0.86]))

# L_joint.append(JOINT_ID)
# L_oMdes.append(oMdes)

# JOINT_ID_BASE = 1
# oMdes_base = se3.SE3(np.eye(3), np.array([0., 0., 0.93]))


# condition feets on ground
# mtp_r
# JOINT_ID_mtp_r = 6
# oMdes_mtp_r = se3.SE3(np.eye(3), np.array([0.09, 0.06, 0.0]))

# L_joint.append(JOINT_ID_mtp_r)
# L_oMdes.append(oMdes_mtp_r)


# # mtp_l
# JOINT_ID_mtp_l = 11
# oMdes_mtp_l = se3.SE3(np.eye(3), np.array([-0.09, -0.06, 0.0]))

# L_joint.append(JOINT_ID_mtp_l)
# L_oMdes.append(oMdes_mtp_l)



q      = se3.neutral(model)
print("q : ", q.T)
eps    = 1e-4
IT_MAX = 1000
DT     = 1e-1
damp   = 1e-6
 
i=0
while True:
     nb_cons = len(L_joint)
     se3.forwardKinematics(model,data,q)
     #print(  oMdes.actInv(data.oMi[JOINT_ID] )  )
     err = np.zeros((1,1))
     for k in range(len(L_oMdes)):
        dMi= L_oMdes[k].actInv(data.oMi[L_joint[k]] )
        if err.shape == (1,1):
            err = se3.log(dMi).vector
        else:
            err = np.hstack( ( err,  se3.log(dMi).vector ) )
     
     if norm(err) < eps:
         success = True
         break
     if i >= IT_MAX:
         success = False
         break

     J = np.zeros((1,1))
     for k in range(len(L_joint)):
         Jk = se3.computeJointJacobian(model,data,q,L_joint[k])
         if J.shape == (1,1):
             J = Jk
         else:
             J = np.vstack( (J, Jk) )
     
     v = - J.T.dot( solve(J.dot(J.T) + damp * np.eye(6*nb_cons), err)           )

     q = se3.integrate(model,q,v*DT)
          
     if not i % 10:
         print('%d: error = %s' % (i, err.T))
         # print( "J : ", J)
         # print(J.shape)
     i += 1
 
if success:
     print("Convergence achieved!")
else:
     print("\nWarning: the iterative algorithm has not reached convergence to the desired precision")
  
 
print('\nresult: %s' % q.flatten().tolist())
print('\nfinal error: %s' % err.T)

for name, oMi in zip(model.names, data.oMi):
    print(("{:<24} : {: .2f} {: .2f} {: .2f}"
          .format( name, *oMi.translation.T.flat )))
print("----- Rotation -----")
for name, oMi in zip(model.names, data.oMi):
    print(("{:<24} : {: .2f} {: .2f} {: .2f} "
            .format(name, *list(alg.euler_from_rot_mat(oMi.rotation)))) )

# for name, oMi in zip(model.names, data.oMi):
#     print("Name : ", name, " Rot data : ", oMi.rotation)

# print(J.shape)
# print(err.shape)
