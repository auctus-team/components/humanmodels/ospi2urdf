import sys
sys.path.append('../../ospi')

import subprocess
import time
#from ospi 
#import viewer_utils as vw
import wrapper as wr
#import motion_parser as mtp

import pinocchio as se3

urdf_filename="/home/auctus/catkin_ws/src/human_control/description/wholebody/wholebody_aCol.urdf"
# Load the urdf model

urdf_model    = se3.buildModelFromUrdf(urdf_filename)
print(urdf_model)

# model, collision_model, visual_model    = se3.buildModelsFromUrdf(urdf_filename)
# print('model name: ' + model.name)

# # Create data required by the algorithms
# data, collision_data, visual_data  = se3.createDatas(model, collision_model, visual_model)
# # Sample a random configuration
# q        = se3.randomConfiguration(model)
# print('q: %s' % q.T)
# # Perform the forward kinematics over the kinematic tree
# se3.forwardKinematics(model,data,q)
# # Update Geometry models
# se3.updateGeometryPlacements(model, data, collision_model, collision_data)
# se3.updateGeometryPlacements(model, data, visual_model, visual_data)
# # Print out the placement of each joint of the kinematic tree
# print("\nJoint placements:")
# for name, oMi in zip(model.names, data.oMi):
#     print(("{:<24} : {: .8f} {: .8f} {: .8f}"
#           .format( name, *oMi.translation.T.flat )))
# # Print out the placement of each collision geometry object
# print("\nCollision object placements:")
# for k, oMg in enumerate(collision_data.oMg):
#     print(("{:d} : {: .8f} {: .8f} {: .8f}"
#           .format( k, *oMg.translation.T.flat )))
# # Print out the placement of each visual geometry object
# print("\nVisual object placements:")
# for k, oMg in enumerate(visual_data.oMg):
#     print(("{:d} : {: .8f} {: .8f} {: .8f}"
#           .format( k, *oMg.translation.T.flat )))
    

# print(len(model.names))
# print(q.shape)

# print("--- Model doc ---")

# # doc recall for model
# for name, function in model.__class__.__dict__.items():
#     print(' **** %s: %s' % (name, function.__doc__))

# print("--- Data doc ---")

# # doc recall for data
# for name, function in data.__class__.__dict__.items():
#     print(' **** %s: %s' % (name, function.__doc__))

# data.saveToXML("data_tst.xml","xml")

#model.saveToXML("tst.xml","xml")

#model.saveToText("tst.txt")

#model.saveToBinary("tstb.txt")

#print(model.saveToString())
