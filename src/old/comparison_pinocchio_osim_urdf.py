import sys
sys.path.append('../../ospi')

# inspired from https://gepettoweb.laas.fr/doc/stack-of-tasks/pinocchio/master/doxygen-html/md_doc_b-examples_i-inverse-kinematics.html

import subprocess
import time
#from ospi 
#import viewer_utils as vw
import wrapper as wr
#import motion_parser as mtp

import pinocchio as se3
from pinocchio.robot_wrapper import RobotWrapper

from numpy.linalg import norm, solve
import numpy as np
import algebra as alg
import random

import os

import xml.etree.ElementTree as xml

import trials

def getDoc(obj):
    print("---- Documentation ----")
    for name, function in obj.__class__.__dict__.items():
        print(' **** %s: %s' % (name, function.__doc__))  
  

def moveListJoint(Lname,Lcons,model,data,verbose=False):
    """
    

    Parameters
    ----------
    Lname : TYPE
        DESCRIPTION.
    Lcons : 
        One element: 
        [  se3.utils.rotate('x',3.0)  , np.array([0.08, -0.07, 0.86]) ]
    model : TYPE
        DESCRIPTION.
    data : TYPE
        DESCRIPTION.

    Returns
    -------
    None.

    """
    L_joint = []
    L_oMdes = []
    
     
    for k in range(len(Lname)):
        JOINT_ID = model.getJointId(Lname[k])
        oMdes = se3.SE3( Lcons[k][0] , Lcons[k][1])
        L_joint.append(JOINT_ID)
        L_oMdes.append(oMdes) 

    q      = se3.neutral(model)

    eps    = 1e-4
    IT_MAX = 1000
    DT     = 1e-1
    damp   = 1e-6
     
    i=0
    while True:
          nb_cons = len(L_joint)
          se3.forwardKinematics(model,data,q)
          #print(  oMdes.actInv(data.oMi[JOINT_ID] )  )
          err = np.zeros((1,1))
          for k in range(len(L_oMdes)):
            dMi= L_oMdes[k].actInv(data.oMi[L_joint[k]] )
            if err.shape == (1,1):
                err = se3.log(dMi).vector
            else:
                err = np.hstack( ( err,  se3.log(dMi).vector ) )
         
          if norm(err) < eps:
              success = True
              break
          if i >= IT_MAX:
              success = False
              break
    
          J = np.zeros((1,1))
          for k in range(len(L_joint)):
              Jk = se3.computeJointJacobian(model,data,q,L_joint[k])
              if J.shape == (1,1):
                  J = Jk
              else:
                  J = np.vstack( (J, Jk) )
         
          v = - J.T.dot( solve(J.dot(J.T) + damp * np.eye(6*nb_cons), err)           )
    
          q = se3.integrate(model,q,v*DT)
              
          if not i % 10 and verbose:
              print('%d: error = %s' % (i, err.T))
              # print( "J : ", J)
              # print(J.shape)
          i += 1
     
    if success :
          print("Convergence achieved!")
    else:
          print("\nWarning: the iterative algorithm has not reached convergence to the desired precision")
      
    if verbose:
        print('\nresult: %s' % q.flatten().tolist())
        print('\nfinal error: %s' % err.T)
    
        printConfig(model,data)
        
    return(q,data,err)


def tstCombi(urdf_model,data_urdf,osim_model,data_osim):
    D_parts_urdf_combi = {}
    D_parts_urdf = {}
    LR = ['RX','RY','RZ']
    max_err = 0.0
    

    for name, parent, jp, joint in zip(urdf_model.names,urdf_model.parents,urdf_model.jointPlacements,urdf_model.joints):
        if name[-2:] in LR :
            joint_name = name[:-3]
            if joint_name not in D_parts_urdf_combi:
                D_parts_urdf_combi[joint_name] = []
            
            D_parts_urdf_combi[joint_name].append([name,parent,jp])
        else:
            
            model_type = str(joint).split(" ")[0][:-1] 
            
            D_parts_urdf[name] = [parent,joint,jp]
    D_parts_osim_combi = {}
    D_parts_osim = {}
    for name, parent, jp,joint in zip(osim_model.names,osim_model.parents,osim_model.jointPlacements,osim_model.joints):    
        if name in D_parts_urdf_combi:
            D_parts_osim_combi[name] = [parent,jp]
        else:

            D_parts_osim[name] = [parent,joint,jp]
    print(D_parts_osim_combi)
    
    # check joint placement
    # = check that relative placement of joints is ok
    
    for part_name in D_parts_urdf_combi:
        urdf_part = D_parts_urdf_combi[part_name]
        sorted(urdf_part, key=lambda element: element[1])
        #print(urdf_part)
        jp = np.eye(4,4)
        for k in range(len(urdf_part)):
            jpk = np.eye(4,4)
            Djpk = urdf_part[k][2]
            #print(Djpk.translation)
            jpk[:3,3] = np.array(Djpk.translation)
            jpk[:3,:3] = Djpk.rotation
            
            jp = np.dot(jp,jpk)
        
        M_urdf = jp
        jp_osim = D_parts_osim_combi[part_name][1]
        M_osim = np.eye(4,4)
        M_osim[:3,3] = np.array(jp_osim.translation)
        M_osim[:3,:3] = jp_osim.rotation

        print("Name : %s"%part_name)
        # print("Osim placement :", M_osim)
        # print("URDF placement : ", M_urdf)
        err = np.linalg.norm(M_osim-M_urdf)
        print("Difference OSIM / URDF : %f"%err)
        if err > max_err:
            max_err = err
        
    for part_name in D_parts_urdf:
        jp = D_parts_urdf[part_name][-1]
        M_urdf = np.eye(4,4)
        M_urdf[:3,3] = np.array(jp.translation)
        M_urdf[:3,:3] = jp.rotation
        urdf_joint = D_parts_urdf[part_name][-2]
        
        urdf_mt = str(urdf_joint).split(" ")[0][:-1] 
        
        
        jp_osim = D_parts_osim[part_name][-1]
        M_osim = np.eye(4,4)
        M_osim[:3,3] = np.array(jp_osim.translation)
        M_osim[:3,:3] = jp_osim.rotation
        osim_joint = D_parts_osim[part_name][-2]
        
        osim_mt = str(osim_joint).split(" ")[0][:-1] 
         
        if osim_mt != urdf_mt:
            print("Error : osim_mt != urdf_mt for part %s"%part_name)
            print("osim model type : ", osim_mt)
            print("urdf model type : ", urdf_mt)
            print("osim joint : ", osim_joint)
            print("urdf joint : ", urdf_joint)
        
        
        print("Name : %s"%part_name)
        # print("Osim placement :", M_osim)
        # print("URDF placement : ", M_urdf)
        err = np.linalg.norm(M_osim-M_urdf)
        print("Difference OSIM / URDF : %f"%err)        
        if err > max_err:
            max_err = err
            
    # check absolute position of joints
    
    q_osim = se3.neutral(osim_model)
    q_urdf = se3.neutral(urdf_model)
    
    
    se3.forwardKinematics(osim_model,data_osim,q_osim)
    se3.forwardKinematics(urdf_model,data_urdf,q_urdf)
    
    print("*** Absolute placement ***")
    
    for part_name in D_parts_urdf_combi:
        urdf_part = D_parts_urdf_combi[part_name]
        #print(urdf_part)
        
        urdf_id = urdf_model.getJointId(urdf_part[-1][0])
        
        jp = data_urdf.oMi[urdf_id]
        M_urdf = np.eye(4,4)
        M_urdf[:3,3] = np.array(jp.translation)
        M_urdf[:3,:3] = jp.rotation        
        

        osim_id = osim_model.getJointId(part_name)
        jp_osim= data_osim.oMi[osim_id]
        
        M_osim = np.eye(4,4)
        M_osim[:3,3] = np.array(jp_osim.translation)
        M_osim[:3,:3] = jp_osim.rotation

        print("Name : %s"%part_name)
        #print("Osim placement :", M_osim)
        #print("URDF placement : ", M_urdf)
        err = np.linalg.norm(M_osim-M_urdf)
        print("Difference OSIM / URDF : %f"%err)
        if err > max_err:
            max_err = err
        
    for part_name in D_parts_urdf:
               
        urdf_id = urdf_model.getJointId(part_name)
        jp= data_urdf.oMi[urdf_id]
        M_urdf = np.eye(4,4)
        M_urdf[:3,3] = np.array(jp.translation)
        M_urdf[:3,:3] = jp.rotation
        urdf_joint = D_parts_urdf[part_name][-2]

        osim_id = osim_model.getJointId(part_name)
        jp_osim= data_osim.oMi[osim_id]
        
        M_osim = np.eye(4,4)
        M_osim[:3,3] = np.array(jp_osim.translation)
        M_osim[:3,:3] = jp_osim.rotation
        osim_joint = D_parts_osim[part_name][-2]

        print("Name : %s"%part_name)
        #print("Osim placement :", M_osim)
        #print("URDF placement : ", M_urdf)
        err = np.linalg.norm(M_osim-M_urdf)
        print("Difference OSIM / URDF : %f"%err)        
        if err > max_err:
            max_err = err    
    
    # printConfig(urdf_model,data_urdf)
    # printConfig(osim_model,data_osim)
    
    return(max_err,D_parts_osim_combi,D_parts_urdf_combi)

def printConfig(model,data):
    print("----- Translation -----")    
    for name, oMi in zip(model.names, data.oMi):
        print(("{:<24} : {: .2f} {: .2f} {: .2f}"
              .format( name, *oMi.translation.T.flat )))
    print("----- Rotation -----")
    for name, oMi in zip(model.names, data.oMi):
        print(("{:<24} : {: .2f} {: .2f} {: .2f} "
            .format(name, *list(alg.euler_from_rot_mat(oMi.rotation)))) )

def getJointAxis(urdf_model):
    urdf_model.saveToXML('temp.xml',"XML")
    tree = xml.parse('temp.xml')
    root = tree.getroot()
    
    L_axis = []
    for joints in root.findall('./XML/joints'):
        
        item_list = joints.iter('item')
        for item in item_list:
            ID = item.find('i_id')
            if ID != None:
                #print(ID)
                AX = []
                axis_coor_list = item.findall('base_variant/value/axis/data/item')
                for coor in axis_coor_list:
                    AX.append(float(coor.text) )
                L_axis.append(AX)
    os.remove('temp.xml')
    print(L_axis)
    # print(urdf_model)
    return(L_axis)
    


def IKOsimFKUrdf_pin(urdf_model,osim_model,data_urdf,data_osim,D_parts_osim_combi,D_parts_urdf_combi,L_name,t_lim,rot_lim,urdf_filename):
    """
    Do Inverse Kinematics with Osim, check if URDF can
    go to the joint positions with FD.

    Returns
    -------
    None.

    """
    
    L_axis = getJointAxis(urdf_model)
    
    q_osim = se3.neutral(osim_model)
    print(q_osim[23:23+4])
    
    Lcons = []
    
    debug = True
    
    for k in range(len(L_name)):
        if not debug:
        
            M = alg.euler_matrix(rot_lim*(2*random.random()-1),
                                   rot_lim*(2*random.random()-1),
                                   rot_lim*(2*random.random()-1))
            t = np.array([t_lim*(2*random.random()-1),
                          t_lim*(2*random.random()-1),
                          t_lim*(2*random.random()-1)])
            
            rot = M[:3,:3]
    
        else:
            M = alg.euler_matrix(0,
                                  0,
                                   0)
            t = np.array([0,
                          0,
                          0])
            
            rot = M[:3,:3]            
        
        Lcons.append([rot,t])
    

    
    q_osim, data_osim, err_osim = moveListJoint(L_name,Lcons,osim_model,data_osim)
    #print("Orders : ", Lcons)    
    
    # set q_urdf from q_osim
    q_urdf = se3.neutral(urdf_model)
    print(q_urdf[7:10])
    #getDoc(urdf_model)
    
    L_chain = ["back","acromial_l","elbow_l","radioulnar_l","radius_lunate_l","lunate_hand_l","fingers_l","acromial_r","elbow_r","radioulnar_r","radius_lunate_r","lunate_hand_r","fingers_r"]
    D_chain_osim = {}
    D_chain_urdf = {}
    for k in range(len(L_chain)):
        D_chain_urdf[L_chain[k]] = [1000,-1]
        D_chain_osim[L_chain[k]] = [1000,-1]
    
    # idx_back = 1000
    # idx_back_osim = 1000
    
    for joint,name in zip(osim_model.joints,osim_model.names):
        nq = joint.nq
        idx_q = joint.idx_q
        q_osim_joint = q_osim[idx_q:idx_q+nq]
        
        if name in L_chain:
            # idx_back_osim = idx_q
            D_chain_osim[name][0] = idx_q
            D_chain_osim[name][1] = nq
        
        if name in D_parts_urdf_combi:
            #print(q_osim_joint)
            # must change q position to make it work
            #q_osim_joint = 
            x,y,z,s = list(q_osim_joint) # 0,0,0,1
            q2 = [s,x,y,z]
            
            #best results
            #q2 = [s, x,z,-y]

            # if name == "back":
            #     q2_u = q2
            
            r,p,y = alg.euler_from_quaternion(q2)

            rot_mat = np.eye(4,4)

            for k in range(len(D_parts_urdf_combi[name])):
                name_part = D_parts_urdf_combi[name][k][0]
                
                idx_urdf =  urdf_model.getJointId(name_part)
                
                urdf_axis = L_axis[idx_urdf]
                
                urdf_joint = urdf_model.joints[idx_urdf]
                nq_urdf = urdf_joint.nq
                idx_q_urdf= urdf_joint.idx_q                
                # print(name_part)
                # print(idx_q_urdf)
                
                if name in D_chain_urdf:
                    idx_back = D_chain_urdf[name][0]
                    if  idx_back > idx_q_urdf:
                        D_chain_urdf[name][0] = idx_q_urdf
                        D_chain_urdf[name][1] = 3
                
                if name_part[-2:] == "RX":
                    if urdf_axis == []:
                        urdf_axis = [1,0,0]
                    q_urdf[idx_q_urdf:idx_q_urdf+nq_urdf] = r*urdf_axis[0]
                    

                elif name_part[-2:] == "RY":
                    if urdf_axis == []:
                        urdf_axis = [0,1,0]  
                    q_urdf[idx_q_urdf:idx_q_urdf+nq_urdf] = p*urdf_axis[1]

                elif name_part[-2:] == "RZ":
                    if urdf_axis == []:
                        urdf_axis = [0,0,1]      
                    q_urdf[idx_q_urdf:idx_q_urdf+nq_urdf] = y*urdf_axis[2]
                
                
                
        # if name in D_parts_urdf_combi:
        #     print(q_osim_joint)
        #    # must change q position to make it work

        #     x,y,z,s = list(q_osim_joint) # 0,0,0,1
        #     q2 = [s,x,z,-y]
            
            
        #     r,p,y = alg.euler_from_quaternion(q2)

        #     rot_mat = np.eye(4,4)

        #     for k in range(len(D_parts_urdf_combi[name])):
        #         name_part = D_parts_urdf_combi[name][k][0]
                
        #         idx_urdf =  urdf_model.getJointId(name_part)
                
        #         urdf_axis = L_axis[idx_urdf]
                
        #         urdf_joint = urdf_model.joints[idx_urdf]
        #         nq_urdf = urdf_joint.nq
        #         idx_q_urdf= urdf_joint.idx_q                
        #         # print(name_part)
        #         # print(idx_q_urdf)
                
        #         if name in D_chain_urdf :
        #             print(idx_q_urdf)
                
        #         angle = -1
        #         if name_part[-2:] == "RX":
        #             angle = r
        #             if urdf_axis == []:
        #                 urdf_axis = [1,0,0]
                    

        #         elif name_part[-2:] == "RY":
        #             angle = p
        #             if urdf_axis == []:
        #                 urdf_axis = [0,1,0]                    

        #         elif name_part[-2:] == "RZ":
        #             angle = y
        #             if urdf_axis == []:
        #                 urdf_axis = [0,0,1]                      
                
                
                
        #         quat = alg.quaternion_about_axis(angle, urdf_axis)
        #         rot_mat = np.dot(alg.quaternion_matrix(quat),rot_mat)
                
        #         if name in D_chain_urdf:
        #             idx_back = D_chain_urdf[name][0]
        #             if  idx_back > idx_q_urdf:
        #                 D_chain_urdf[name][0] = idx_q_urdf
        #                 D_chain_urdf[name][1] = 3
        #     quat_ax = alg.quaternion_from_matrix(rot_mat)
        #     rax,pax,yax = alg.euler_from_matrix(rot_mat)
            
        #     for k in range(len(D_parts_urdf_combi[name])):   
        #         name_part = D_parts_urdf_combi[name][k][0]
                
        #         idx_urdf =  urdf_model.getJointId(name_part)
                
        #         urdf_axis = L_axis[idx_urdf]
                
        #         urdf_joint = urdf_model.joints[idx_urdf]
        #         nq_urdf = urdf_joint.nq
        #         idx_q_urdf= urdf_joint.idx_q                        
        #         if name_part[-2:] == "RX":
        #             q_urdf[idx_q_urdf:idx_q_urdf+nq_urdf] = rax

        #         elif name_part[-2:] == "RY":
        #             q_urdf[idx_q_urdf:idx_q_urdf+nq_urdf] = pax

        #         elif name_part[-2:] == "RZ":
        #             q_urdf[idx_q_urdf:idx_q_urdf+nq_urdf] = yax
            
        else:
            
            idx_urdf =  urdf_model.getJointId(name)
            urdf_joint = urdf_model.joints[idx_urdf]
            nq_urdf = urdf_joint.nq
            
            idx_q_urdf= urdf_joint.idx_q
            
            urdf_axis = L_axis[idx_urdf]
            
            q_urdf[idx_q_urdf:idx_q_urdf+nq_urdf] = q_osim_joint
            
        if name in D_chain_urdf :
            idx_back = D_chain_urdf[name][0]
            if  idx_back > idx_q_urdf:
                D_chain_urdf[name][0] = idx_q_urdf
                if name in D_parts_urdf_combi:
                    D_chain_urdf[name][1] = 3
                else:
                    D_chain_urdf[name][1] = nq_urdf
            
       
    urdf_model_tst = se3.buildModelFromUrdf(urdf_filename)
    data_urdf_tst = urdf_model_tst.createData()     
    q_urdf_tst, data_urdf_tst, err_urdf_tst = moveListJoint(L_name,Lcons,urdf_model_tst,data_urdf_tst)        
    
    se3.forwardKinematics(urdf_model,data_urdf,q_urdf)
    

    # printConfig(urdf_model_tst,data_urdf_tst)    
    # printConfig(urdf_model,data_urdf)

    #printConfig(osim_model,data_osim)
    
    for k in range(len(L_chain)):
        print("Name : ", L_chain[k])
        idx_back = D_chain_urdf[L_chain[k]][0]
        nq_urdf = D_chain_urdf[L_chain[k]][1]
        idx_back_osim = D_chain_osim[L_chain[k]][0]
        nq_osim = D_chain_osim[L_chain[k]][1]
        print("idx_back : %i ; nq_back : %i"%( idx_back, nq_urdf) )
        print("urdf_tst : ", q_urdf_tst[idx_back:idx_back+nq_urdf])    
        print("urdf_from_osim : ",q_urdf[idx_back:idx_back+nq_urdf])  
        if nq_urdf > 1:
            x,y,z,s = list(q_osim[idx_back_osim : idx_back_osim+nq_osim])
            q2 = [s,x,y,z]
            r0,p0,y0 = alg.euler_from_quaternion(q2)
            print("rpy osim : ",[r0,p0,y0])
            r,p,y = list(q_urdf_tst[idx_back:idx_back+nq_urdf])
            print("quat urdf_tst : ",alg.inv_rpytoQUAT(r, p, y))
            print("quat osim : ",q2)
            r2,p2,y2 = list(q_urdf[idx_back:idx_back+nq_urdf])
            print("quat urdf from osim : ",alg.inv_rpytoQUAT(r2, p2, y2))
        else:
            print("angle osim : ",q_osim[idx_back_osim : idx_back_osim+nq_osim])
        print("****")
    # ERROR : PRINT CONFIG PAS IDENTIQUES SUR TRANSLATION
    # PIRE ENCORE, OBJECTIFS PLUS ATTEINTS AVEC URDF
    # --> trouver raison
    
    # print(q_urdf[idx_back:idx_back+3])
    # r,p,y = list(q_urdf[idx_back:idx_back+3])
    # print(alg.rpytoQUAT(r,p,y) )
    # print(q_osim[idx_back_osim:idx_back_osim+4])
    # print(alg.euler_from_quaternion(q_osim[idx_back_osim:idx_back_osim+4]) )
    printConfig(urdf_model_tst,data_urdf_tst)  
    printConfig(urdf_model,data_urdf)
    #check all translations and rotations are ok
    
    
    
    # print("q URDF : ", q_urdf.flatten().T)
    
    # print(np.allclose(q_urdf,q_bef))
    
    # check pour back
    
    

    # getDoc(data_osim.oMi[0])
    
    # M_urdf = {}
    
    # L_urdf_combi = []
    
    # # # put into a better format

    # for name, oMi_urdf in zip(urdf_model.names, data_urdf.oMi):    
    #     if name[:-3] in D_parts_urdf_combi:
    
    #         if name[:-3] not in M_urdf:

    #             M_urdf[name[:-3]] = [None,[0,0,0]]
    #             L_urdf_combi.append(name[:-3])
                
            
    #         M = np.eye(4,4)
    #         M[:3,:3] = oMi_urdf.rotation
            
    #         if np.linalg.norm(oMi_urdf.translation) !=0:
                  # # verif si bien ok 
    #             M_urdf[name[:-3]][0] =  oMi_urdf.translation 
            
            
    #         if name[:-3] == "back":
    #             # print(M[:3,3])
    #             # print("back : ", M_urdf[name[:-3]])
            
    #             if name[-2:] == "RX":
    #                 print("RX : ", alg.euler_from_rot_mat(M[:3,:3]))
    #                 #JOINT_ID = urdf_model.getJointId("back_RX")
    #                 #print(urdf_model.jointPlacements[JOINT_ID] )
    #             elif name[-2:] == "RY":
    #                 print("RY : ", alg.euler_from_rot_mat(M[:3,:3]))
    #             elif name_part[-2:] == "RZ":
    #                 print("RZ : ", alg.euler_from_rot_mat(M[:3,:3]))
                
    #             #new_rot = np.dot(M[:3,:3],oMi_urdf.rotation)
                
    #             #M_urdf[name[:-3]][:3,:3] = new_rot
    #     else:
    #         M = np.eye(4,4)
    #         M[:3,:3] = oMi_urdf.rotation
    #         M[:3,3] = oMi_urdf.translation
    #         M_urdf[name] = M
    
    # #print(M_urdf)
    # let's check translation and rotation
    # max_err_t = 0
    # max_err_r = 0
    # for name, oMi_osim in zip(osim_model.names, data_osim.oMi):
    #     if name == "back":
    #     #if True:   
            
    #         JOINT_ID = urdf_model.getJointId("back_RX")
    #         print(urdf_model.jointPlacements[JOINT_ID] )
            
    #         trans_osim = oMi_osim.translation.T.flat
    #         trans_urdf = M_urdf[name][:3,3]
            
    #         print("osim matrix : ", oMi_osim.rotation)
    #         print("urdf matrix : ", M_urdf[name][:3, :3])

    #         rpy_osim = np.array( alg.euler_from_rot_mat(oMi_osim.rotation) )
    #         rpy_urdf = np.array( alg.euler_from_rot_mat( M_urdf[name][:3,:3] ) )

    #         print("osim rpy : ", rpy_osim)
    #         print("urdf rpy : ", rpy_urdf)            

    #         err_t = (trans_osim-trans_urdf)
           
    #         err_r = (rpy_osim-rpy_urdf)
           
    #         nerr_t =  np.linalg.norm(err_t)
    #         nerr_r = np.linalg.norm(err_r)
           
    #         print("{:<24} : {: .2f} {: .2f} {: .2f} |  {: .2f} {: .2f} {: .2f}"
    #                   .format(name, err_t[0], err_t[1], err_t[2], err_r[0], err_r[1], err_r[2]    ) )
           
           
    #         if nerr_t > max_err_t:
    #             max_err_t = nerr_t
    #             #print("Max t : ", name)
               
    #         if nerr_r > max_err_r :
    #             max_err_r = nerr_r
    #             #print("Max r : ",name)
    
    # print("Max error t : ", max_err_t)
    # print("Max error r : ", max_err_r)    
    
    return(None)


def IKOsimFKURDF(OS_model, urdf_model, urdf_data):
    """
    
    Do Inverse Kinematics with Osim, check if URDF can
    go to the joint positions with FD.

    Parameters
    ----------
    osim_filename : TYPE
        DESCRIPTION.
    ik_path : TYPE
        DESCRIPTION.
    setup_ik_file : TYPE
        DESCRIPTION.
    trc_path : TYPE
        DESCRIPTION.
    geometry_path : TYPE
        DESCRIPTION.

    Returns
    -------
    None.

    """
    
    # IK with osim    
    
    getJointAxis(urdf_model)
    
    
    # set q for urdf_model
    
    q_urdf = se3.neutral(urdf_model)
    
    

    
    for joint,name in zip(urdf_model.joints,urdf_model.names):
        nq = joint.nq
        idx_q = joint.idx_q
        q_osim_joint = OS_model.D_joints_value[name]
        q_urdf[idx_q:idx_q+nq] = q_osim_joint
    
    # set first for ground joint ( base --> ground )   
    


## ---- wholebody.osim
            
# # The path to the model meshes
# mesh_path='/home/auctus/ospi/models/whole_body/obj'
# # The path to the model and the filename
# filename='/home/auctus/ospi/models/whole_body/wholebody.osim'
# # Create a wrapper specific to the whole-body model 
# # The wrapper parse the OpenSim model and builds pinocchio model and data
# wb_model = wr.Wrapper(filename, mesh_path, name='whole-body_model1')

#urdf_filename="/home/auctus/catkin_ws/src/human_control/description/wholebody/wholebody_aCol.urdf"

## ---- arm26.osim

ik_path = '/home/auctus/Documents/OpenSim/4.2-2020-11-24-d9c8bb2c/Models/Arm26/OutputReference/InverseKinematics'

osim_filename = '/home/auctus/Documents/OpenSim/4.2-2020-11-24-d9c8bb2c/Models/Arm26/OutputReference/arm26.osim'  

setup_ik_file = 'Setup_IK_arm26_elbow_flex.xml'

trc_path= '/home/auctus/Documents/OpenSim/4.2-2020-11-24-d9c8bb2c/Models/Arm26/OutputReference/InverseKinematics/arm26_elbow_flex.trc'

geometry_path = "/home/auctus/Documents/OpenSim/4.2-2020-11-24-d9c8bb2c/Models/SimulationDataAndSetupFiles-4.0/Geometry"

urdf_filename = "/home/auctus/catkin_ws/src/human_control/description/arm26/arm26_aCol.urdf"


# Load the urdf model

urdf_model    = se3.buildModelFromUrdf(urdf_filename)
data_urdf = urdf_model.createData()



# robot_urdf_model    = RobotWrapper.BuildFromURDF(urdf_filename)
# urdf_model = robot_urdf_model.model
# data_urdf = robot_urdf_model.data

print("URDF : ")


print(urdf_model)



# osim_model_pin = wb_model.model

# data_osim_pin    = wb_model.data






getJointAxis(urdf_model)

q_urdf = se3.neutral(urdf_model)

print(q_urdf.shape)

#print(OS_model.D_body_pos)



# getDoc(urdf_model.jointPlacements)
# print(urdf_model.jointPlacements.__getinit

# print(urdf_model.joints.__len__)

# getDoc(data_urdf)

# print(data_urdf.h)

#print( urdf_model.parents.__getstate__() )

#print(urdf_model.subtrees[7].__getstate__())

# print(urdf_model.__getstate__())
# --> les axis semblent bien enregistres ici

# for frame in urdf_model.frames:
#     print(frame)



# getDoc(urdf_model.joints[n])
# print(urdf_model.names[n], urdf_model.joints[n])
#print(urdf_model.frames[id_frame].placement)

#getDoc(se3.JointModelRevoluteUnaligned())
# print(se3.JointModelRevoluteUnaligned(0,0,1).axis )
# getDoc(data_urdf.oMi[n])
# print(data_urdf.oMi[n])

#print(str(urdf_model.joints[0]).split(" ")[0][:-1] )
# print(data_urdf.oMi)

# for frame in urdf_model.frames:
#     # contient a la fois les referentiels pour les joints et pour les body_parts
#     print(frame)
    
# getDoc(frame)

#print(urdf_model.name)

# for name,joint in zip(urdf_model.names, urdf_model.joints):
#     print("Name : {:<24} : joint : ".format(name), joint)

# max_err, D_parts_osim_combi, D_parts_urdf_combi = tstCombi(urdf_model,data_urdf,osim_model,data_osim)

# # q = se3.neutral(urdf_model)
# # se3.forwardKinematics(urdf_model,data_urdf,q)
# # printConfig(urdf_model,data_urdf)

# print("Maximum error : ", max_err)
# if max_err < 1e-2:
#     Lname = ['fingers_r','mtp_r','mtp_l','fingers_l']
#     t_lim = 0.4
#     rot_lim = 0.5
#     # --> abandon. Aucune idee de raison pbme.
    #IKOsimFKUrdf(urdf_model,osim_model,data_urdf,data_osim,D_parts_osim_combi,D_parts_urdf_combi,Lname,t_lim,rot_lim,urdf_filename)



# I = [0.,0,0,1]
# quat = I

# for k in range(1):
#     r,p,y = alg.euler_from_quaternion(quat)
    
#     print(r,p,y)
#     quat = alg.inv_rpytoQUAT(r, p, y)
    
#     #quat = alg.rpytoQUAT(r, p, y)
#     print(quat)

# r = -np.pi
# p = 0
# y = 0
# print(alg.inv_rpytoQUAT(r, p, y))

# Lname = ['fingers_r','mtp_r','mtp_l','fingers_l']
# Lcons = [  [np.eye(3)  , np.array([0.0, 0.0, 2.0])],
#            [np.eye(3), np.array([0.09, 0.06, 0.0])],
#            [np.eye(3), np.array([-0.09, -0.06, 0.0]) ],
#            [np.eye(3)  , np.array([-0.1, 0.2, 0.9])],
#           ]

# moveListJoint(Lname, Lcons, urdf_model, data_urdf)
# moveListJoint(Lname, Lcons, osim_model, data_osim)
# tst : combiner joints representant meme chose (multiplication matrice),
# voir si retombe sur nos pattes
# voir aussi avec configuration random (bornes cpdt pr tjrs reussir) si renvoie memes resultats



# print( urdf_model.nbodies) 

# for name in urdf_model.names:
#     # nom des joints du modele
#     print(name)

# for parent in urdf_model.parents:
#     # nbe assos ac le joint parent du joint considere
#     print(parent)
    
# for jp in urdf_model.jointPlacements:
#     # placement du joiny par rapport a parent de ref
#     print(jp)    

# nqtot = 0
# for jn,name in zip(urdf_model.joints,urdf_model.names):
#     print("Joint name : ", name, " ; details : ", jn )
#     nqtot += jn.nq
# print(nqtot)
# print(urdf_model)


# 



# q_urdf = se3.neutral(urdf_model)
# print('q_urdf: %s' % q_urdf.shape)


# # osim part
# print(" osim : ")
# print(wb_model.model)
# osim_model = wb_model.model
# q_osim = se3.neutral(osim_model)
# print('q_osim: %s' % q_osim.shape)



# L_joint = []
# L_oMdes = []

 
# ## -- fingers_r id = 19
# JOINT_ID = 19
# oMdes = se3.SE3( se3.utils.rotate('y',np.pi)  , np.array([0.0, 0.0, 1.0]))
# L_joint.append(JOINT_ID)
# L_oMdes.append(oMdes) 


# # hip_r = 3
# # JOINT_ID = 3
# # oMdes = se3.SE3( se3.utils.rotate('x',3.0)  , np.array([0.08, -0.07, 0.86]))

# # L_joint.append(JOINT_ID)
# # L_oMdes.append(oMdes)

# # JOINT_ID_BASE = 1
# # oMdes_base = se3.SE3(np.eye(3), np.array([0., 0., 0.93]))


# # condition feets on ground
# # mtp_r
# JOINT_ID_mtp_r = 6
# oMdes_mtp_r = se3.SE3(np.eye(3), np.array([0.09, 0.06, 0.0]))

# L_joint.append(JOINT_ID_mtp_r)
# L_oMdes.append(oMdes_mtp_r)


# # mtp_l
# JOINT_ID_mtp_l = 11
# oMdes_mtp_l = se3.SE3(np.eye(3), np.array([-0.09, -0.06, 0.0]))

# L_joint.append(JOINT_ID_mtp_l)
# L_oMdes.append(oMdes_mtp_l)



# q      = se3.neutral(model)
# print("q : ", q.T)
# eps    = 1e-4
# IT_MAX = 1000
# DT     = 1e-1
# damp   = 1e-6
 
# i=0
# while True:
#      nb_cons = len(L_joint)
#      se3.forwardKinematics(model,data,q)
#      #print(  oMdes.actInv(data.oMi[JOINT_ID] )  )
#      err = np.zeros((1,1))
#      for k in range(len(L_oMdes)):
#         dMi= L_oMdes[k].actInv(data.oMi[L_joint[k]] )
#         if err.shape == (1,1):
#             err = se3.log(dMi).vector
#         else:
#             err = np.hstack( ( err,  se3.log(dMi).vector ) )
     
#      if norm(err) < eps:
#          success = True
#          break
#      if i >= IT_MAX:
#          success = False
#          break

#      J = np.zeros((1,1))
#      for k in range(len(L_joint)):
#          Jk = se3.computeJointJacobian(model,data,q,L_joint[k])
#          if J.shape == (1,1):
#              J = Jk
#          else:
#              J = np.vstack( (J, Jk) )
     
#      v = - J.T.dot( solve(J.dot(J.T) + damp * np.eye(6*nb_cons), err)           )

#      q = se3.integrate(model,q,v*DT)
          
#      if not i % 10:
#          print('%d: error = %s' % (i, err.T))
#          # print( "J : ", J)
#          # print(J.shape)
#      i += 1
 
# if success:
#      print("Convergence achieved!")
# else:
#      print("\nWarning: the iterative algorithm has not reached convergence to the desired precision")
  
 
# print('\nresult: %s' % q.flatten().tolist())
# print('\nfinal error: %s' % err.T)

# for name, oMi in zip(model.names, data.oMi):
#     print(("{:<24} : {: .2f} {: .2f} {: .2f}"
#           .format( name, *oMi.translation.T.flat )))
# print("----- Rotation -----")
# for name, oMi in zip(model.names, data.oMi):
#     print(("{:<24} : {: .2f} {: .2f} {: .2f} "
#             .format(name, *list(alg.euler_from_rot_mat(oMi.rotation)))) )

