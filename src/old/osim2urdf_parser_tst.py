#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Fri Dec  4 10:33:53 2020

@author: auctus
"""
import sys
sys.path.append('../ospi')

import numpy as np
import pinocchio as se3
import os 
import utils

import stl
from stl import mesh

import math

import algebra as alg

import xml.etree.ElementTree as xml

import subprocess

class Osim2URDF():
    """
    Transform a .osim file into a .urdf file.
    """
    
    def __init__(self,osim_path,vtp_path,filename, ros_package_path, exactly_visual, exactly_collision, correc_stl_dic, verbose=True, secure_collision=True, trans_plugin=True, ident_size = 4):
        """
        Constructor of the Osim2URDF class.
        
        Parameters
        ----------
        osim_path : String
            Path to the .osim file. It should not end by a "/".
            
        vtp_path : String
            Path to the .vtp files of the model. Those files will then
            be converted into .stl files. It should not end by a "/".
            (If you already have the .stl files but not the .vtp, you can
             copy the folder (named stl) containing those files into the 
             following path : 
                 (ros_package_path)/description/(name of .osim) )
            
        filename : String
            Name to the .osim file. Same name will be used for the URDF file.
                
        ros_package_path : String
            Path to the ROS package associated with the control of the URDF model.
            It should not end by a "/".
            Intervening directly in the ROS package during the realization of the URDF 
            is necessary for the use of the joint_state_publisher package, in order to avoid 
            having to handwrite all the joints in the URDF.

        exactly_visual : Boolean
            True : try to represent the visual aspect of the model from the .stl files
            False : use approximations according to the dimensions of the .stl file.
            
        exactly_collision : Boolean
            True : try to represent the collision aspect of the model from the .stl files
            False : use approximations according to the dimensions of the .stl file.
            
        correc_stl_dic: Dictionnary of dictionnaries
            Sometimes, the stl files could have some mistakes into them, especially
            about the position of their mass center. In order to fix those mistakes,
            the user can add in this dictionary the names of the stl files or body parts 
            causing problems, as well as their true position depending on whether we are 
            dealing with an exact or approximate representation of the human body.
            Then, during the writing of the URDF file, the algorithm will check those
            informations and use them if necessary.
            
            To sum up, here's a example of an element of this dictionnary :
                
                'name of stl file' : {True_Case : [ coordinates xyz of the mass center of element in case true ],
                                      False_Case : [ coordinates xyz of the mass center of element in case false ]
                                      }
                
                'name of body part' : {True_Case : [ coordinates rpy xyz of the origin of all the .stl parts of the
                                                    body part in case true ],
                                      False_Case : [ coordinates rpy xyz of the origin of all the .stl parts of the
                                                    body part in case false ]
                                      }   
                
            With True_Case and False_Case respectively the boolean "True" and "False".
            If no correction is needed in True_Case or False_Case, the list should be empty.
            
            Ex : 
                 'stl file bofoot': {True:[],
                                    False:[-0.00004618,  0.025, -0.00304539]
                                    },
                 'body part fingers_r':{True:[0.,0.,0.,0.,0.,0.], 
                                        False:[]
                                        },                
        verbose : Boolean
            Activate the print functions of the algorithm.
            

        secure_collision : Boolean
            True : in the case the exactly_collision parameter is activated, use an approximation
            of the .stl to secure the collision parameters of the stl file.
            False : deactivate this option.
            
        trans_plugin : Boolean
            Activates the writing of the transmission tag and of the .yaml file
            containing the controllers of every joint. 
        
        ident_size : Int
            Allows to set the indentation size in the realized files (.urdf, .yaml, and .launch)

        Returns
        -------
        None.

        """
        
        # set the booleans
        
        self.secure_collision = secure_collision
        self.exactly_visual = exactly_visual
        self.exactly_collision = exactly_collision    
        self.trans_plugin = trans_plugin
        self.verbose = verbose
        
        
        # set the path
        
        while osim_path[-1] == "/":
            osim_path = osim_path[:-1]
        self.osim_path = osim_path
        
        while ros_package_path[-1] == "/":
            ros_package_path = ros_package_path[:-1]
        
        self.package_path = ros_package_path
        self.vtp_path = vtp_path
        self.osim_filename = filename
        
        self.model_name = self.osim_filename.split(".")[0]
        self.urdf_path = self.package_path+"/description/"+ self.model_name  + "/"     

        # set useful variables
        
        # correction dictionnary
        
        self.correc_stl_dic = correc_stl_dic        
        
        # names
        
        self.package_name = self.package_path.split("/")[-1]     
        self.pwd_begin_launchfile = "../utilis_files/begin_launcher.txt"
        self.launchfile_name = "human_control.launch"
        
        options = ""
        if not self.exactly_visual:
            options+="_aVis"
        if not self.exactly_collision:
            options+="_aCol"
        if self.exactly_collision and self.secure_collision:
            options+="_sCol"
            
        self.urdf_filename = self.model_name + options+ ".urdf"

        # set of strings
        
        self.position_controller = ""
        self.launchfile_add = []
        
        # indentation variables
                        
        self.ident =0
        self.ident_size = ident_size
        self.ident_trans = 0
        
        # fake links infos
        
        self.fake_mass = 0.0001
        self.fake_in= 0.0003

        # Define global variables, useful for the transform osim -> urdf
                
        self.pymodel = readOsim(self.osim_path+"/"+self.osim_filename)

        self.osMpi = np.dot(se3.utils.rotate('z', np.pi/2) , se3.utils.rotate('x', np.pi/2) ) 
        # corresponds to np.array([[0.,0.,1.],[1.,0.,0.],[0.,1.,0.] ])
                
        self.osMpi_rpy = alg.euler_from_rot_mat(self.osMpi)

        test = True
        if test :
            self.joint_models, self.joint_transformations = utils._parse2PinocchioJoints(self.pymodel,self.osMpi)      
        else:
            self.joint_models, self.joint_transformations = utils._parse2PinocchioJoints_ori(self.pymodel)      
        
        # Define variables useful for the parsing

        self.L_rot = ["JointModelSpherical", "JointModelRX", "JointModelRY", "JointModelRZ", "JointModelRevoluteUnaligned", "JointModelComposite" ]
        
        self.idx = []
        for joint in range(0,len(self.pymodel['Joints'])):
            parent_name = self.pymodel['Joints'][joint][0]['parent_body'][0]
            self.idx.append(parent_name)
        
        self.dic_body = {}
        self.bpart = []
        for body in self.pymodel['Bodies']:
            body_name = body['name'][0]
            self.bpart.append(body_name)
            self.dic_body[body_name] = []

        # create a Matrix containing the maximal effort between each
        # body parts (beta)
        
        self.M_forces = -1*np.ones(  ( len(self.bpart), len(self.bpart) ) )
        
        self.checkMuscularEffort()

        # then, set the minimum in case we do not have efforts informations
        # for two body parts that are linked by a joint.

        mini = np.inf
        for i in range(len(self.bpart)):
            for j in range( len(self.bpart) ):
                if self.M_forces[i,j] != -1 and self.M_forces[i,j] < mini:
                    mini = self.M_forces[i,j]
        
        self.approx_effort = mini
        
        self.approx_velocity = 1.0
        
        # Let's go! 
        # First, create the folder where the urdf file will be.
       
        try:
            os.mkdir(self.package_path+"/description/")
        except OSError:
            print("Folder"+self.package_path+"/description" +" already existing.")        
        try:
            os.mkdir(self.package_path+"/description/"+self.model_name)
        except OSError:
            print("Folder"+self.package_path+"/description/"+ self.model_name +" already existing.") 
       

        # transformation : turn joint_transformations into urdf coordinate system
        
        k = 0
        while test==False and k <( len( self.joint_transformations ) ) :
            
            #print(k,self.joint_transformations)
            joint_axis = self.joint_transformations[k]
            joint_axis_urdf = joint_axis
            #print(joint_axis_urdf.shape)
            if joint_axis_urdf.shape[1] ==0:
                k+=1

            else:
                #print("Bef : ", joint_axis_urdf)
                joint_axis_urdf = np.dot( joint_axis_urdf, self.osMpi.T )
                #print("Aft : ", joint_axis_urdf)
                self.joint_transformations[k] = joint_axis_urdf
                k+=1
        
        # fix an issue of the .osim parser (adding fake 'Visuals' objects, that
        # are not linked to any .stl file)  
        
        k = 0
        while k < (len(self.pymodel['Visuals'])):
            #print(self.pymodel['Visuals'][k][1])
            if len(self.pymodel['Visuals'][k][1 ]['geometry_file']) ==0:
                del(self.pymodel['Visuals'][k])
            else:
                k+=1

        if len(self.pymodel['Visuals'])!=len(self.pymodel['Bodies']):
            self.pymodel['Visuals'] = [None]+ self.pymodel['Visuals']          

        # check that the .osim file has all the range infos that are necessary
        
        k = 0

        while k < len(self.pymodel['Joints']):
            
            joint_range = self.pymodel['Joints'][k][1]['range']
            joint_dim = self.joint_models[k][2].nv
            if len(joint_range) != joint_dim:
                print(joint_range)
                print(self.joint_models[k])
            k+=1
        

        #create the .stl files
        
        self.createSTLfiles()
        
        # print(se3.utils.rotate('y', -np.pi/2) )
        # print(alg.euler_matrix(0, np.pi, -np.pi/2)[:3,:3])
        # M2 = alg.euler_matrix(0, np.pi, -np.pi/2)[:3,:3]
        # M = np.dot( alg.euler_matrix(0, np.pi, -np.pi/2)[:3,:3],se3.utils.rotate('y', -np.pi/2) )
        # print(M)

        # print(np.dot(np.dot(self.osMpi, M), self.osMpi.T) )
        
        # print(np.dot(np.dot(self.osMpi,se3.utils.rotate('y', -np.pi/2)), self.osMpi.T ))
        
        # print(np.dot(np.dot(self.osMpi,M2), self.osMpi.T ))        
        
        # M5 = np.dot(np.dot(self.osMpi,M2), self.osMpi.T )
        
        # print(alg.euler_matrix(np.pi/2,np.pi/2,np.pi/2))
        # M3 = alg.euler_matrix(np.pi/2,np.pi/2,np.pi/2)[:3,:3]
        # print(alg.euler_from_rot_mat(M3))
        
        # print( alg.euler_matrix(np.pi/2,0,np.pi/2)[:3,:3]         )
        
        # print(alg.euler_from_rot_mat( np.array([[0.,0.,1.],[1.,0.,0.],[0.,1.,0.]])   ))
        
        # M4 = alg.euler_matrix(np.pi/2, np.pi, 0.)[:3,:3]
        
        # print(M4)
        # print(np.dot(np.dot(self.osMpi,M4), self.osMpi.T ))    
        
        

        
        # print(alg.euler_matrix(np.pi, 0., -np.pi/2)[:3,:3])
        
        # print(alg.euler_from_rot_mat(M5) )  
        
        # print(alg.euler_matrix(np.pi, 0., 3*np.pi/2)[:3,:3])        
        
        # M6= np.array([[0.,0.,-1.],[0.,1.,0.],[1.,0.,0.]])
        
        # print(alg.euler_from_rot_mat(M6) )  
        # #print(M5)
        
        # print(alg.euler_matrix(0., np.pi/2,0)[:3,:3])        
        # print( alg.euler_matrix(np.pi/2,0,np.pi/2)[:3,:3]    )        
        
        
        # M7 = -alg.euler_matrix(np.pi/2,0,np.pi/2)[:3,:3]     
        
        # print(alg.euler_from_rot_mat(M7))
                
        # M8= np.array([[0.,0.,-1.],[-1.,0.,0.],[0.,1.,0.]])
        # print(alg.euler_from_rot_mat(M8))    
        
        
        
        #print(alg.euler_from_rot_mat(M5))
        print( alg.euler_matrix(np.pi/2,0,-np.pi)[:3,:3]    )  
        print( alg.euler_matrix(np.pi/2,0,np.pi)[:3,:3]    )  
        print( alg.euler_matrix(0,np.pi/2,0)[:3,:3]    )  

        M6= np.array([[0.,0.,1.], [1.,0.,0.],[0.,1.,0.]])
        
        print(np.dot(np.dot(self.osMpi.T,M6), self.osMpi) )
        print( alg.euler_matrix(np.pi/2,-np.pi/2,np.pi/2)[:3,:3]    )  
        M7 = alg.euler_matrix(np.pi/2,np.pi/2,-np.pi/2)[:3,:3]
        

        
        S = np.array([[0.,0.,1.], [1.,0.,0.],[0.,1.,0.]])
        V = np.array([[1.,0.,0.], [0.,1.,0.],[0.,0.,1.]])
        
        Beg =  alg.euler_matrix(np.pi/2,0.0,0)[:3,:3] 
        
        Fin =  alg.euler_matrix(np.pi/2,0,np.pi)[:3,:3]

        Fin_c = np.dot( np.dot(S,Beg), V)
        
        print("Start : ", Beg)
        print("Fin_c : ", Fin_c)
        print("Fin : ", Fin)

        
        print(alg.euler_from_rot_mat(Fin))        
        
        print(np.dot(np.dot(S,S),S))
        exit(1)
        #write the .urdf

        self.writeModelToURDF()


    def createSTLfiles(self):
        """
        Create the .stl files from the .vtp files, and save them at the
        same location as the .urdf (if those files were not already created).
        This is done thanks to a temporary bash script.
        Warning : this step can take a few minutes!

        Returns
        -------
        None.

        """
        
        folder = False
        try:
            os.mkdir(self.urdf_path+"stl")
        except OSError:
            print("STL files already created. No need to make them another time.")
            folder = True
        
        if not folder:
            with open(self.urdf_path+ "convert_vtp2stl.sh","w+") as f:
                f.write("#!/bin/bash \n\n")
                f.write('PATH2VTP='+self.vtp_path+" \n")
                f.write('PATH2STL='+self.urdf_path+"stl \n")
                f.write("source ~/vmtk/build/Install/vmtk_env.sh \n")
                f.write("cd $PATH2VTP \n")
                f.write("for i in *.vtp \n")
                f.write("    do \n")
                f.write('        vmtksurfacewriter -ifile $(basename $i .${i##*.}).vtp -ofile $PATH2STL"/"$(basename $i .${i##*.})".stl" \n')
                f.write("    done \n")
            
            os.system("chmod +x "+ self.urdf_path+ "convert_vtp2stl.sh")
            subprocess.call(self.urdf_path+ "convert_vtp2stl.sh")
            os.remove(self.urdf_path+ "convert_vtp2stl.sh")
        self.stl_path = self.urdf_path+"stl"


    def checkMuscularEffort(self):
        """
        Create a Matrix containing the maximal effort between each
        body parts.
        
        Stills in Beta, as this information may not be useful for Rviz or
        Pinocchio. 
        
        As for now, this is done by realizing an average of the product of the
        optimal_fiber_length and the max_isometric_force of each muscle between
        the concerned body parts.

        Returns
        -------
        None.

        """
        
        occ_part = np.zeros( ( len(self.bpart), len(self.bpart) ) )
        for forces in self.pymodel['Forces']:
            ofl = float(forces[0]['optimal_fiber_length'][0])
            mif =float( forces[0]['max_isometric_force'][0])
            val = ofl*mif
            Lbody = []
            for points in forces[1]:
                body_name = points['body'][0]
                if body_name not in Lbody:
                    Lbody.append(body_name)
            #print("Lbody : ", Lbody)
            if len(Lbody) < 2:
                print("error : not a even number of bodies")
            else:
                #print("Lbody : ", Lbody)
                for i in range(len(Lbody)):
                    numb_i = self.bpart.index(Lbody[i])
                    for j in range(i+1,len(Lbody)):
                        numb_j = self.bpart.index(Lbody[j])
                        eff_max =  (self.M_forces[numb_i,numb_j]*occ_part[numb_i,numb_j] + ofl*mif)/(occ_part[numb_i,numb_j]+1)
                        occ_part[numb_i,numb_j] += 1
                        occ_part[numb_j,numb_i] += 1
                        
                        # # add to debug
                        #eff_max = eff_max*10
                        
                        self.M_forces[numb_i,numb_j] = eff_max
                        self.M_forces[numb_j,numb_i] = eff_max       
                        

        # verification step : check that every part of body has a force
        for k in range(len(self.bpart)):
            L = [ x for x in range(len(self.bpart)) if self.M_forces[k,x ] !=-1 ]
            if len(L) == 0:
                print("Warning : part %s do not have any muscular links with any other body part."%self.bpart[k])

    def writeModelToURDF(self):
        """
        
        Write the URDF file.
        Can also write the files useful for the package controller_manager 
        (control.yaml) and for the ros package (.launch).
        
        Returns
        -------
        None.

        """
        
        with open(self.urdf_path +self.urdf_filename,"w+") as f: 
            
            f.write('<?xml version="1.0" ?>\n\n')
            
            f.write('<robot name="' + self.model_name+ '"> \n')
            
            self.ident+= self.ident_size
            
            if self.trans_plugin:
                # write gazebo plugins
                f.write(self.ident*" "+"<gazebo>\n")
                self.ident+= self.ident_size
                f.write(self.ident*" "+'<plugin name="gazebo_ros_control" filename="libgazebo_ros_control.so">\n')
                self.ident+= self.ident_size
                f.write(self.ident*" "+'<robotNamespace>/'+ self.model_name  + '</robotNamespace>\n')
                self.ident-= self.ident_size
                f.write(self.ident*" "+'</plugin>\n')
                self.ident-= self.ident_size
                f.write(self.ident*" "+'</gazebo>\n \n')      
                try:
                    os.mkdir(self.package_path+"/config/")
                except OSError:
                    print("Folder"+self.package_path+"/config" +" already existing.")
                
                try:
                    os.remove(self.package_path+"/config/"+ self.model_name+'_control.yaml')
                except OSError:
                    print("File"+self.package_path+"/config/"+ self.model_name+'_control.yaml' +" is new.")
                
                with open(self.package_path+'/config/'+ self.model_name+'_control.yaml',"a+") as f2:
                    f2.write(self.model_name+":\n\n")
                    self.ident_trans +=self.ident_size
                    f2.write(self.ident_trans*" "+'joint_state_controller:\n')
                    
                    self.ident_trans +=self.ident_size
                    f2.write(self.ident_trans*" "+'type: joint_state_controller/JointStateController\n')
                    f2.write(self.ident_trans*" "+'publish_rate: 50\n\n')            
                    self.ident_trans -=self.ident_size

 
            # first, let's create all the links, for each link
            
            for body in range(0,len(self.pymodel['Bodies'])): # self.pymodel['Joints']
                if self.pymodel['Bodies'][body]['name'][0] !="ground":
                    self.writeLinkVisuals(f,body)            
                else:
                    # we create a special fake link for the ground (or universe)
                    f.write(self.ident*" "+ '<link name="ground" /> \n \n')
                    
                    
            # then, let's write the joints, each joint at a time

            for joint in range(0,len(self.pymodel['Joints'])):
                self.writeJoint(f,joint)          
            

            
            self.ident-= self.ident_size            
            f.write("</robot> \n")

        self.writeLaunchFile()    
        return(None)
    
    
    def writeLaunchFile(self):
        """
        
        Write the launchfile associated with the URDF file.
        
        NB : A small part of this launchfile is static and written into the 
        "begin_launcher.txt" file. Should you need to add more nodes to the
        launchfile of the URDF, you can add them into this file.

        Returns
        -------
        None.

        """
        
        with open(self.pwd_begin_launchfile,"r+") as f3:
            beg_content = f3.read()
        
        with open(self.package_path+"/launch/"+self.launchfile_name,"w+") as f:
            f.write(beg_content)
            self.ident+=self.ident_size
            
            str_robot_desc = '<param name="robot_description" command="cat '
            str_robot_desc += self.urdf_path+self.urdf_filename +'"/>\n\n'
            f.write(self.ident*" "+ str_robot_desc)
            
            pwd_control = self.package_path+'/config/'+ self.model_name+'_control.yaml'
            f.write(self.ident*" "+ '<rosparam file="'+pwd_control+ '" command="load"/>\n \n')
            
            f.write(self.ident*" "+ '<node name="robot_state_publisher" pkg="robot_state_publisher" type="robot_state_publisher" respawn="false" output="screen">\n')
            self.ident+=self.ident_size
            f.write(self.ident*" "+ '<remap from="/joint_states" to="/'+self.model_name+'/joint_states" />\n')
            self.ident-=self.ident_size
            f.write(self.ident*" "+ '</node>\n\n')
            
            f.write(self.ident*" "+ '<node name="joint_state_publisher" pkg="joint_state_publisher" type="joint_state_publisher" respawn="false" output="screen">\n')
            self.ident+=self.ident_size
            f.write(self.ident*" "+ '<remap from="/joint_states" to="/'+self.model_name+'/joint_states" />\n')
            f.write(self.ident*" "+ '<param name="use_gui" value="true" />\n')
            self.ident-=self.ident_size
            f.write(self.ident*" "+ '</node>\n\n')            
            
            
            str_controller = '<node name="controller_spawner" pkg="controller_manager" type="spawner" respawn="false" output="screen" ns="/'
            str_controller+= self.model_name + '" args="'
            str_controller+= self.position_controller+ ' joint_state_controller"/>\n\n'
            f.write(self.ident*" "+ str_controller)
            
            str_spawn_urdf = '<node name="spawn_urdf" pkg="gazebo_ros" type="spawn_model" args="-file '
            str_spawn_urdf += self.urdf_path+self.urdf_filename + ' -urdf -x 0 -y 0 -z 2 -model '
            str_spawn_urdf += self.model_name + '" />\n\n'
            f.write(self.ident*" "+ str_spawn_urdf)
            
            for string in self.launchfile_add:
                f.write(self.ident*" "+ string)
            
            self.ident-=self.ident_size
            f.write("</launch>")
            
            

    def writeJoint(self,f,joint):
        """
        Write one Joint into the URDF file f.

        Parameters
        ----------
        f : File
            The URDF file currently written.
        joint : Int
            The number associated with the joint currently being processed.

        Returns
        -------
        None.

        """

        
        # First, let's get all the necessary informations about our joint
                
        joint_axis_names = list(filter(None,self.pymodel['Joints'][joint][2]['coordinates']))
        
        child = self.pymodel['Joints'][joint][0]['child_body'][0]
        joint_name = self.pymodel['Joints'][joint][0]['name'][0] 
        parent = self.pymodel['Joints'][joint][0]['parent_body'][0]       
        joint_model = self.joint_models[joint][2]
        joint_limits = self.pymodel['Joints'][joint][1]['range']

        # From OpenSim to Pinocchio
        joint_placement = se3.SE3.Identity()
        
        r = np.matrix(self.pymodel['Joints'][joint][0]['orientation_in_parent'][0],dtype = np.float64).T
        
        joint_placement.rotation = se3.utils.rpyToMatrix( np.dot(self.osMpi , r) )
                
        t = self.pymodel['Joints'][joint][0]['location_in_parent'][0]   
             
        joint_placement.translation = np.dot( self.osMpi ,  np.matrix(t,dtype=np.float64).T )        
        
        joint_axis = self.joint_transformations[joint]
        
        joint_model_type = str( type(joint_model) ).split(".")[-1][:-2] 
        
        
        if self.verbose:
            print('Joint Name: ', joint_name)
            print('Parent Name: ', parent)
            print("Child name :", child )
            print("Joint axis names : ", joint_axis_names)
            print( 'Joint Model: ', joint_model ) 
            print( 'Joint Limits: ', joint_limits )
            print("Joint orientation (w.r.t parent frame) : ", joint_axis.astype(float) ) #joint_axis[0,1])
            print("Joint origin (w.r.t parent frame): ", joint_placement ) #joint_placement.translation[2]) 
            print("Joint origin rot Euler : ", alg.euler_from_rot_mat(joint_placement.rotation))
            print("Joint Model Type : ", joint_model_type)
            print("Joint Model nv : ", joint_model.nv)
            print("*****")        
        
        # check if the joint is a rotation type 
        if joint_model_type in self.L_rot:
            
            if joint_model_type != "JointModelRevoluteUnaligned" :
                # check if the axis should be expressed with int or
                # float values
                
                n_j_ax=joint_axis.astype(float).astype(int)

                if np.sum(n_j_ax) == 0 :
                    #bug : fix it 
                    joint_axis = joint_axis.astype(float)
                else:
                    joint_axis = n_j_ax
                

            # check if it only has one DOF
            if joint_model.nv == 1:
                joint_placement_rot = alg.euler_from_rot_mat(joint_placement.rotation)
                joint_axis_name = joint_axis_names[0]
                
                self.writeOneDofRevoluteJoint(f,joint_axis_name,parent,child,joint_limits, joint_axis, joint_placement.translation,joint_placement_rot)
            else:
                
                self.writeMultipleDimensionJoint(f,joint_axis_names,parent,child,joint_limits, joint_axis, joint_placement,joint_model.nv)
        
        elif joint_model.nv==6:
            # means that we are in case JointModelFreeFlyer
            # --> equivalent to floating joint
            if self.exactly_visual : 
                #R = self.osMpi
                R = np.eye(3,3)
            else:
                R = np.eye(3,3)

            self.writeFloatingJoint(f,joint_name,parent,child, joint_placement.translation,R)
                

        
    def writeFloatingJoint(self,f,joint_name,parent,child, joint_placement_trans,R):      
        """
        Write a floating Joint into URDF file.
        (Normally, only joint from ground to 1st part of body)
        
        https://github.com/ros/robot_model/issues/188 : according to this,
        there is an issue with floating joints for joint_state_publisher. In order
        to fix this, we'll just add a static transform publisher to the launchfile.
        
        
        NB : we also write a floating joint into the urdf, just to be sure that the
        joint_state_publisher node won't consider that there is two links without any
        connexion, thus two root links.

        Parameters
        ----------
        f : File
            The URDF file currently written.
            
        joint_name : String
            The name associated with the joint currently being processed.
            
        parent : String
            Name of the parent body part of the joint.
            
        child : String
            Name of the child body part of the joint.
            
        joint_placement_trans : Array 1x3
            Position xyz of the origin of the joint.
        
        R : numpy.array (3x3)
            Rotation matrix of the origin of the joint

        Returns
        -------
        None.
        
        """

        # into URDF
        
        f.write(self.ident*" "+ '<joint name="' + joint_name + '" type="floating"> \n \n')
        self.ident+=self.ident_size
        
        p = joint_placement_trans
        
        r,pc,y =  alg.euler_from_rot_mat(R)
        
        str_p = "{: .8f} {: .8f} {: .8f}".format( p[0], p[1], p[2] )
        
        str_rpy = "{: .8f} {: .8f} {: .8f}".format( r, pc, y )

        f.write(self.ident*" "+ '<origin xyz="'+ str_p +  '" rpy="'+str_rpy  +'" /> \n')   
        f.write(self.ident*" "+ '<parent link="'+ parent +  '"/> \n')           
        f.write(self.ident*" "+ '<child link="'+ child +  '"/> \n \n')                  
        
        self.ident-=self.ident_size        
        f.write(self.ident*" "+"</joint> \n \n")
        
        # into launchfile
        
        rate = 1
                
        p = joint_placement_trans
        
        str_p = "{: .8f} {: .8f} {: .8f} {: .8f} {: .8f} {: .8f}".format(p[0], p[1], p[2],r, pc, y )
        
        launch_str = '<node pkg="tf" type="static_transform_publisher" name="%s" '%joint_name
        launch_str+= 'args="%s %s %s %d" />\n\n'%(str_p,parent,child,rate)

        self.launchfile_add.append(launch_str)
           
        
    def writeOneDofRevoluteJoint(self,f,joint_name,parent,child,joint_limits, joint_axis, joint_placement_trans, joint_placement_rot):
        """        
        
        Write one DOF revolute Joint into URDF file

        Parameters
        ----------
        f : File
            The URDF file currently written.
            
        joint_name : String
            The name associated with the joint currently being processed.
            
        parent : String
            Name of the parent body part of the joint.
            
        child : String
            Name of the child body part of the joint.
            
        joint_limits : List ( format : [ ['-1.57'],['1.57'] ] )
            List containing the angular limits of the joints.
        
        joint_axis : Array 3x1
            The joint axis specified in the joint frame. This is the axis of rotation for the
            revolute joint, specified in the joint frame of reference.
            
        joint_placement_trans : Array 1x3
            Position xyz of the origin of the joint.
            
        joint_placement_rot : Array 1x3
            Orientation rpy of the origin of the joint.

        Returns
        -------
        None.

        """
            
        f.write(self.ident*" "+ '<joint name="' + joint_name + '" type="revolute"> \n \n')
        self.ident+=self.ident_size
        
        p = joint_placement_trans
        r = joint_placement_rot
        
        str_p = "{: .8f} {: .8f} {: .8f}".format( p[0], p[1], p[2] )
        str_r = "{: .8f} {: .8f} {: .8f}".format( r[0], r[1], r[2] )

        f.write(self.ident*" "+ '<origin xyz="'+ str_p +  '" rpy="'+str_r+'" /> \n')   
        f.write(self.ident*" "+ '<parent link="'+ parent +  '"/> \n')           
        f.write(self.ident*" "+ '<child link="'+ child +  '"/> \n')          
        
        str_axis = "{: .8f} {: .8f} {: .8f}".format( joint_axis[0,0], joint_axis[0,1], joint_axis[0,2] )
        
        
        f.write(self.ident*" "+ '<axis xyz="'+ str_axis +  '"/> \n')      
        
        if child[-3:-1] == "_f":
            child = child[:-3]
        if parent[-3:-1] == "_f":
            parent = parent[:-3]
        
        # write efforts
        
        numb_i = self.bpart.index(parent)
        numb_j = self.bpart.index(child)
        
        if self.M_forces[numb_i,numb_j] !=-1:
            eff = str(self.M_forces[numb_i,numb_j])
        else:
            eff = str(self.approx_effort )
        
        veloc = str(self.approx_velocity)

        f.write(self.ident*" "+ '<limit effort="'+ eff + '" velocity="' + veloc +  '" lower="' + joint_limits[0][0] + '" upper="' + joint_limits[0][1] +  '" /> \n \n')

        self.ident-=self.ident_size        
        f.write(self.ident*" "+"</joint> \n \n")
        
        if self.trans_plugin:
            self.writeTransmission(f,joint_name)
            
    def writeTransmission(self,f,joint_name):
        """
        
        Write the transmission elements and the controller elements of the joint,
        in order to be used by the ros_control package.

        Parameters
        ----------
        f : File
            The URDF file currently written.
        joint_name : String
            The name associated with the joint currently being processed.

        Returns
        -------
        None.

        """
        
        with open(self.package_path+'/config/'+ self.model_name+'_control.yaml',"a+") as f2:
            # modify control.yaml
            self.position_controller+= joint_name+"_position_controller "
            
            f2.write(self.ident_trans*" "+joint_name+"_position_controller:\n")
            self.ident_trans+=self.ident_size
            f2.write(self.ident_trans*" "+"type: effort_controllers/JointPositionController\n")
            f2.write(self.ident_trans*" "+"joint: "+ joint_name+ "\n")
            f2.write(self.ident_trans*" "+"pid: {p: 100.0, i: 0.01, d: 10.0}\n\n")
            self.ident_trans-=self.ident_size
        
        # write transmission
        f.write(self.ident*" "+ '<transmission name="trans_'+ joint_name+'">\n')
        self.ident+=self.ident_size
        f.write(self.ident*" "+ '<type>transmission_interface/SimpleTransmission</type>\n')
        f.write(self.ident*" "+ '<joint name="'+ joint_name +'">\n')
        self.ident+=self.ident_size
        f.write(self.ident*" "+ '<hardwareInterface>EffortJointInterface</hardwareInterface>\n')
        self.ident-=self.ident_size
        f.write(self.ident*" "+ '</joint>\n')
        f.write(self.ident*" "+ '<actuator name="motor_'+ joint_name +'">\n')
        self.ident+=self.ident_size
        f.write(self.ident*" "+ '<hardwareInterface>EffortJointInterface</hardwareInterface>\n')
        f.write(self.ident*" "+ '<mechanicalReduction>1</mechanicalReduction>\n')        
        self.ident-=self.ident_size
        f.write(self.ident*" "+ '</actuator>\n')        
        self.ident-=self.ident_size
        f.write(self.ident*" "+ '</transmission>\n \n')               
        
    

    def writeMultipleDimensionJoint(self,f,joint_axis_names,parent,child,joint_limits, joint_axis, joint_placement,dim):
        """
        Write a revolute joint with multiple DOF. 
        As the URDF format do not accept to write a joint with multiple DOF and limits
        on its DOF, the trick is to write multiple one DOF joint between the parent
        and the child link (or body part). To do so, some fake links are created between
        the parent joint and the child joint, and each of these fake links has a
        one DOF joint.

        Parameters
        ----------
        f : File
            The URDF file currently written.
            
        joint_axis_names : List of String
            The list of names of each DOF of the joint.
            
        parent : String
            Name of the parent body part of the joint.
            
        child : String
            Name of the child body part of the joint.
            
        joint_limits : List ( format : [ [ ['0.0'],['3.14'] ], [ ['-1.57'],['1.57'] ], ... ])
            List containing the angular limits of the joints on all its DOF.
        
        joint_axis : Array 3xDOF
            The joint axis specified in the joint frame on each DOF. Those are the axis of rotation for the
            revolute joint, specified in the joint frame of reference.
            
        joint_placement_trans : Array DOFx3
            Position xyz of the origin of the joint on each DOF axis.
            
        dim : Int
            Number of DOF of the joint.

        Returns
        -------
        None.

        """

        pname = parent
        j_p_t = joint_placement.translation
        j_p_r = alg.euler_from_rot_mat(joint_placement.rotation)
        
        for k in range(0,dim):
            
            cur_joint_axis = joint_axis[k]
            name_fj = joint_axis_names[k]
            j_l = [joint_limits[k] ]

            if k == dim-1:
                cname = child 
            else:
                cname = child + "_f%i"%(k+1)
                self.writeFakeLink(f,cname)
                
            self.writeOneDofRevoluteJoint(f, name_fj, pname, cname, j_l, cur_joint_axis, j_p_t,j_p_r)
            
            pname = cname
            j_p_t = np.zeros( (1,3) ).flatten()
            j_p_r = np.zeros( (1,3) ).flatten()

        
    
    def writeFakeLink(self,f,body_name):
        """
        In the case of a joint with more than one DOF (in order to
        keep the limits into the URDF file), we create a fake link which will
        be used to express the transform. 
        
        Here, the fake link is written consecutively to the fake joint.         

        Parameters
        ----------
        f : File
            The URDF file currently written.
            
        body_name : String
            The name associated with the fake link currently written.

        Returns
        -------
        None.

        """

        f.write(self.ident*" "+ '<link name="' + body_name + '"> \n \n')
        self.ident+=self.ident_size
        
        # Time to get infos about the body
        mass = self.fake_mass
        mass_center = np.zeros((3,1))
        inertia_matrix = self.fake_in*np.eye(3,3)
        
        ixx = str(inertia_matrix[0,0])
        iyy = str(inertia_matrix[1,1])
        izz = str(inertia_matrix[2,2])
        ixy = str(inertia_matrix[0,1])
        ixz = str(inertia_matrix[0,2])
        iyz = str(inertia_matrix[1,2])

        # Let's put inertial first
        f.write(self.ident*" "+ "<inertial>\n")
        self.ident+= self.ident_size
    
        f.write(self.ident*" "+ '<mass value="'+ str(mass)+  '"/> \n')

        str_mass_center = "{: .8f} {: .8f} {: .8f}".format( mass_center[0,0], mass_center[1,0], mass_center[2,0] )
        
        f.write(self.ident*" "+ '<origin xyz="'+ str_mass_center+  '" rpy="0 0 0" /> \n')   
        
        f.write(self.ident*" "+ '<inertia ixx="'+ixx+ '" iyy="'+iyy+'" izz="'+izz +'" ixy="' + ixy + '" ixz="' + ixz + '" iyz="' + iyz + '"/> \n')

        self.ident-= self.ident_size                
        f.write(self.ident*" " + "</inertial> \n \n")

        # everything set for this body!
        self.ident-=self.ident_size        
        f.write(self.ident*" "+"</link> \n \n")        
    
    
    def writeLinkVisuals(self,f,body):
        """
        
        Write a link with multiple STL files, as the .urdf format supports
        this feature (which avoids having to write a large number of useless 
        links for each .stl file.).

        Parameters
        ----------
        f : File
            The URDF file currently written.
        body : Int
            The number associated with the body part currently being processed.

        Returns
        -------
        None.

        """
        
        body_name =self.pymodel['Bodies'][body]['name'][0]
            
        f.write(self.ident*" "+ '<link name="' + body_name + '"> \n \n')
        self.ident+=self.ident_size
        
        # Time to get infos about the body
        mass = np.float64(self.pymodel['Bodies'][body]['mass'][0])
        mass_center = np.dot( self.osMpi , np.matrix(self.pymodel['Bodies'][body]['mass_center'][0], dtype = np.float64).T )
        inertia_matrix = np.matrix(self.pymodel['Bodies'][body]['inertia'][0], dtype = np.float64)
        
        ixx = str(inertia_matrix[0,0])
        iyy = str(inertia_matrix[1,1])
        izz = str(inertia_matrix[2,2])
        ixy = str(inertia_matrix[0,1])
        ixz = str(inertia_matrix[0,2])
        iyz = str(inertia_matrix[1,2])
                
        # Let's put inertial first
        f.write(self.ident*" "+ "<inertial> \n ")
        self.ident+= self.ident_size
    
        #print(mass)
        f.write(self.ident*" "+ '<mass value="'+ str(mass)+  '"/> \n')

        str_mass_center = "{: .8f} {: .8f} {: .8f}".format( mass_center[0,0], mass_center[1,0], mass_center[2,0] )
        
        f.write(self.ident*" "+ '<origin xyz="'+ str_mass_center+  '" rpy="0 0 0" /> \n')   
        
        f.write(self.ident*" "+ '<inertia ixx="'+ixx+ '" iyy="'+iyy+'" izz="'+izz +'" ixy="' + ixy + '" ixz="' + ixz + '" iyz="' + iyz + '"/> \n')

        self.ident-= self.ident_size                
        f.write(self.ident*" " + "</inertial> \n \n")

        # then, visuals and collisions
        # let's add all available visuals in one link
        try:
            scale_factors_visu = np.matrix(self.pymodel['Visuals'][body][0]['scale_factors'][0], np.float64).T
            #scale_factors_visu = np.dot( self.osMpi , (np.matrix(self.pymodel['Visuals'][body][0]['scale_factors'][0], np.float64)).T )
            #scale_factors_visu = np.asarray(scale_factors_visu.T)[0]
        except Exception:
            scale_factors_visu = [1.0,1.0,1.0]

    
        # add to visuals list
        
        if self.verbose:
            print("---- Body name : %s ----"%body_name)
            print("Meshes : ", self.pymodel['Visuals'][body][1]['geometry_file'])
        
        for i_mesh in range(0, len(self.pymodel['Visuals'][body][1]['geometry_file']) ):
            #take infos about the .stl file
            
            visual_name = os.path.splitext(self.pymodel['Visuals'][body][1]['geometry_file'][i_mesh])[0]

            filename = self.stl_path+ '/'+visual_name+'.stl'
            ros_filename ="/" + self.package_name + "/description/"+self.model_name+'/stl/'+visual_name+'.stl'                       
            
            try:
                scale_factors_mesh = (np.matrix(self.pymodel['Visuals'][body][1]['scale_factors'][i_mesh], np.float64)).T 
                scale_factors = np.asarray(scale_factors_mesh.T)[0]                
                # scale_factors_mesh = np.dot( self.osMpi , (np.matrix(self.pymodel['Visuals'][body][1]['scale_factors'][i_mesh], np.float64)).T )
                # scale_factors = np.asarray(scale_factors_mesh.T)[0]
            except Exception:
                scale_factors = scale_factors_visu
            
            str_scale_factors= "{: .8f} {: .8f} {: .8f}".format( scale_factors[0], scale_factors[1], scale_factors[2] )

            # if needed, get an approximation of the .stl file
            
            type_obj = []
            carac_obj = []
            
            if self.exactly_collision==False or self.exactly_visual==False or (self.exactly_collision== True and self.secure_collision== True):
                type_obj,carac_obj = self.getSTLApproximation(filename,body_name)
                cur_xyz = self.dic_body[body_name][-1]
            
            # write visual tag
            
            # check if there is a need of correction
            
            if body_name in self.correc_stl_dic and len(self.correc_stl_dic[body_name][self.exactly_visual])>0:
                
                Ltrans = self.correc_stl_dic[body_name][self.exactly_visual]
                
                transform = np.array([Ltrans]).reshape((6,1))
            else:
            
                transform = np.matrix(self.pymodel['Visuals'][body][1]['transform'][i_mesh],dtype=np.float64).T
                    
                transform[3:6] = np.dot( self.osMpi , transform[3:6] )

                R_visu_osim = alg.euler_matrix(transform[0,0], transform[1,0], transform[2,0])[:3,:3]
                
                R_visu_urdf = np.dot( np.dot(self.osMpi, R_visu_osim), self.osMpi.T)
                
                ru,pu,yu = alg.euler_from_rot_mat(R_visu_urdf)
                
                transform[0,0] = ru
                transform[1,0] = pu
                transform[2,0] = yu
                
                #transform[0:3] = np.dot(  self.osMpi , transform[0:3] )

            if self.exactly_visual:

                if np.sum(transform[0:3,0]) == 0:
                    transform[0,0] = self.osMpi_rpy[0]
                    transform[1,0] = self.osMpi_rpy[1]
                    transform[2,0] = self.osMpi_rpy[2]
                elif np.linalg.norm(transform[0:3,0]-np.array([[np.pi/2],[0.],[-np.pi]]))< 1e-4:
                    # case where the orientation of the .vtp file has been changed;
                    # beta for now.
                    transform[0,0] = np.pi/2
                    transform[1,0] = np.pi/2
                    transform[2,0] = np.pi/2                 
                # else:
                #     print(transform[0:3,0])
                #     print(transform[0:3,0].shape)
                #     print(np.linalg.norm(transform[0:3,0]-np.array([-np.pi/2,0.,np.pi])))
                #     exit(1)

                str_rpy_transform = "{: .8f} {: .8f} {: .8f}".format(
                    transform[0, 0], transform[1, 0], transform[2, 0])
                str_xyz_transform = "{: .8f} {: .8f} {: .8f}".format( transform[3,0], transform[4,0], transform[5,0] )
                
                
                self.writeExact("visual", visual_name, ros_filename, f, str_rpy_transform, str_xyz_transform, str_scale_factors)
            
            else:
                if (self.verbose): 
                    print("File : %s"%filename)
                    print("Type object : %s"%type_obj)        
                
                str_xyz_transform = "{: .8f} {: .8f} {: .8f}".format( cur_xyz[0], cur_xyz[1], cur_xyz[2] )
                str_rpy_transform = "{: .8f} {: .8f} {: .8f}".format( transform[0,0], transform[1,0], transform[2,0] )
                self.writeApproximation("visual",visual_name, f, str_rpy_transform, str_xyz_transform, str_scale_factors,type_obj,carac_obj)
            
            # write collision tag
            
            # check if there is a need of correction
            
            if body_name in self.correc_stl_dic and len(self.correc_stl_dic[body_name][self.exactly_collision])>0:
                Ltrans = self.correc_stl_dic[body_name][self.exactly_collision]
                transform = np.array([Ltrans]).reshape((6,1))
            else:
            
                transform = np.matrix(self.pymodel['Visuals'][body][1]['transform'][i_mesh],dtype=np.float64).T
                    
                transform[3:6] = np.dot( self.osMpi , transform[3:6] )
                
                R_col_osim = alg.euler_matrix(transform[0,0], transform[1,0], transform[2,0])[:3,:3]
                
                R_col_urdf = np.dot( np.dot(self.osMpi, R_col_osim), self.osMpi.T)
                
                ru,pu,yu = alg.euler_from_rot_mat(R_col_urdf)
                
                transform[0,0] = ru
                transform[1,0] = pu
                transform[2,0] = yu                
                
                #transform[0:3] = np.dot(  self.osMpi , transform[0:3] )            
            
            if self.exactly_collision:
                if np.sum(transform[0:3,0]) == 0:
                    transform[0,0] = self.osMpi_rpy[0]
                    transform[1,0] = self.osMpi_rpy[1]
                    transform[2,0] = self.osMpi_rpy[2]
                elif np.linalg.norm(transform[0:3,0]-np.array([[np.pi/2],[0.],[-np.pi]]))< 1e-4:
                    # case where the orientation of the .vtp file has been changed;
                    # beta for now.
                    transform[0,0] = np.pi/2                     
                    transform[1,0] = np.pi/2
                    transform[2,0] = np.pi/2                 
                # else:
                #     print(transform[0:3,0])
                #     print(transform[0:3,0].shape)
                #     print(np.linalg.norm(transform[0:3,0]-np.array([-np.pi/2,0.,np.pi])))
                #     exit(1)

                
                str_xyz_transform = "{: .8f} {: .8f} {: .8f}".format( transform[3,0], transform[4,0], transform[5,0] )
                str_rpy_transform = "{: .8f} {: .8f} {: .8f}".format( transform[0,0], transform[1,0], transform[2,0])
                self.writeExact("collision",visual_name, ros_filename, f,  str_rpy_transform, str_xyz_transform, str_scale_factors)
                if self.secure_collision:
                    str_xyz_transform = "{: .8f} {: .8f} {: .8f}".format( cur_xyz[0], cur_xyz[1], cur_xyz[2] )
                    str_rpy_transform = "{: .8f} {: .8f} {: .8f}".format( transform[0,0], transform[1,0], transform[2,0] )
                    self.writeApproximation("collision_checking",visual_name, f, str_rpy_transform, str_xyz_transform, str_scale_factors,type_obj,carac_obj)                   
                
            else:
                str_xyz_transform = "{: .8f} {: .8f} {: .8f}".format( cur_xyz[0], cur_xyz[1], cur_xyz[2] )
                str_rpy_transform = "{: .8f} {: .8f} {: .8f}".format( transform[0,0], transform[1,0], transform[2,0] )
                self.writeApproximation("collision",visual_name, f, str_rpy_transform, str_xyz_transform, str_scale_factors,type_obj,carac_obj)
            
        # everything set for this body!
        self.ident-=self.ident_size        
        f.write(self.ident*" "+"</link> \n \n")

    def getSTLApproximation(self,stl_file,body_name):
        """
        
        Reads an .stl file and get usefull informations from this file, in order
        to approximate it by a geometric object supported by the .urdf format 
        ( a box, a cylinder, or a sphere).

        Parameters
        ----------
        stl_file : String
            Path to the .stl file currently under review.
        body_name : String
            Name of the body associated with the .stl file.

        Returns
        -------
        
        type_obj : String
            Name of the object approximating the .stl file (cylinder, box, or sphere)
            
        carac_obj : List of float
            Features of the object approximating the .stl file.
            
            Cylinder : [radius of the cylinder, length of the cylinder ]
            
            Sphere : [radius of the sphere]
            
            Box : [Size of box on the x axis, Size of box on y axis, Size of box on z axis]

        """
        main_body = mesh.Mesh.from_file(stl_file)
        
        stl_name = stl_file.split("/")[-1].split(".")[0]

        if stl_name in self.correc_stl_dic and len(self.correc_stl_dic[stl_name][False])>0:
            # approximation --> case False : we do not want the correction
            # for the exact case, but for the approximation case
            mass_center_stl = self.correc_stl_dic[stl_name][False]
        else:
            mass_props = main_body.get_mass_properties()
            mass_center_stl = np.dot(self.osMpi, mass_props[1] )
        self.dic_body[body_name].append( mass_center_stl )

        minx, maxx, miny, maxy, minz, maxz = self.find_mins_maxs(main_body)
        
        try : 
            Dx = maxx - minx
            Dy = maxy - miny
            Dz= maxz - minz
        except Exception:
            print("Error with file %s : are you sure of its name? \n"%(stl_file))
            print("If the file is there, it may be corrupted. \n")
            exit(0)
        
        # let's approximate this object
        
        min_order=  math.log10( max(Dx,Dy,Dz) )  -1
        
        if min_order < 0:
            min_order = math.floor(min_order)
        else:
            min_order = math.ceil(min_order)
        
        lim_diff = 4*(10**(min_order))
        
        type_obj= []
        carac_obj = []
        
        if abs(Dx-Dz) < lim_diff and abs(Dy-Dz) < lim_diff and abs(Dx-Dy) < lim_diff:
            type_obj = "sphere"
            radius = np.mean([Dx,Dy,Dz])/2
            carac_obj = [radius]

        
        elif abs(Dx-Dz) < lim_diff:
            type_obj = "cylinder"
            radius = np.mean([Dx,Dz])/2
            length = Dy
            carac_obj = [radius, length]
                    
        else:
            type_obj = "box"
            Mdim = np.array([Dx,Dy,Dz])
            Mdim_urdf = np.dot(self.osMpi, Mdim)
            Dx_urdf = abs(Mdim_urdf[0])
            Dy_urdf = abs(Mdim_urdf[1])
            Dz_urdf = abs(Mdim_urdf[2])
            
            carac_obj = [Dx_urdf,Dy_urdf,Dz_urdf]

        return(type_obj,carac_obj)
                   
    def find_mins_maxs(self,obj):
        """
        
        Reads the .stl file, and get it's minimum and maximum dimensions
        on each of the axes ( of the coordinate system written into the
        .stl file).

        Parameters
        ----------
        obj : Mesh object
            Object describing the .stl file.

        Returns
        -------
        minx, maxx : float
            Minimum and maximum positions of the points of the .stl file, on 
            the x axis. 
        miny, maxy : float
            Minimum and maximum positions of the points of the .stl file, on 
            the y axis. 
        minz, maxz : float
            Minimum and maximum positions of the points of the .stl file, on 
            the z axis. 

        """
        minx = maxx = miny = maxy = minz = maxz = None
        for p in obj.points:
            # p contains (x, y, z)
            if minx is None:
                minx = p[stl.Dimension.X]
                maxx = p[stl.Dimension.X]
                miny = p[stl.Dimension.Y]
                maxy = p[stl.Dimension.Y]
                minz = p[stl.Dimension.Z]
                maxz = p[stl.Dimension.Z]
            else:
                maxx = max(p[stl.Dimension.X], maxx)
                minx = min(p[stl.Dimension.X], minx)
                maxy = max(p[stl.Dimension.Y], maxy)
                miny = min(p[stl.Dimension.Y], miny)
                maxz = max(p[stl.Dimension.Z], maxz)
                minz = min(p[stl.Dimension.Z], minz)
        return minx, maxx, miny, maxy, minz, maxz

    def writeApproximation(self,name_type,visual_name,f, str_rpy_transform, str_xyz_transform, str_scale_factors,type_obj, carac_obj):
        """

        Write an approximation of the .stl file as an geometry object supported 
        by the .urdf format, for a tag specified by the variable name_type.
        
        This feature is added for two reasons : 
            * Solve issues with corrupted .stl files, that won't be read afterwards
            during the visualization of the .urdf file.
            
            * Reduce the complexity of calculating model surfaces (for example, 
            to calculate possible collisions of the model with other elements 
            in space).
        
        It won't look as pretty as with a .stl file, but it is nearly sure it 
        will work well.        

        Parameters
        ----------
        name_type : String
            Type of the tag that should be written into the .urdf file.
            
        visual_name : String
            Name of the .stl file.
            
        f : File
            The .urdf file.
            
        str_rpy_transform : String
            String representing the rpy origin of the .stl object.
            
        str_xyz_transform : String
            String representing the xyz origin of the .stl object.
            
        str_scale_factors : String
            String representing the scale factors of the .stl object.
        
        type_obj : String
            Name of the object approximating the .stl file (cylinder, box, or sphere)
            
        carac_obj : List of float
            Features of the object approximating the .stl file.
            
            Cylinder : [radius of the cylinder, length of the cylinder ]
            
            Sphere : [radius of the sphere]
            
            Box : [Size of box on the x axis, Size of box on y axis, Size of box on z axis]

        Returns
        -------
        None.

        """

        f.write(self.ident*" "+ '<'+name_type+  ' name="'+ visual_name+ '"> \n')
        self.ident+= self.ident_size

        f.write(self.ident*" "+ '<origin xyz="'+ str_xyz_transform+  '" rpy="'+ str_rpy_transform+'" /> \n') 
            
        f.write(self.ident*" "+ '<geometry> \n')
        self.ident+= self.ident_size            

        str_obj = '<' + type_obj
        if type_obj == "sphere":
            str_obj += ' radius="' + str(carac_obj[0]) + '" />'
        
        elif type_obj == "cylinder":
            str_obj += ' radius="' + str(carac_obj[0]) + '" length="' + str(carac_obj[1])   + '" />'            
        
        elif type_obj == "box":
            size = "{: .4f} {: .4f} {: .4f}".format( carac_obj[0], carac_obj[1], carac_obj[2] )
            str_obj += ' size="' + size + '" />'               

        f.write(self.ident*" "+ str_obj + ' \n')

        self.ident-= self.ident_size                   
        f.write(self.ident*" "+ "</geometry> \n")
        self.ident-= self.ident_size                   
        f.write(self.ident*" "+ "</"+ name_type +"> \n \n")        
 
        

    def writeExact(self,name_type,visual_name,filename, f, str_rpy_transform, str_xyz_transform, str_scale_factors):
        """
        Write the exact .stl file object into the .urdf file, for a tag 
        specified by the variable name_type.        

        Parameters
        ----------
        name_type : String
            Type of the tag that should be written into the .urdf file.
            
        visual_name : String
            Name of the .stl file.
            
        f : File
            The .urdf file.
            
        str_rpy_transform : String
            String representing the rpy origin of the .stl object.
            
        str_xyz_transform : String
            String representing the xyz origin of the .stl object.
            
        str_scale_factors : String
            String representing the scale factors of the .stl object.

        Returns
        -------
        None.

        """
        
        f.write(self.ident*" "+ '<'+name_type+  ' name="'+ visual_name+ '"> \n')
        self.ident+= self.ident_size
        
        f.write(self.ident*" "+ '<origin xyz="'+ str_xyz_transform+  '" rpy="'+ str_rpy_transform+'" /> \n') 
        f.write(self.ident*" "+ '<geometry> \n')
        self.ident+= self.ident_size            
        
        f.write(self.ident*" "+ '<mesh filename="package:/'+filename+'" scale="'+ str_scale_factors+ '"/> \n')

        self.ident-= self.ident_size                   
        f.write(self.ident*" "+ "</geometry> \n")
        self.ident-= self.ident_size                   
        f.write(self.ident*" "+ "</"+ name_type +"> \n \n")
            
def readJointSet(root,pymodel):
    """
    Special case where the .osim file got a "JointSet" tag.
    This means that the definition of joints and the definition of
    body parts of the model are separated. Thus, they need to be treated
    separately. 

    Parameters
    ----------
    root : ElementTree Object
        Object containing all informations about the .osim file. Those
        informations are organized as a tree.
    
    pymodel : Dictionnary of List
        Dictionnary containing every useful informations about the .osim
        file, in order to convert this file into a .urdf format.

    Returns
    -------
    pymodel : Dictionnary of List
        The pymodel object with the informations into the <JointSet> tag of
        the .osim.

    """
    poss_joints = ['CustomJoint','WeldJoint','PinJoint','UniversalJoint']
    
    for name_joint in poss_joints:
    
        for joint in root.findall('./Model/JointSet/objects/'+name_joint):
        
            joint_data = {
                'name':[], 
                'parent_body':[], 
                'child_body':[],
                'location_in_parent':[], 
                'orientation_in_parent':[], 
                'location':[],
                'orientation':[]
            }
            

            is_socket = type(joint.find('socket_parent_frame')) != type(None) 
            
            if is_socket : 
                # means that we are working with sockets. 
                
                joint_data['name'].append(joint.get('name'))
                
                parent_socket = joint.find('socket_parent_frame').text
                child_socket = joint.find('socket_child_frame').text
                
                frames = joint.findall('frames/PhysicalOffsetFrame')

                for pf in frames:
                    name_pf = pf.get('name')
                    
                    bpart = pf.find("socket_parent").text.split("/")[-1]
                    trans = pf.find("translation").text.split()
                    orient = pf.find("orientation").text.split()
                    
                    # location : Location of the joint in the child body specified in the child reference frame. For SIMM models, this vector is always the zero vector (i.e., the body reference frame coincides with the joint). 
                    # orientation : Orientation of the joint in the owing body specified in the owning body reference frame.  Euler XYZ body-fixed rotation angles are used to express the orientation. 
                
                    # location_in_parent : Location of the joint in the parent body specified in the parent reference frame. Default is (0,0,0).
                    # orientation_in_parent : Orientation of the joint in the parent body specified in the parent reference frame. Euler XYZ body-fixed rotation angles are used to express the orientation. Default is (0,0,0).

                    if name_pf == parent_socket :
                        joint_data['parent_body'].append(bpart)
                        joint_data['location_in_parent'].append(trans)
                        joint_data['orientation_in_parent'].append(orient)
                    elif name_pf == child_socket:
                        joint_data['child_body'].append(bpart)
                        joint_data['location'].append(trans)
                        joint_data['orientation'].append(orient)                       
            
            else:
                print("New case : JointSet but no socket (in readJointSet). Exit.")
                exit(1)

            # Coordinate Set
            coordinates_list = joint.iter('Coordinate')
            coordinate_data = {'name':[],
                               'motion_type':[],
                               'default_value':[],
                               'default_speed_value':[],
                               'range':[],
                               'clamped':[],
                               'locked':[],
                               'prescribed_function':[]
            }
            
            L_coordinate_name = []
            
            for coordinates in coordinates_list:
                L_coordinate_name.append(coordinates.get('name'))
                
                L_coord_dat = list(coordinate_data.keys())
                
                L_get = ['name']
                L_find_split = ['range']
                
                for data in L_coord_dat:
                    if type(coordinates.find(data)) != type(None) :
                        
                        if data in L_get:
                            coordinate_data[data].append(coordinates.get(data) )
                        
                        elif data in L_find_split:
                            coordinate_data[data].append( (coordinates.find(data).text).split() )

                        else:
                            coordinate_data[data].append(coordinates.find(data).text)

            spatial_list = joint.findall('SpatialTransform/TransformAxis')
            spatial_data = {
                'name': [],
                'coordinates': [],
                'axis': []
            }

            if name_joint =='PinJoint':
                # https://simtk.org/api_docs/opensim/api_docs20b/classOpenSim_1_1PinJoint.html
                # z axis of the joint frame
                # --> Unaligned, on z
                #
                
                joint_orientation = np.array(joint_data['orientation'][0],dtype=float)

                M_j_o = alg.euler_matrix(joint_orientation[0],joint_orientation[1], joint_orientation[2])
                
                M_z = M_j_o[:3,2]

                z_orientation = [str(M_z[0]), str(M_z[1]), str(M_z[2])]
                
                spatial_data['name'] = ['rotation1', 'rotation2', 'rotation3', 'translation1', 'translation2', 'translation3']
                
                
                spatial_data['coordinates'] = [L_coordinate_name[-1],  None, None, None, None, None ]
                spatial_data['axis'] = [z_orientation,  ['0', '1', '0'], ['1', '0', '0'], ['1', '0', '0'], 
                                            ['0', '1', '0'], ['0', '0', '1'] ]
                
            if name_joint =='UniversalJoint':
                # https://simtk.org/api_docs/opensim/api_docs/classOpenSim_1_1UniversalJoint.html#details
                # first one on x axis in the joint frame
                # second one on y axis in the joint frame
                # 
                # --> Unaligned, on x, then Unaligned on y.

                joint_orientation = np.array(joint_data['orientation'][0],dtype=float)
                
                M_j_o = alg.euler_matrix(joint_orientation[0],joint_orientation[1], joint_orientation[2])
                
                M_x = M_j_o[:3,0]
                M_y = M_j_o[:3,1]
                x_orientation = [str(M_x[0]), str(M_x[1]), str(M_x[2])]
                y_orientation = [str(M_y[0]), str(M_y[1]), str(M_y[2])]
                
                spatial_data['name'] = ['rotation1', 'rotation2', 'rotation3', 'translation1', 'translation2', 'translation3']
                spatial_data['coordinates'] = [L_coordinate_name[-2],  L_coordinate_name[-1], None, None, None, None ]
                spatial_data['axis'] = [x_orientation,  y_orientation, ['1', '0', '0'], ['1', '0', '0'], 
                                            ['0', '1', '0'], ['0', '0', '1'] ]            
            
            else:

                for spatial_transform in spatial_list:
                    is_linear_function = type(spatial_transform.find('LinearFunction')) != type(None)
                    is_simm_function = type(spatial_transform.find('SimmSpline')) != type(None)

                    if is_linear_function or is_simm_function:
                    
                        spatial_data['coordinates'].append(spatial_transform.find('coordinates').text)
                            
                    else:
                        spatial_data['coordinates'].append(None)

                    spatial_data['axis'].append((spatial_transform.find('axis').text).split())
                    spatial_data['name'].append(spatial_transform.get('name'))      
                    
            pymodel['Joints'].append([joint_data, coordinate_data, spatial_data])    
    return(pymodel)


def readJoint(name_joint,pymodel,bodies,body_name):
        joints_list =  bodies.iter(name_joint)
        n = -1
        for joint in joints_list:
            if n == -1:
                n = 0
            joint_data = {
                'name':[], 
                'parent_body':[], 
                'child_body':[],
                'location_in_parent':[], 
                'orientation_in_parent':[], 
                'location':[],
                'orientation':[]
            }
            joint_data['name'].append(joint.get('name'))
            joint_data['child_body'].append(body_name)
            joint_data['parent_body'].append(joint.find('parent_body').text)
            joint_data['location_in_parent'].append((joint.find('location_in_parent').text).split())
            joint_data['orientation_in_parent'].append((joint.find('orientation_in_parent').text).split())
            joint_data['location'].append((joint.find('location').text).split())
            joint_data['orientation'].append((joint.find('orientation').text).split())

            # Coordinate Set
            coordinates_list = joint.iter('Coordinate')
            coordinate_data = {'name':[],
                               'motion_type':[],
                               'default_value':[],
                               'default_speed_value':[],
                               'range':[],
                               'clamped':[],
                               'locked':[],
                               'prescribed_function':[]
            }
            
            L_coordinate_name = []
            
            for coordinates in coordinates_list:
                L_coordinate_name.append(coordinates.get('name'))
                coordinate_data['name'].append(coordinates.get('name'))
                coordinate_data['motion_type'].append(coordinates.find('motion_type').text)
                coordinate_data['default_value'].append(coordinates.find('default_value').text)
                coordinate_data['default_speed_value'].append(coordinates.find('default_speed_value').text)
                coordinate_data['range'].append((coordinates.find('range').text).split())
                coordinate_data['clamped'].append(coordinates.find('clamped').text)
                coordinate_data['locked'].append(coordinates.find('locked').text)
                coordinate_data['prescribed_function'].append(coordinates.find('prescribed_function').text)
                               
            #get spatial transform
            spatial_list = joint.iter('TransformAxis')
            spatial_data = {
                'name': [],
                'coordinates': [],
                'axis': []
            }
            
            if name_joint =='PinJoint':
                # https://simtk.org/api_docs/opensim/api_docs20b/classOpenSim_1_1PinJoint.html
                # z axis of the joint frame
                # --> Unaligned, on z
                #
                
                joint_orientation = np.array(joint_data['orientation'][0],dtype=float)
                #print(joint_orientation)
                
                M_j_o = alg.euler_matrix(joint_orientation[0],joint_orientation[1], joint_orientation[2])
                
                M_z = M_j_o[:3,2]
                #print(M_z)
                z_orientation = [str(M_z[0]), str(M_z[1]), str(M_z[2])]
                
                # z_orientation = ["0", "0", "1"]
                
                spatial_data['name'] = ['rotation1', 'rotation2', 'rotation3', 'translation1', 'translation2', 'translation3']
                
                
                spatial_data['coordinates'] = [L_coordinate_name[-1],  None, None, None, None, None ]
                spatial_data['axis'] = [z_orientation,  ['0', '1', '0'], ['1', '0', '0'], ['1', '0', '0'], 
                                            ['0', '1', '0'], ['0', '0', '1'] ]
                
            if name_joint =='UniversalJoint':
                # https://simtk.org/api_docs/opensim/api_docs/classOpenSim_1_1UniversalJoint.html#details
                # first one on x axis in the joint frame
                # second one on y axis in the joint frame
                # 
                # --> Unaligned, on x, then Unaligned on y.
                #
                
                joint_orientation = np.array(joint_data['orientation'][0],dtype=float)
                #print(joint_orientation)
                
                M_j_o = alg.euler_matrix(joint_orientation[0],joint_orientation[1], joint_orientation[2])
                
                M_x = M_j_o[:3,0]
                M_y = M_j_o[:3,1]
                x_orientation = [str(M_x[0]), str(M_x[1]), str(M_x[2])]
                y_orientation = [str(M_y[0]), str(M_y[1]), str(M_y[2])]
                
                # x_orientation = ["1","0","0"]
                # y_orientation=["0","1","0"]
                
                spatial_data['name'] = ['rotation1', 'rotation2', 'rotation3', 'translation1', 'translation2', 'translation3']
                spatial_data['coordinates'] = [L_coordinate_name[-2],  L_coordinate_name[-1], None, None, None, None ]
                spatial_data['axis'] = [x_orientation,  y_orientation, ['1', '0', '0'], ['1', '0', '0'], 
                                            ['0', '1', '0'], ['0', '0', '1'] ]            
            
            else:
                
                for spatial_transform in spatial_list:
                    is_linear_function = type(spatial_transform.find('function/LinearFunction')) != type(None)
                    is_simm_function = type(spatial_transform.find('function/SimmSpline')) != type(None)

                    if is_linear_function or is_simm_function:
                    
                        spatial_data['coordinates'].append(spatial_transform.find('coordinates').text)
                            
                    else:
                        spatial_data['coordinates'].append(None)
                            # if disp:
                            #     print(spatial_transform.find('axis').text)
                            #     print((spatial_transform.find('axis').text).split() )
                            
                    spatial_data['axis'].append((spatial_transform.find('axis').text).split())
                    spatial_data['name'].append(spatial_transform.get('name'))                    
                
                
                # for spatial_transform in spatial_list:
                #     # check that the spatial_transform is really going to be used; to do
                #     # so, check the function 
                #     # accept only the LinearFunction
                    
                #     is_linear_function = type(spatial_transform.find('function/LinearFunction')) != type(None)
                    
                #     if is_linear_function:
                #         spatial_data['coordinates'].append(spatial_transform.find('coordinates').text)

                #     else:
                #         spatial_data['coordinates'].append(None)
                        
                #     spatial_data['axis'].append((spatial_transform.find('axis').text).split())
                #     spatial_data['name'].append(spatial_transform.get('name'))
                
                # if spatial_data['coordinates'].count(None) == 6:
                #     # error, no function linear in joint
                #     # (bad writing of the file)
                #     # try to fix it with coordinates
                    
                #     spatial_data = {
                #     'name': [],
                #     'coordinates': [],
                #     'axis': []
                #     }
                    
                    
                #     for spatial_transform in spatial_list:
                #         spatial_data['coordinates'].append(spatial_transform.find('coordinates').text)
                #         spatial_data['axis'].append((spatial_transform.find('axis').text).split())
                #         spatial_data['name'].append(spatial_transform.get('name'))
                    
            pymodel['Joints'].append([joint_data, coordinate_data, spatial_data])    
   
        return(pymodel)

def readOsim(filename):
    tree = xml.parse(filename)
    root = tree.getroot()
    pymodel = {}
    for version in root.findall('.'):
        n_ver = int(version.get('Version'))
    if n_ver == 40000:
        pymodel = readOsim40(filename)
    if n_ver <= 30000:
        pymodel = readOsim30(filename)
    return(pymodel)
        
def readVisuals40(pymodel,bodies):
    visuals_data = {
            'scale_factors':[]
        }
        
    bones_data = {
                'geometry_file': [],
                'color': [],
                'transform': [],
                'scale_factors': [],
                'opacity': []
        }

    visuals_data,bones_data = readVisualsName40('components/PhysicalOffsetFrame',visuals_data,bones_data,bodies)        
        # if body_name == "thorax":
        #     print(pymodel['Visuals'])
        #     exit(1)
    visuals_data,bones_data = readVisualsName40('PhysicalOffsetFrame',visuals_data,bones_data,bodies)        
        
    visuals_data,bones_data = readVisualsName40('.',visuals_data,bones_data,bodies)        


    pymodel['Visuals'].append([visuals_data, bones_data])
                

    return(pymodel)


def readVisualsName40(name,visuals_data,bones_data,bodies):
    """
    Main difference : does not seem to have VisibleObject tag here. 
    Means that compared to versions <= 3.0, bones_data are not included 
    into visuals data. Needs to be solved.

    Parameters
    ----------
    name : TYPE
        DESCRIPTION.
    pymodel : TYPE
        DESCRIPTION.
    bodies : TYPE
        DESCRIPTION.

    Returns
    -------
    None.

    """
    # Visible Objects
   
    visible_list =  bodies.findall(name)

    for visuals in visible_list:

        visuals_data['scale_factors'].append((visuals.find('FrameGeometry/scale_factors').text).split())
        
        if name == ".":
            # case where the meshes are treated as fixed to the frame and they share the transform of the frame when visualized
            translation = "0.0 0.0 0.0"
            orientation = "0.0 0.0 0.0"
            
        else:
            translation = visuals.find('translation').text
            orientation = visuals.find('orientation').text
        transform = (orientation + " " + translation).split()
            
            

        
        if name == ".":
            bones_list = visuals.findall("attached_geometry/Mesh")
        else:
            bones_list =  visuals.iter('Mesh')
        for bones in bones_list:
            mesh_name = bones.find('mesh_file').text
            
            if mesh_name != None:

                bones_data['geometry_file'].append(bones.find('mesh_file').text) 
                bones_data['color'].append((bones.find('Appearance/color').text).split())
                bones_data['transform'].append(transform)
                bones_data['scale_factors'].append((bones.find('scale_factors').text).split())
                bones_data['opacity'].append(bones.find('Appearance/opacity').text)

    return(visuals_data,bones_data)


def readOsim40(filename):
    """
    
    An improved version of the readOsim method of the Ospi package. This 
    method is able to read a larger number of .osim files.

    Parameters
    ----------
    filename : String
         Path to the .osim file.

    Returns
    -------
    pymodel : Dictionnary of List
        Dictionnary containing every useful informations about the .osim
        file, in order to convert this file into a .urdf format.

    """
    pymodel = {
        'Bodies':[], 
        'Joints':[], 
        'Visuals':[],
        'Forces':[],
        'Point':[]
    }
    tree = xml.parse(filename)
    root = tree.getroot()
    
    # *********************************************************
    #                   Get body set
    # *********************************************************
    
    # set ground
    
    body_ground = root.findall('./Model/Ground')
    for ground in body_ground:
        body_data = {'name':[], 
                     'mass':[], 
                     'mass_center':[], 
                     'inertia':[]}        
        body_name= ground.get('name')
        body_data['name'].append(body_name)
        body_data['mass'].append('0.0')
        body_data['mass_center'].append(['0.0','0.0','0.0'])
        Y = [['0.0',
                  '0.0',
                  '0.0'],
                 ['0.0',
                  '0.0',
                  '0.0'],
                 ['0.0',
                  '0.0',
                  '0.0']]        
        body_data['inertia'].append(Y)
        pymodel['Bodies'].append(body_data)        
        
        visuals_data = {
                'scale_factors':[]
            }
        visuals_data['scale_factors'].append((ground.find('FrameGeometry/scale_factors').text).split())
            
        bones_data = {
                'geometry_file': [],
                'color': [],
                'transform': [],
                'scale_factors': [],
                'opacity': []
        }
        bones_list =  ground.iter('Mesh')
        for bones in bones_list:
            bones_data['geometry_file'].append(bones.find('mesh_file').text) 
            bones_data['color'].append((bones.find('Appearance/color').text).split())
            bones_data['transform'].append(["0.0","0.0","0.0","0.0","0.0","0.0"])
            bones_data['scale_factors'].append((bones.find('scale_factors').text).split())
               
            bones_data['opacity'].append(bones.find('Appearance/opacity').text)

        pymodel['Visuals'].append([visuals_data, bones_data])        
        
    # test if there is a JointSet tag
    is_joint_set = (  type(root.find("./Model/JointSet")) != type(None)   )
    if is_joint_set:
        # means that there is a JointSet tag... great.
        pymodel = readJointSet(root,pymodel)

    
    
    for bodies in root.findall('./Model/BodySet/objects/Body'):
        body_data = {'name':[], 
                     'mass':[], 
                     'mass_center':[], 
                     'inertia':[]}
        body_name= bodies.get('name')
        
        body_data['name'].append(body_name)
        body_data['mass'].append(bodies.find('mass').text)
        body_data['mass_center'].append((bodies.find('mass_center').text).split())
        
        
        
        try:
            Y = [[bodies.find('inertia_xx').text,
                  bodies.find('inertia_xy').text,
                  bodies.find('inertia_xz').text],
                 [bodies.find('inertia_xy').text,
                  bodies.find('inertia_yy').text,
                  bodies.find('inertia_yz').text],
                 [bodies.find('inertia_xz').text,
                  bodies.find('inertia_yz').text,
                  bodies.find('inertia_zz').text]]
        except Exception:
            # error in 4.0, not the same model for inertia
            #print("Bodies error : ", bodies.get('name'))
            inertia = bodies.find('inertia').text
            L_inertia = inertia.split(" ")
            Y = [[L_inertia[0],
                 L_inertia[3],
                 L_inertia[4]
                     ],
                 [L_inertia[3],
                  L_inertia[1],
                  L_inertia[5]
                     ],
                 [L_inertia[4],
                  L_inertia[5],
                  L_inertia[2]
                     ]
                ]
        body_data['inertia'].append(Y)
        pymodel['Bodies'].append(body_data)

        if not is_joint_set : 
            pymodel = readJoint('CustomJoint',pymodel,bodies,body_name)
            pymodel = readJoint('WeldJoint',pymodel,bodies,body_name)
            pymodel = readJoint('PinJoint',pymodel,bodies,body_name)
            pymodel = readJoint('UniversalJoint',pymodel,bodies,body_name)

        # Visible Objects
        
        pymodel = readVisuals40(pymodel,bodies)        

        # if body_name == "thorax":
        #     print(pymodel['Visuals'][1])
        #     print(pymodel['Visuals'][1][1]['geometry_file'] )
        #     exit(1)




    # *********************************************************
    #                 Get force set
    # *********************************************************
    
    # Schutte1993Muscle_Deprecated
    for forces in root.findall('./Model/ForceSet/objects/Schutte1993Muscle_Deprecated'):
        force_data = {
            'force_name':[],
            'type':[],
            'max_isometric_force':[],
            'optimal_fiber_length':[]
        }
        force_data['force_name'].append(forces.get('name'))
        force_data['type'].append('Schutte1993Muscle_Deprecated')
        force_data['max_isometric_force'].append(forces.find('max_isometric_force').text)
        force_data['optimal_fiber_length'].append(forces.find('optimal_fiber_length').text)        
        # point set
        points = []
        path_point_list =  forces.iter('PathPoint')
        for point in path_point_list:
            point_data = {'point_name':[], 'location':[], 'body':[]}
            point_data['point_name'].append(point.get('name'))
            point_data['location'].append((point.find('location').text).split())
            point_data['body'].append(point.find('body').text)
            points.append(point_data)
        pymodel['Forces'].append([force_data,points]) 
        
    # Thelen2003Muscle
    for forces in root.findall('./Model/ForceSet/objects/Thelen2003Muscle'):
        force_data = {
            'force_name':[],
            'type':[],
            'max_isometric_force':[],
            'optimal_fiber_length':[]
        }
        force_data['force_name'].append(forces.get('name'))
        force_data['type'].append('Thelen2003Muscle')
        force_data['max_isometric_force'].append(forces.find('max_isometric_force').text)
        force_data['optimal_fiber_length'].append(forces.find('optimal_fiber_length').text)          
        # point set
        points = []
        path_point_list =  forces.iter('PathPoint')
        for point in path_point_list:
            point_data = {'point_name':[], 'location':[], 'body':[]}
            point_data['point_name'].append(point.get('name'))
            point_data['location'].append((point.find('location').text).split())
            try :
                point_data['body'].append(point_data['body'].append(point.find('body').text) ) 
            except Exception:
                # error in 4.0, not the same model for body
                body_str = (point.find('socket_parent_frame').text).split("/")[-1]
                
                point_data['body'].append(body_str) 
            points.append(point_data)
        pymodel['Forces'].append([force_data,points])

    # Millard2012EquilibriumMuscle
    for forces in root.findall('./Model/ForceSet/objects/Millard2012EquilibriumMuscle'):
        force_data = {
            'force_name':[],
            'type':[],
            'max_isometric_force':[],
            'optimal_fiber_length':[]
        }
        force_data['force_name'].append(forces.get('name'))
        force_data['type'].append('Millard2012EquilibriumMuscle')
        force_data['max_isometric_force'].append(forces.find('max_isometric_force').text)
        force_data['optimal_fiber_length'].append(forces.find('optimal_fiber_length').text)          
        # point set
        points = []
        path_point_list =  forces.iter('PathPoint')
        for point in path_point_list:
            point_data = {'point_name':[], 'location':[], 'body':[]}
            point_data['point_name'].append(point.get('name'))
            point_data['location'].append((point.find('location').text).split())
            point_data['body'].append(point.find('body').text)
            points.append(point_data)
        pymodel['Forces'].append([force_data,points])


    
    return pymodel        


def readOsim30(filename):
    """
    
    An improved version of the readOsim method of the Ospi package. This 
    method is able to read a larger number of .osim files.

    Parameters
    ----------
    filename : String
         Path to the .osim file.

    Returns
    -------
    pymodel : Dictionnary of List
        Dictionnary containing every useful informations about the .osim
        file, in order to convert this file into a .urdf format.

    """
    pymodel = {
        'Bodies':[], 
        'Joints':[], 
        'Visuals':[],
        'Forces':[],
        'Point':[]
    }
    tree = xml.parse(filename)
    root = tree.getroot()
    
    # *********************************************************
    #                   Get body set
    # *********************************************************
    for bodies in root.findall('./Model/BodySet/objects/Body'):
        body_data = {'name':[], 
                     'mass':[], 
                     'mass_center':[], 
                     'inertia':[]}
        body_name= bodies.get('name')
        
        body_data['name'].append(body_name)
        body_data['mass'].append(bodies.find('mass').text)
        body_data['mass_center'].append((bodies.find('mass_center').text).split())
        try:
            Y = [[bodies.find('inertia_xx').text,
                  bodies.find('inertia_xy').text,
                  bodies.find('inertia_xz').text],
                 [bodies.find('inertia_xy').text,
                  bodies.find('inertia_yy').text,
                  bodies.find('inertia_yz').text],
                 [bodies.find('inertia_xz').text,
                  bodies.find('inertia_yz').text,
                  bodies.find('inertia_zz').text]]
        except Exception:
            # error in 4.0, not the same model for inertia
            #print("Bodies error : ", bodies.get('name'))
            inertia = bodies.find('inertia').text
            L_inertia = inertia.split(" ")
            Y = [[L_inertia[0],
                 L_inertia[3],
                 L_inertia[4]
                     ],
                 [L_inertia[3],
                  L_inertia[1],
                  L_inertia[5]
                     ],
                 [L_inertia[4],
                  L_inertia[5],
                  L_inertia[2]
                     ]
                ]
        body_data['inertia'].append(Y)
        pymodel['Bodies'].append(body_data)


        pymodel = readJoint('CustomJoint',pymodel,bodies,body_name)
        pymodel = readJoint('WeldJoint',pymodel,bodies,body_name)
        pymodel = readJoint('PinJoint',pymodel,bodies,body_name)
        pymodel = readJoint('UniversalJoint',pymodel,bodies,body_name)

        # Visible Objects
        visible_list =  bodies.iter('VisibleObject')

        
        for visuals in visible_list:
            visuals_data = {
                'scale_factors':[],
                'show_axes':[],
                'display_preference':[]
            }
            visuals_data['scale_factors'].append((visuals.find('scale_factors').text).split())
            visuals_data['show_axes'].append(visuals.find('show_axes').text)
            visuals_data['display_preference'].append(visuals.find('display_preference').text)
                
            bones_list =  visuals.iter('DisplayGeometry')
            bones_data = {
                'geometry_file': [],
                'color': [],
                'transform': [],
                'scale_factors': [],
                'display_preference': [],
                'opacity': []
            }
            for bones in bones_list:
                bones_data['geometry_file'].append(bones.find('geometry_file').text) 
                bones_data['color'].append((bones.find('color').text).split())
                bones_data['transform'].append((bones.find('transform').text).split())
                bones_data['scale_factors'].append((bones.find('scale_factors').text).split())
                bones_data['display_preference'].append(bones.find('display_preference').text)
                bones_data['opacity'].append(bones.find('opacity').text)
                
            pymodel['Visuals'].append([visuals_data, bones_data])




    # *********************************************************
    #                 Get force set
    # *********************************************************
    
    # Schutte1993Muscle_Deprecated
    for forces in root.findall('./Model/ForceSet/objects/Schutte1993Muscle_Deprecated'):
        force_data = {
            'force_name':[],
            'type':[],
            'max_isometric_force':[],
            'optimal_fiber_length':[]
        }
        force_data['force_name'].append(forces.get('name'))
        force_data['type'].append('Schutte1993Muscle_Deprecated')
        force_data['max_isometric_force'].append(forces.find('max_isometric_force').text)
        force_data['optimal_fiber_length'].append(forces.find('optimal_fiber_length').text)        
        # point set
        points = []
        path_point_list =  forces.iter('PathPoint')
        for point in path_point_list:
            point_data = {'point_name':[], 'location':[], 'body':[]}
            point_data['point_name'].append(point.get('name'))
            point_data['location'].append((point.find('location').text).split())
            point_data['body'].append(point.find('body').text)
            points.append(point_data)
        pymodel['Forces'].append([force_data,points]) 
        
    # Thelen2003Muscle
    for forces in root.findall('./Model/ForceSet/objects/Thelen2003Muscle'):
        force_data = {
            'force_name':[],
            'type':[],
            'max_isometric_force':[],
            'optimal_fiber_length':[]
        }
        force_data['force_name'].append(forces.get('name'))
        force_data['type'].append('Thelen2003Muscle')
        force_data['max_isometric_force'].append(forces.find('max_isometric_force').text)
        force_data['optimal_fiber_length'].append(forces.find('optimal_fiber_length').text)          
        # point set
        points = []
        path_point_list =  forces.iter('PathPoint')
        for point in path_point_list:
            point_data = {'point_name':[], 'location':[], 'body':[]}
            point_data['point_name'].append(point.get('name'))
            point_data['location'].append((point.find('location').text).split())
            try :
                point_data['body'].append(point_data['body'].append(point.find('body').text) ) 
            except Exception:
                # error in 4.0, not the same model for body
                body_str = (point.find('socket_parent_frame').text).split("/")[-1]
                
                point_data['body'].append(body_str) 
            points.append(point_data)
        pymodel['Forces'].append([force_data,points])

    # Millard2012EquilibriumMuscle
    for forces in root.findall('./Model/ForceSet/objects/Millard2012EquilibriumMuscle'):
        force_data = {
            'force_name':[],
            'type':[],
            'max_isometric_force':[],
            'optimal_fiber_length':[]
        }
        force_data['force_name'].append(forces.get('name'))
        force_data['type'].append('Millard2012EquilibriumMuscle')
        force_data['max_isometric_force'].append(forces.find('max_isometric_force').text)
        force_data['optimal_fiber_length'].append(forces.find('optimal_fiber_length').text)          
        # point set
        points = []
        path_point_list =  forces.iter('PathPoint')
        for point in path_point_list:
            point_data = {'point_name':[], 'location':[], 'body':[]}
            point_data['point_name'].append(point.get('name'))
            point_data['location'].append((point.find('location').text).split())
            point_data['body'].append(point.find('body').text)
            points.append(point_data)
        pymodel['Forces'].append([force_data,points])


    
    return pymodel        



if __name__ == "__main__":
    
    ### --- wholebody.osim
    
    # #Path to the ros package
    # ros_package_path = "/home/auctus/catkin_ws/src/human_control/"
    # # Path to the vtp files
    # vtp_path='/home/auctus/opensim_optitrack/OpenSim-Gui-Install/Geometry'
    # # The path to the .osim model
    # osim_path = '/home/auctus/ospi2urdf/models/whole_body/'
    # filename='wholebody.osim'
    
    # exactly_collision = False
    # exactly_visual = False
    
    # # True : condition exactly
    # # False : condition approx
    
    # correc_stl_dic = {
    #     'hat_spine':{True:[],
    #                   False:[0.00011565, -0.00353864,  0.26422478] #0.29422478
    #                   },
    #     'bofoot':{True:[],
    #               False:[-0.00004618,  0.025, -0.00304539]
    #               },
    #     'fingers_r':{True:[0.,0.,0.,0.,0.,0.], 
    #                   False:[]
    #                   },
    #       'fingers_l':{True:[0.,0.,0.,0.,0.,0.], 
    #                   False:[]
    #                   },               
    #       'jaw':{True:[], 
    #               False:[ 0.00007359,  0.05567701,  0.07865301]
    #                   },     
          
    #       'head_and_neck':{True:[0.,0.,0.,0.,0.,0.], 
    #                   False:[]
    #                   }                   
    #     #'hat_ribs_scap':[0.00011565, -0.00353864,  0.37]
        
    #     }
    
    ### --- arm26.osim

    # # Path to the ros package
    # ros_package_path = "/home/auctus/catkin_ws/src/human_control/"
    # # Path to the vtp files
    # vtp_path='/home/auctus/opensim_optitrack/OpenSim-Gui-Install/Geometry'
    # # The path to the .osim model
    # osim_path = '/home/auctus/ospi/models/tst_arm/'
    # filename='arm26.osim'
    
    # exactly_collision = False
    # exactly_visual = True
    
    
    # # True : condition exactly
    # # False : condition approx
        
    # correc_stl_dic = {
    #     'ground_jaw':{True:[],
    #               False:[0.00067671,  -0.04661849, 0.01257363]
    #               },
    #     'ground_skull':{True:[],
    #               False:[-0.01669981,  -0.03813969, 0.10198523]
    #               },
    #     'arm_r_radius':{True:[],
    #               False:[0.06350344,  0.00224989,  -0.129649]
    #               },

    #       'ground_r_scapula':{True:[],
    #               False:[0.10804415, -0.04549465, -0.01132800]
    #               }     
                
    #     }
    

    ### --- Rajagopal2015.osim

    # #Path to the ros package
    # ros_package_path = "/home/auctus/catkin_ws/src/human_control/"
    # # Path to the vtp files
    # vtp_path = '/home/auctus/Documents/OpenSim/4.2-2020-11-24-d9c8bb2c/Models/SimulationDataAndSetupFiles-4.0/Geometry'
    # # The path to the .osim model
    # osim_path = '/home/auctus/Documents/OpenSim/4.2-2020-11-24-d9c8bb2c/Models/SimulationDataAndSetupFiles-4.0'
    # filename = 'Rajagopal2015.osim'

    # exactly_collision = False
    # exactly_visual = True

    # # True : condition exactly
    # # False : condition approx

    # correc_stl_dic = {
    #     # 'hat_spine':{True:[],
    #     #              False:[0.00011565, -0.00353864,  0.29422478]
    #     #              },
    #     # 'ground_jaw':{True:[],
    #     #           False:[0.00067671,  -0.04661849, 0.01257363]
    #     #           },
    #     # 'ground_skull':{True:[],
    #     #           False:[-0.01669981,  -0.03813969, 0.10198523]
    #     #           },
    #     # 'arm_r_radius':{True:[],
    #     #           False:[0.06350344,  0.00224989,  -0.129649]
    #     #           },

    #     'bofoot': {True: [],
    #                 False: [-0.00004618,  0.025, -0.00304539]
    #                 }
    #     # 'fingers_r':{True:[0.,0.,0.,0.,0.,0.],
    #     #              False:[]
    #     #              },
    #     #  'fingers_l':{True:[0.,0.,0.,0.,0.,0.],
    #     #              False:[]
    #     #              },
    #     #  'head_and_neck':{True:[0.,0.,0.,0.,0.,0.],
    #     #              False:[]
    #     #              }
    #     #'hat_ribs_scap':[0.00011565, -0.00353864,  0.37]

    # }
    
    ### --- TwoArm_7ddOS4.osim

    #Path to the ros package
    ros_package_path = "/home/auctus/catkin_ws/src/human_control/"
    # Path to the vtp files
    vtp_path = '/home/auctus/Documents/OpenSim/4.2-2020-11-24-d9c8bb2c/Models/Arm7/Geometry'
    # The path to the .osim model
    osim_path =  '/home/auctus/Documents/OpenSim/4.2-2020-11-24-d9c8bb2c/Models/Arm7'

    #"/home/auctus/ospi2urdf/models/tst_arm"
    
    #'/home/auctus/Documents/OpenSim/4.2-2020-11-24-d9c8bb2c/Models/Arm7'
    filename = 'TwoArm_7ddOS4.osim'

    exactly_collision = False
    exactly_visual = True

    # True : condition exactly
    # False : condition approx

    correc_stl_dic = {
        # 'hat_spine':{True:[],
        #              False:[0.00011565, -0.00353864,  0.29422478]
        #              },
        # 'ground_jaw':{True:[],
        #           False:[0.00067671,  -0.04661849, 0.01257363]
        #           },
        # 'ground_skull':{True:[],
        #           False:[-0.01669981,  -0.03813969, 0.10198523]
        #           },
        'ulna':{True:[0.,0.,0.,0.,0.,0.0],
                  False:[]
                  },
        'radius':{True:[0.,0.,0.,0.,0.,0.0],
                  False:[]
                  },
        'ulna_l':{True:[0.,0.,0.,0.,0.,0.0],
                  False:[]
                  },
        'radius_l':{True:[0.,0.,0.,0.,0.,0.0],
                  False:[]
                  },
        
        # 'hand_l':{True:[0.0,0.,0.,0.,0.0,0.05],
        #           False:[]
        #           }
        # 'hand_r':{True:[0.,0.,0.,0.,0.0,0.0],
        #           False:[]
        #           }
        # 'fingers_r':{True:[0.,0.,0.,0.,0.,0.],
        #              False:[]
        #              },
        #  'fingers_l':{True:[0.,0.,0.,0.,0.,0.],
        #              False:[]
        #              },
          'head_and_neck':{True:[0.,0.,0.,0.,0.,0.],
                      False:[]
                      }
        #'hat_ribs_scap':[0.00011565, -0.00353864,  0.37]

    }

    ### --- SubjectMoved.osim
    
    # #Path to the ros package
    # ros_package_path = "/home/auctus/catkin_ws/src/human_control/"
    # # Path to the vtp files
    # vtp_path='/home/auctus/opensim_python/OpenSim/FullBodyModel/Geometry'
    # # The path to the .osim model
    # osim_path = '/home/auctus/opensim_python/OpenSim/FullBodyModel'
    # filename='SubjectMoved.osim'
    
    # exactly_collision = False
    # exactly_visual = True
    
    # # True : condition exactly
    # # False : condition approx
    
    # correc_stl_dic = {
    #     'hat_spine':{True:[],
    #                   False:[0.00011565, -0.00353864,  0.26422478] #0.29422478
    #                   },
    #     'bofoot':{True:[],
    #               False:[-0.00004618,  0.025, -0.00304539]
    #               },
    #     'fingers_r':{True:[0.,0.,0.,0.,0.,0.], 
    #                   False:[]
    #                   },
    #       'fingers_l':{True:[0.,0.,0.,0.,0.,0.], 
    #                   False:[]
    #                   },               
    #       'jaw':{True:[], 
    #               False:[ 0.00007359,  0.05567701,  0.07865301]
    #                   },     
          
    #       'head_and_neck':{True:[0.,0.,0.,0.,0.,0.], 
    #                   False:[]
    #                   }                   
    #     #'hat_ribs_scap':[0.00011565, -0.00353864,  0.37]
        
    #     }

    o2u  = Osim2URDF(osim_path, vtp_path, filename, ros_package_path, exactly_visual, exactly_collision,correc_stl_dic,True)
