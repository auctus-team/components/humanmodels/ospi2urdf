#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Fri Feb 26 16:06:14 2021

@author: auctus
"""

import json
import numpy as np



if __name__ == "__main__":

    ### ----- TO FILL UP FOR EACH NEW .OSIM FILE -----
    
    # Name of the .osim file
    L_filename = ['two_arms_only_marks.osim',
		'two_arms_only_marks_mod_march.osim',
    'two_arms_only_marks_c3d.osim']
    
    # Path to the ros package
    L_ros_package_path = ["/home/auctus/catkin_ws/src/human_control/",
			"/home/auctus/catkin_ws/src/human_control/",
      "/home/auctus/catkin_ws/src/human_control/"]
    # Path to the vtp files
    L_vtp_path = ['','', '']
    # The path to the .osim model
    L_osim_path = ['/home/auctus/ospi2urdf/models/two_arms_only/',
		'/home/auctus/ospi2urdf/models/two_arms_only/',
    '/home/auctus/ospi2urdf/models/two_arms_only/']

    # True : try to represent the collision aspect of the model from the .stl files.
    # False : use approximations according to the dimensions of the .stl file.
    L_exactly_collision = [False, False, False]
    
    # True : try to represent the visual aspect of the model from the .stl files.      
    # False : use approximations according to the dimensions of the .stl file. 
    L_exactly_visual = [True,True, True]


    # Sometimes, the stl files could have some mistakes into them, especially
    # about the position of their mass center. In order to fix those mistakes,
    # the user can add in this dictionary the names of the stl files or body parts 
    # causing problems, as well as their true position depending on whether we are 
    # dealing with an exact or approximate representation of the human body.
    # Then, during the writing of the URDF file, the algorithm will check those
    # informations and use them if necessary.
    # To sum up, here's a example of an element of this Dictionary :
        
    # 'name of stl file' : {True_Case : [ coordinates xyz of the mass center of element in case true ],
    #                           False_Case : [ coordinates xyz of the mass center of element in case false ]
    #                     }
        
    # 'name of body part' : {True_Case : [ coordinates rpy xyz of the origin of all the .stl parts of the
    #                                         body part in case true ],
    #                           False_Case : [ coordinates rpy xyz of the origin of all the .stl parts of the
    #                                         body part in case false ]
    #                         }   
        
    # With True_Case and False_Case respectively the boolean "True" and "False".
    # If no correction is needed in True_Case or False_Case, the list should be empty.
    # Ex : 
        
    # 'stl file bofoot': {True:[],
    #                         False:[-0.00004618,  0.025, -0.00304539]
    #                     },
    # 'body part fingers_r':{True:[0.,0.,0.,0.,0.,0.], 
    #                           False:[]
    #                       }    
    L_correc_stl_dic = [ {
        'hat_spine': {True: [],
                      # 0.29422478
                      False: [0.00011565, -0.00353864,  0.26422478]
                      },
        'bofoot': {True: [],
                    False: [-0.00004618,  0.025, -0.00304539]
                    },
        'fingers_r': {True: [np.pi/2., 0., np.pi/2, 0., 0., 0.],
                      False: []
                      },
        'fingers_l': {True: [np.pi/2, 0., np.pi/2, 0., 0., 0.],
                      False: []
                      },
        'jaw': {True: [],
                False: [0.00007359,  0.05567701,  0.07865301]
                },

        'head_and_neck': {True: [np.pi/2, 0., np.pi/2, 0., 0., 0.],
                          False: []
                          }
        #'hat_ribs_scap':[0.00011565, -0.00353864,  0.37]

    }    , 
	{
        'hat_spine': {True: [],
                      # 0.29422478
                      False: [0.00011565, -0.00353864,  0.26422478]
                      },
        'bofoot': {True: [],
                    False: [-0.00004618,  0.025, -0.00304539]
                    },
        'fingers_r': {True: [np.pi/2., 0., np.pi/2, 0., 0., 0.],
                      False: []
                      },
        'fingers_l': {True: [np.pi/2, 0., np.pi/2, 0., 0., 0.],
                      False: []
                      },
        'jaw': {True: [],
                False: [0.00007359,  0.05567701,  0.07865301]
                },

        'head_and_neck': {True: [np.pi/2, 0., np.pi/2, 0., 0., 0.],
                          False: []
                          }
        #'hat_ribs_scap':[0.00011565, -0.00353864,  0.37]

    }    ,
    	{
        'hat_spine': {True: [],
                      # 0.29422478
                      False: [0.00011565, -0.00353864,  0.26422478]
                      },
        'bofoot': {True: [],
                    False: [-0.00004618,  0.025, -0.00304539]
                    },
        'fingers_r': {True: [np.pi/2., 0., np.pi/2, 0., 0., 0.],
                      False: []
                      },
        'fingers_l': {True: [np.pi/2, 0., np.pi/2, 0., 0., 0.],
                      False: []
                      },
        'jaw': {True: [],
                False: [0.00007359,  0.05567701,  0.07865301]
                },

        'head_and_neck': {True: [np.pi/2, 0., np.pi/2, 0., 0., 0.],
                          False: []
                          }
        #'hat_ribs_scap':[0.00011565, -0.00353864,  0.37]

    }   
    
    ]
    
    ### ---- END OF FILLING UP ----


    json_file = "../utilis_files/data.txt"
    
    with open(json_file, 'w') as outfile:
        data = {}
        for i in range(len(L_filename)):
            data[L_filename[i]] = []
            
            
            data[L_filename[i]].append({'ros_package_path': L_ros_package_path[i],
                                        'vtp_path' : L_vtp_path[i],
                                        'osim_path': L_osim_path[i],
                                        'exactly_visual': L_exactly_visual[i],
                                        'exactly_collision': L_exactly_collision[i],
                                        'correc_stl_dic' : L_correc_stl_dic[i]
                                        })
                                        
        
        json.dump(data,outfile)
            

        
    
    
