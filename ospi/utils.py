"""
.. module:: opensim parser with pinocchio
       :platform: ubuntu
       :parse OpenSim into pinocchio models
.. moduleauthor:: Galo Maldonado <galo_xav@hotmail.com>
"""

import numpy as np
import pinocchio as se3
import algebra as alg 
#from IPython import embed

def _parse2PinocchioJoints_ori(pymodel):
    """
    Original method used by the ospi package, in order to express .osim
    joints into .urdf joints.
    Parameters
    ----------
    pymodel : Dictionnary of List
        Dictionnary containing every useful informations about the .osim
        file, in order to convert this file into a .urdf format.

    Returns
    -------
    joint_models : List of lists
        Contains informations about each of the joints 
        [index,name,axis expressed into the .urdf referential]
    joint_transformations : List of lists
        Contains the transformations of each joints, expressed into the .urdf
        referential.
        
    """
    jts = 0 
    # save pinocchio like joint models
    joint_models = []
    # axis transformations (e.g. for inversing sign with left and right)
    joint_transformations = []
    for joints in pymodel['Joints']:
        #print(joints[2])
        dof_in_joint = 6 - (joints[2]['coordinates']).count(None)
        #print(dof_in_joint)
        if dof_in_joint == 5:
            print("ERROR 5 DDL")
            print(joints[0]['name'][0])
            print(joints[2]['coordinates'])
        if dof_in_joint == 4:
            print("ERROR 4 DDL")
            print(joints[0]['name'][0])
            print(joints[2]['coordinates'])           
        if dof_in_joint == 0:
            print("ERROR 0 DDL")
            print(joints[0]['name'][0])
            print(joints[2]['coordinates'])           
                  
                  

        if dof_in_joint == 6:
            print("6 DOF : name : ", joints[0]['name'])

            joint_transformations.append(np.matrix(np.float64(joints[2]['axis'])))
            joint_models.append([jts, pymodel['Joints'][jts][0]['name'][0], se3.JointModelFreeFlyer()])
        elif dof_in_joint == 3:
            # print("3 DOF")
            # print("Name : %s"%joints[0]['name'])
            # print(np.matrix(np.float64(joints[2]['axis']))[0:3,:])

            joint_transformations.append( np.matrix(np.float64(joints[2]['axis']))[0:3,:]) 
            joint_models.append([jts, pymodel['Joints'][jts][0]['name'][0], se3.JointModelSpherical()])
        elif dof_in_joint == 2:
            print('2 dof in beta')

            a = se3.JointModelComposite()
            for dof in range(0, len(joints[2]['coordinates'])):
                if joints[2]['coordinates'][dof] != None:
                    if joints[2]['name'][dof][0:8] == 'rotation':

                        if joints[2]['axis'][dof] == ['1', '0', '0'] :
                        #if joints[2]['axis'][dof] == ['1', '0', '0'] or joints[2]['axis'][dof] == ['-1', '0', '0']:
                            #Y
                            
                            a.addJoint(se3.JointModelRY())
                            
                        elif joints[2]['axis'][dof] == ['0', '1', '0']:
                            #Z
                            a.addJoint(se3.JointModelRZ())

                        elif joints[2]['axis'][dof] == ['0', '0', '1']:
                            #X
                            a.addJoint(se3.JointModelRX())

                        else:
                            #print("Not aligned : name : ", joints[0]['name'])
                            #print(np.matrix(np.float64(joints[2]['axis']))[dof] )

                            v=np.matrix( [np.float64(joints[2]['axis'][dof][0]),
                                          np.float64(joints[2]['axis'][dof][1]), 
                                          np.float64(joints[2]['axis'][dof][2])] )
                            a.addJoint(se3.JointModelRevoluteUnaligned(v[0,2], v[0,0], v[0,1]))
            
            joint_transformations.append(np.matrix(np.float64(joints[2]['axis']))[0:2,:])
            joint_models.append([jts, pymodel['Joints'][jts][0]['name'][0], a])             
            
        elif dof_in_joint == 1:
            for dof in range(0, len(joints[2]['coordinates'])):
                if joints[2]['coordinates'][dof] != None:
                    if joints[2]['name'][dof][0:8] == 'rotation':
                        if joints[2]['axis'][dof] == ['1', '0', '0'] :
                                                
                        #if joints[2]['axis'][dof] == ['1', '0', '0'] or joints[2]['axis'][dof] == ['-1', '0', '0']:
                                                    #Y
                            joint_models.append([jts,pymodel['Joints'][jts][0]['name'][0],se3.JointModelRY()])
                            joint_transformations.append(np.matrix(np.float64(joints[2]['axis']))[dof])
                            #print("Joint_transfo pr [1,0,0] : ", joint_transformations[-1])
                            
                        elif joints[2]['axis'][dof] == ['0', '1', '0']:
                            #Z
                            joint_models.append([jts,pymodel['Joints'][jts][0]['name'][0],se3.JointModelRZ()])
                            joint_transformations.append(np.matrix(np.float64(joints[2]['axis']))[dof])
                            #print("Joint_transfo pr [0,1,0] : ", joint_transformations[-1])

                        elif joints[2]['axis'][dof] == ['0', '0', '1']:
                            #X
                            joint_models.append([jts,pymodel['Joints'][jts][0]['name'][0],se3.JointModelRX()])
                            joint_transformations.append(np.matrix(np.float64(joints[2]['axis']))[dof])
                            #print("Joint_transfo pr [0,0,1] : ", joint_transformations[-1])

                        else:
                            #print("Not aligned : name : ", joints[0]['name'])
                            #print(np.matrix(np.float64(joints[2]['axis']))[dof] )
                            joint_transformations.append(np.matrix(np.float64(joints[2]['axis']))[dof])
                            v=np.matrix( [np.float64(joints[2]['axis'][dof][0]),
                                          np.float64(joints[2]['axis'][dof][1]), 
                                          np.float64(joints[2]['axis'][dof][2])] )
                            #2,0,1
                            joint_models.append([jts,
                                                 pymodel['Joints'][jts][0]['name'][0],
                                                 se3.JointModelRevoluteUnaligned(v[0,2], v[0,0], v[0,1])])
                            # x,y,z osim --> z,x,y urdf
        jts += 1                    
    return joint_models, joint_transformations




def _parse2PinocchioJoints(pymodel,M = np.dot(se3.utils.rotate('z', np.pi/2) , se3.utils.rotate('x', np.pi/2) ), free_flyer=False):
    """
    New method used to express .osim joints into .urdf joints. 
    
    Parameters
    ----------
    pymodel : Dictionnary of List
        Dictionnary containing every useful informations about the .osim
        file, in order to convert this file into a .urdf format.
    M : numpy.array (3x3)
        Matrix for moving from the .osim referential to the .urdf referential.

    Returns
    -------
    joint_models : List of lists
        Contains informations about each of the joints 
        [index,name,axis expressed into the .urdf referential]
    joint_transformations : List of lists
        Contains the transformations of each joints, expressed into the .urdf
        referential.
        
    """
    jts = 0 
    # save pinocchio like joint models
    joint_models = []
    # axis transformations (e.g. for inversing sign with left and right)
    joint_transformations = []
    for joints in pymodel['Joints']:
        #print(joints[2])
        dof_in_joint = 6 - (joints[2]['coordinates']).count(None)
        #print(dof_in_joint)
        if dof_in_joint == 5:
            print("ERROR 5 DDL")
            print(joints[0]['name'][0])
            print(joints[2]['coordinates'])
        if dof_in_joint == 4:
            print("ERROR 4 DDL")
            print(joints[0]['name'][0])
            print(joints[2]['coordinates'])           
        if dof_in_joint == 0:
            print("0 DOF : name : ", joints[0]['name'])
            joint_transformations.append(np.matrix(np.float64(joints[2]['axis'])))
            joint_models.append([jts, pymodel['Joints'][jts][0]['name'][0], se3.JointModelComposite()])
                  
        if dof_in_joint == 6:
            print("6 DOF : name : ", joints[0]['name'])
            

            M2D = M
            M_ax = np.array([])
            for k in range(6):
                try : 
                    osim_axis= np.matrix(joints[2]['axis'][k]).astype(int)
                except ValueError: 
                    osim_axis= np.matrix(joints[2]['axis'][k]).astype(float)

                urdf_axis = (np.dot(M2D, osim_axis.reshape(3,1) ).astype(str)).flatten()
                if M_ax.shape == (0,):
                    M_ax = np.float64(urdf_axis)
                else:
                    M_ax= np.vstack((M_ax,np.float64(urdf_axis) )) 
            
            M_ax = M_ax.astype(str)
            joint_transformations.append( M_ax )
            joint_models.append([jts, pymodel['Joints'][jts][0]['name'][0], se3.JointModelFreeFlyer()])
            #exit(1)
        elif dof_in_joint == 3:
            
            osim_axis = np.matrix(np.float64(joints[2]['axis'])[0:3,:]).astype(float)

            urdf_axis = np.array([])
            
            for k in range(3):
                cur_ax = osim_axis[k,:]
                cur_ax = (np.dot(M, cur_ax.reshape(3,1) ).astype(str)).flatten() 
                if urdf_axis.shape == (0,):
                    urdf_axis = cur_ax.reshape(1,3)
                else:
                    urdf_axis = np.vstack( (urdf_axis,cur_ax) )

            # print("Name 3 DOF : ", joints[0]['name'][0])
            # print("Osim axis : ", osim_axis)
            #print("URDF_axis : ", urdf_axis)

            joint_transformations.append( urdf_axis )
            joint_models.append([jts, pymodel['Joints'][jts][0]['name'][0], se3.JointModelSpherical()])
        elif dof_in_joint == 2:

            a = se3.JointModelComposite()
            M_ax = np.array([])
            
            tot_osim = np.array([])

            M2D = M

            for dof in range(0, len(joints[2]['coordinates'])):
                if joints[2]['coordinates'][dof] != None:
                    #print("Curr transform : ", joints[2]['coordinates'][dof])
                    if joints[2]['name'][dof][0:8] == 'rotation':
                        try : 
                            osim_axis= np.matrix(joints[2]['axis'][dof]).astype(int)
                        except ValueError: 
                            osim_axis= np.matrix(joints[2]['axis'][dof]).astype(float)
                        
                        # if tot_osim.shape == (0,):
                        #     tot_osim = osim_axis.reshape(1,3)
                        # else:
                        #     tot_osim = np.vstack((tot_osim, osim_axis.reshape(1,3)))

                        urdf_axis = (np.dot(M2D, osim_axis.reshape(3,1) ).astype(str)).flatten()

                        if urdf_axis[0,0] == '1.0':
                            #X
                            a.addJoint(se3.JointModelRX())
                            
                        elif urdf_axis[0,1] == '1.0':
                            #Y
                            a.addJoint(se3.JointModelRY())

                        elif urdf_axis[0,2] == '1.0':
                            #Z
                            a.addJoint(se3.JointModelRZ())

                        else:

                            v=np.matrix( [ np.float64(urdf_axis[0,0]),
                                            np.float64(urdf_axis[0,1]), 
                                            np.float64(urdf_axis[0,2])] )
                            a.addJoint(se3.JointModelRevoluteUnaligned(v[0,0], v[0,1], v[0,2]))
                        if M_ax.shape == (0,):
                            M_ax = np.float64(urdf_axis)
                        else:
                            M_ax= np.vstack((M_ax,np.float64(urdf_axis) )) 

            M_ax = M_ax.astype(str)
            # print("Name 2 DOF : ", joints[0]['name'][0])
            # print("Osim axis : ", tot_osim)
            # print("URDF_axis : ", M_ax)
            joint_transformations.append( M_ax )
            joint_models.append([jts, pymodel['Joints'][jts][0]['name'][0], a])             
            
        elif dof_in_joint == 1:
            for dof in range(0, len(joints[2]['coordinates'])):
                if joints[2]['coordinates'][dof] != None:
                    if joints[2]['name'][dof][0:8] == 'rotation':
                        try : 
                            osim_axis= np.matrix(joints[2]['axis'][dof]).astype(int)
                        except ValueError:
                            osim_axis =  np.matrix(joints[2]['axis'][dof]).astype(float)
                        

                        
                        
                        urdf_axis = (np.dot(M, osim_axis.reshape(3,1) ).astype(str)).flatten()

                        # print("Name : ", joints[0]['name'][0])
                        # print("Osim axis : ", osim_axis)
                        # print("URDF_axis : ", urdf_axis)
                        
                        if urdf_axis[0,0] == '1.0':
                            #X
                            joint_models.append([jts,pymodel['Joints'][jts][0]['name'][0],se3.JointModelRX()])
                            joint_transformations.append(np.matrix(np.float64(urdf_axis)))
                            
                        elif urdf_axis[0,1] == '1.0':
                            #Y
                            joint_models.append([jts,pymodel['Joints'][jts][0]['name'][0],se3.JointModelRY()])
                            joint_transformations.append(np.matrix(np.float64(urdf_axis)))

                        elif urdf_axis[0,2] == '1.0':
                            #Z
                            joint_models.append([jts,pymodel['Joints'][jts][0]['name'][0],se3.JointModelRZ()])
                            joint_transformations.append(np.matrix(np.float64(urdf_axis)))

                        else:
                            #print("Not aligned : name : ", joints[0]['name'])
                            #print(np.matrix(np.float64(joints[2]['axis']))[dof] )
                            # print("axis : ", np.float64(urdf_axis))
                            # print("dof : ", dof)
                            joint_transformations.append(np.matrix(np.float64(urdf_axis))[0])

                            v=np.matrix( [np.float64(urdf_axis[0,0]),
                                          np.float64(urdf_axis[0,1]), 
                                          np.float64(urdf_axis[0,2])] )
                            #2,0,1
                            joint_models.append([jts,
                                                  pymodel['Joints'][jts][0]['name'][0],
                                                  se3.JointModelRevoluteUnaligned(v[0,0], v[0,1], v[0,2])])
        jts += 1                    
    return joint_models, joint_transformations


def pinocchioCoordinates(model, joint_transformations, dof, representation="quat"):
    """
    This method seems to be used in order to express the limits of each joints
    into the .urdf referential.
    Used only into the ospi package

    Parameters
    ----------
    model : TYPE
        DESCRIPTION.
    joint_transformations : TYPE
        DESCRIPTION.
    dof : TYPE
        DESCRIPTION.
    representation : TYPE, optional
        DESCRIPTION. The default is "quat".

    Returns
    -------
    TYPE
        DESCRIPTION.

    """
    jt = joint_transformations
    oMp = se3.utils.rotate('z', np.pi/2) * se3.utils.rotate('x', np.pi/2)
    q = np.matrix(np.zeros((model.nq, 1)))
    qo_idx = 0 #osim index
    qp_idx = 0 #pinocchio index
    
    # for quaternions
    def orderQuat(quat):
    	return [quat[1], quat[2], quat[3], quat[0]]
    
    for i in range(1,len(model.joints)):
        if (model.joints[i].shortname() == "JointModelFreeFlyer"):
            q[qp_idx+0:qp_idx+3,0] = ( oMp * dof[qo_idx+3:qo_idx+6,0] ).A  #tx,ty,tz
            q[qp_idx+3:qp_idx+7,0] = np.matrix(orderQuat(alg.quaternion_from_matrix(alg.euler_matrix((dof[qo_idx,0]), dof[qo_idx+1,0], dof[qo_idx+2,0], 'rxyz')))).T
            qo_idx += 6
            qp_idx += 7
        elif (model.joints[i].shortname() == "JointModelSpherical"):
            jt_sph = jt[i-1]
            tup = np.nonzero(jt_sph)
            row=tup[0]; col=tup[1];
            
            # reshape necessary
            
            row = row.reshape(1,3)
            col = col.reshape(1,3)

            q[qp_idx:qp_idx+4,0] = np.matrix(orderQuat(alg.quaternion_from_matrix(alg.euler_matrix((dof[qo_idx,0])*jt_sph[row[0,0],col[0,0]], dof[qo_idx+1,0]*jt_sph[row[0,1],col[0,1]], dof[qo_idx+2,0]*jt_sph[row[0,2],col[0,2]], 'rxyz')))).T
            qo_idx += 3
            qp_idx += 4
        elif (model.joints[i].shortname() == "JointModelSphericalZYX"):
            # TODO joint transformation
            q[qp_idx:qp_idx+4,0] = np.matrix(orderQuat(alg.quaternion_from_matrix(alg.euler_matrix((dof[qo_idx,0]), dof[qo_idx+1,0], dof[qo_idx+2,0], 'rzyx')))).T
            qo_idx += 3
            qp_idx += 4
        elif (model.joints[i].shortname() == "JointModelRevoluteUnaligned"):

            q[qp_idx,0] = dof[qo_idx,0]
            
            
            qo_idx += 1
            qp_idx += 1
        elif (model.joints[i].shortname() == "JointModelRX" or 
              model.joints[i].shortname() == "JointModelRY" or 
              model.joints[i].shortname() == "JointModelRZ"):
            #print("dof : ", dof)
            #print("q_bef : ", q)
            jt_rev = jt[i-1]
            jt_idx = np.nonzero(jt_rev.A1)[0][0]
            q[qp_idx,0] = dof[qo_idx,0]*jt_rev[0,jt_idx]
            qo_idx += 1
            qp_idx += 1
            #print("q_aft : ", q)
            
    return q



