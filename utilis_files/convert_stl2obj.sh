#!/bin/bash
echo conversion fichiers stl to obj

PATH2STL="/home/auctus/ospi/models/tst_arm/stl"
PATH2OBJ="/home/auctus/ospi/models/tst_arm/obj"

mkdir $PATH2OBJ
cd $PATH2STL
for i in *.stl
	do
		ctmconv $PATH2STL"/"$(basename $i .${i##*.}).stl $PATH2OBJ"/"$(basename $i .${i##*.}).obj
	done

