# osim2urdf 

This library is used to convert .osim files into .urdf files. It is in part based on the ospi package.
For now it was tested on Ubuntu 18.04, Python 3.6; but also on Ubuntu 20.04, Python 3.8.10. 
This branch is dedicated to Ubuntu 20.04.
It was not tested with conda : all packages were directly installed into usr, without using a virtual environnement.

Please check the [wiki](https://gitlab.inria.fr/auctus/ospi2urdf/-/wikis/Home-page-of-osim2urdf) for more informations.

# How to use

## Check your .osim model

Do : 

opensim-cmd viz --geometry /home/auctus/sensoring_ws/opensim_ws/Documents/Models/SimulationDataAndSetupFiles-4.0/Geometry model /home/auctus/sensoring_ws/opensim_ws/src/ospi2urdf2/models/upper_body/20230309/full_upper_body_marks_mod.osim

opensim-cmd viz --geometry /home/auctus/sensoring_ws/opensim_ws/src/addbiomechanicslocal/scripts/addbiomechanicslocal/Geometry/ model unscaled_generic.osim 

opensim-cmd viz --geometry /home/auctus/sensoring_ws/opensim_ws/src/addbiomechanicslocal/scripts/addbiomechanicslocal/Geometry/ model osim_results/Models/final.osim

